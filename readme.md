# LibNexaKotlin

Multiplatform light client Nexa blockchain access library.

## Documentation

[See https://nexa.gitlab.io/libnexakotlin/]

## Inclusion in Gradle Kotlin DSL projects

[Find the latest library version here.](https://gitlab.com/api/v4/projects/48545045/packages)

In your build.grade.kts:

```gradle
repositories {
    maven { url = uri("https://gitlab.com/api/v4/projects/48545045/packages/maven") } // LibNexaKotlin
}

dependencies {
    // typically this library is used in test code so the example shown uses "testImplementation"
    implementation("org.nexa:libnexakotlin:0.0.1")  // Update this version with the latest
}
```

## Package Maintainer Notes

### Installing build tools

Building this project requires "NDK" build tools installed in Android Studio, and the native Kotlin compiler installed and run on your system.
This project does not use the native Kotlin compiler directly, but we need to use the same compilers (located in ~/.konanc/dependencies) to ensure 
object compatibility.

See the CI environment docker file for the latest details on how to install the build tools: https://github.com/BitcoinUnlimited/gitlab-ci-android/blob/main/Dockerfile

### Building the C libraries

Gradle sync builds the libnexalight C++ code.  To do this, you need to symlink libnexa/nexa to the Nexa full node project (https://gitlab.com/nexa/nexa).

Assuming the full node is cloned at the same level of this project you could execute the following at the bash prompt:
```(cd libnexa; ln -s ../../nexa nexa)```

The Nexa full node must be "clean" -- you cannot have run an "in-source-tree" build inside it or your local architecture will be
preferred over the multiplatform architectures.  If it is not clean, clean it "git clean -fxd", and do out-of-source-tree builds
from now on.

Next, you need to prepare some dependencies that don't exist in some architectures inside the full node code.  There is a script to
do this:
(cd libnexa; ./prepnexa.sh) 

The build should now work (see "Build all libraries" below).

If this document becomes out of date, look at the build recipe in .gitlab-ci.yml to see how the automated test does it.

**NOTE**
Building on linux cannot include Apple targets due to Apple being uncooperative with respect to crossbuilds.  But we need those
targets to be defined, or they are not put into the Maven multiplatform inventory file.  Therefore, the build.gradle.kts file
does an ugly thing -- it defines the Apple targets on Linux hosts, but gradle automatically "forgets" about their dependency on 
the libnexa subproject on Linux.

### Refresh gradle dependencies

```bash
./gradlew -refresh-dependencies
```

### Ubuntu Build environment setup

```
# install necessary packages
# prevent installation of openjdk-11-jre-headless with a trailing minus,
# as openjdk-8-jdk can provide all requirements and will be used anyway
RUN apt-get update && apt-get install -qqy --no-install-recommends \
    apt-utils \
    openjdk-17-jdk \
    checkstyle \
    unzip \
    curl \
    cmake \
    lldb \
    git \
    python-is-python3 \
    ninja-build \
    build-essential \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
```

See the [Dockerfile used in GitLab CI](https://github.com/bitcoinunlimited/gitlab-ci-android) for more information on env setup.

#### Android SDK and NDK
1. Install Android Studio
2. Install latest Android SDK using Android Studio
3. Add Android SDK path to env variable $ANDROID_HOME
4. Install Android NDK version 26.2.11394342 Android Studio

### Build all libraries

```bash
(cd ../nexa; git reset --hard HEAD; git pull)
(cd libnexa; sudo ./prepnexa.sh)
sudo ./gradlew assemble
```

### Publish to Maven

To publish, you need to publish on MacOS (./gradlew publish), and then publish on Linux (same command).  If you do it in the reverse order,
the MacOS publish will overwrite the maven table-of-contents file with a version that is missing all targets not built on MacOS.

The effect of this will be that you will see targets like Linux in your Maven repository, but gradle will not be able to access the dependency.

```bash
./gradlew publish
```

The projects are set up so that a local (on your computer) maven repository is searched first.  So you may be able to overwrite a release during 
development to try iterative fixes without bumping the release number.  But since gradle caches dependencies, this often does not work.  You can try
"./gradlew -U" to reload dependencies, but again my experience with this is erratic.  When in doubt, bump the release number!

```bash
./gradlew publishToMavenLocal
```