package org.nexa.libnexakotlin

// CLI stuff isn't available on native anyway because you need a kotlin interpreter

actual fun showObject(
    obj: Any,
    level: Display,
    style: DisplayStyles,
    depth: Int,
    indent: Int,
    maxString: Int
): String
{
    return obj.toString()
}

actual fun showApis(obj: Any, level: Display, style: DisplayStyles): String?
{
    return null
}