@file:OptIn(ExperimentalForeignApi::class)

package org.nexa.libnexakotlin.socketPosix

import kotlinx.cinterop.*
import org.nexa.libnexakotlin.SocketAddress
import org.nexa.libnexakotlin.SocketOptions
import org.nexa.libnexakotlin.iTcpSocket
import org.nexa.libnexakotlin.millinow
import org.nexa.libnexakotlin.resolveDomain
import platform.posix.*
import kotlin.coroutines.CoroutineContext
import kotlin.native.concurrent.ThreadLocal

private val LogIt = org.nexa.libnexakotlin.GetLog("BU.socket")

fun Int.htons(): UShort
{
    if (BYTE_ORDER == platform.posix.BIG_ENDIAN) return this.toUShort()
    else
    {
        val v = this.toUInt()
        //LogIt.info("Int.htons swapping $this $v ${v.toString(16)}")
        val a = ((v shr 8) and 0xffU)
        val b = (v and 0xffU) shl 8
        //LogIt.info("Int.htons ${a.toString(16)} ${b.toString(16)}")
        return (a or b).toUShort()
    }
}

fun ByteArray.bigEndianToUInt(): UInt
{
    //return this[0].toUInt() or (this[1].toUInt() shl 8) or (this[2].toUInt() shl 16) or (this[3].toUInt() shl 24)
    var ret:UInt = this[0].toUByte().toUInt()
    //LogIt.info(ret.toString(16))
    ret = ret or (this[1].toUByte().toUInt() shl 8)
    //LogIt.info(ret.toString(16))
    val b2 = (this[2].toUByte().toUInt() shl 16)
    ret = ret or b2
    //LogIt.info("b2: $b2, ret: ${ret.toString(16)}")
    val b3 = (this[3].toUByte().toUInt() shl 24)
    ret = ret or b3
    //LogIt.info("b3: $b3, ret: ${ret.toString(16)}")
    return ret
}

// actual
fun awaitAvailable(cnxns: Collection<iTcpSocket>, timeout: Long): Boolean
{
    if (cnxns.size == 0) return false
    memScoped {
        val fds = alloc<fd_set>()

        var maxFd: Int = -1
        for (ic in cnxns)
        {
            val c = ic as TcpSocketPosix
            val fd = c.sock
            if (fd > maxFd) maxFd = fd
            if (fd >=0)
            {
                val idx = fd/32
                val bitOffset = fd and 31
                fds.fds_bits.set(idx, fds.fds_bits.get(idx) or (1.toUInt() shl bitOffset).toInt())
            }
        }
        if (maxFd == -1) return false  // Every socket is closed

        val expire = alloc<timeval>()
        expire.tv_sec = (timeout/1000L)
        expire.tv_usec = ((timeout%1000L) * 1000L).toInt()

        val ret = select(maxFd+1, fds.ptr, null, null, expire.ptr)
        if (ret < 0)
        {
            val err = errno
            LogIt.warning("select error: $err")
            perror("socket posix select")
            return false
        }
        return true
    }
}

class TcpSocketPosix constructor(val sendCtxt: CoroutineContext, val receiveCtxt: CoroutineContext): iTcpSocket
{
    var sock: Int = -1  // < 0 used to mean socket closed
    var noDelay: Boolean = false
    /** Attempt to connect */
    @OptIn(ExperimentalStdlibApi::class, ExperimentalForeignApi::class)
    //actual
    override fun connect(name: String, port: Int, connectTimeout: Int, sockOptions: (SocketOptions.() -> Unit)?)
    {
        // Otherwise SIGPIPE will cause the entire app to die (attempt to write a socket with nothing on the other side)
        signal(SIGPIPE, SIG_IGN)

        val domains = resolveDomain(name, port)
        val domain = domains.random()
        //LogIt.info("Resolved IP to ${domain.toHex()}")
        val dInt:UInt = domain.bigEndianToUInt()
        // LogIt.info("As an int this is ${dInt.toHexString()}")
        val so = SocketOptions(true, 65536, 65536, 5000, false, false)
        sock = socket(AF_INET, SOCK_STREAM, 0)
        // LogIt.info("Socket is $sock")
        if (sock < 0) return
        noDelay = so.noDelay
        setSocketOption(TCP_NODELAY, if (noDelay) 1 else 0)
        setSocketOption(SO_RCVBUF, so.receiveBufferSize)
        setSocketOption(SO_SNDBUF, so.sendBufferSize)
        setSocketOption(SO_RCVTIMEO, so.timeout)
        setSocketOption(SO_SNDTIMEO, so.timeout)
        setSocketOption(SO_KEEPALIVE, if (so.keepalive) 1 else 0)

        // get socket flags
        val flags = fcntl(sock, F_GETFL, 0)
        if (flags < 0)
        {
            LogIt.info("socket connect, cannot get flags")
            close()
            return
        }
        // set socket non-blocking just for the connect
        if (fcntl(sock, F_SETFL, flags or O_NONBLOCK) < 0)
        {
            LogIt.info("socket connect, cannot set flags")
            close()
            return
        }

        val porthtons = port.htons()
        val start = millinow()
        LogIt.info("$name  Connect attempt ip: 0x${dInt.toString(16)} port $port 0x${port.toString(16)}  htons: $porthtons 0x${porthtons.toString(16)}")
        val ret = domain.usePinned{
            memScoped {
                val si = alloc<sockaddr_in>().apply {
                    sin_family = AF_INET.convert()
                    sin_port = porthtons
                    sin_addr.s_addr = dInt
                    sin_len = 4.toUByte()
                }
                platform.posix.connect(sock, si.ptr.reinterpret(), sizeOf<sockaddr_in>().convert())
            }
        }
        if (ret < 0)
        {
            val connErr = errno
            if (connErr == EINPROGRESS)
            {
                LogIt.info("$name Connect in progress")
                memScoped {
                    val fds = alloc<fd_set>()
                    val idx = sock / 32
                    val bitOffset = sock and 31
                    fds.fds_bits.set(idx, fds.fds_bits.get(idx) or (1.toUInt() shl bitOffset).toInt())
                    val expire = alloc<timeval>()
                    expire.tv_sec = (connectTimeout / 1000L)
                    expire.tv_usec = ((connectTimeout % 1000L) * 1000L).toInt()

                    val selret = select(sock + 1, null, fds.ptr, null, expire.ptr)
                    val end = millinow()
                    if (selret < 0)
                    {
                        val err = errno
                        LogIt.info("$name Connect error $err  in ${end - start}ms")
                        perror("attempting to connect")
                        close()
                    }
                    else if (selret > 0)
                    {
                        LogIt.info("$name  Connect success (waited) in ${end - start}ms")
                    }
                    else
                    {
                        LogIt.info("$name  Connect timeout in ${end - start}ms")
                        close()
                    }
                }
            }
            else
            {
                val end = millinow()
                LogIt.info("$name Connect error $connErr  in ${end - start}ms")
                perror("attempting to connect")
                close()
            }
        }
        else
        {
            val end = millinow()
            LogIt.info("$name  Connect success in ${end - start}ms")
        }

        if (sock != -1)  // If I didn't close, it succeeded
        {
            // set socket flags back
            if (fcntl(sock, F_SETFL, flags) < 0)
            {
                close()
                return
            }
        }

    }

    /** Return true if the underlying connection is ok */
    //actual
    override fun isAlive(): Boolean
    {
        if (sock == -1) return false
        memScoped {
            val error = alloc<IntVar>()
            val len = alloc<socklen_tVar>()
            val result = getsockopt(sock, SOL_SOCKET, SO_ERROR, error.ptr, len.ptr)
            if (result != 0) return false // if getsockopt failed, socket is bad we are not connected
            return error.value == 0
        }
    }

    /** Close the underlying connection */
    //actual
    override fun close()
    {
        val tmp = sock
        sock = -1
        if (tmp != -1) platform.posix.close(tmp)
    }

    //actual
    override fun finalize()
    {
        close()
    }

    /** Returns number of bytes that can be read without blocking.  If the platform cannot
     * determine this (TLS), -1 is returned */
    //actual
    override val availableForRead: Int
        get() {
            if (sock == -1) return 0
            memScoped {
                val avail = alloc<IntVar>()
                avail.value = 0
                val ret = ioctl(sock, FIONREAD, avail.ptr)
                if (ret < 0) {
                    println("availableForRead error $ret")
                    return 0
                }
                return avail.value
            }
        }

    /** Reads all available bytes to dst buffer and returns immediately or suspends if no bytes available
    @return number of bytes were read or -1 if the channel has been closed
     */
    //actual
    override fun readAvailable(dst: ByteArray, offset: Int, length: Int): Int
    {
        if (sock == -1) return 0
        if (length == 0) return 0
        dst.usePinned {
            val ptr = it.addressOf(offset)
            val readlen = read(sock, ptr, length.toULong())
            if (readlen == -1L) {
                val err = errno
                if ((err == EAGAIN) || (err == EWOULDBLOCK) || (err == EINTR)) return 0
                if (err == EBADF) {
                    sock = -1
                    return 0
                }
                return 0
            }
            return readlen.toInt()
        }
    }

    /** Writes all src bytes and suspends until all bytes written. Causes flush if buffer filled up or when autoFlush Crashes if channel get closed while writing
     */
    //actual
    override fun writeFully(src: ByteArray, offset: Int, length: Int)
    {
        if (sock == -1) return
        var written = 0
        src.usePinned {
            while (written < length) {
                val ptr = it.addressOf(written)
                val wrote = write(sock, ptr, (length - written).toULong())
                if (wrote < 0) {
                    val err = errno
                    if ((err == EINTR) || (err == EAGAIN)) // try again
                    {
                    }
                    if ((err == EBADF) || (err == EINVAL) || (err == ECONNRESET) || (err == EPIPE))
                    {
                        sock = -1
                        return
                    }
                    if ((err == EACCES) || (err == ENETDOWN) || (err == ENETUNREACH))
                    {
                        LogIt.info("Socket network issue: $err")
                        close()
                    }
                } else written += wrote.toInt()
            }
        }
    }

    fun setSocketOption(flag: Int, value: Int):Int
    {
        memScoped {
            val v = alloc<IntVar>()
            v.value = value
            val result = setsockopt(sock, IPPROTO_TCP, flag, v.ptr, sizeOf<IntVar>().convert())
            if (result < 0)
            {
                val err = errno
                return err
            }
        }
        return 0
    }

    /** force any writes to actually be sent */
    //actual
    override fun flush()
    {
        // If the socket is not auto-flushing, turning nagle off then on will make it flush
        if (noDelay == false)
        {
            setSocketOption(TCP_NODELAY, 1)
            setSocketOption(TCP_NODELAY, 0)
        }
        // Otherwise, its flushed should be nothing to do
    }

    // TODO
    //actual
    override val localAddress: SocketAddress
        get() = SocketAddress("")
    //actual
    override val remoteAddress: SocketAddress
        get() = SocketAddress("")

}