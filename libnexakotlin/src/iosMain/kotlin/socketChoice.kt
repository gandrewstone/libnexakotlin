package org.nexa.libnexakotlin
import kotlin.coroutines.CoroutineContext

enum class TcpSocketChoice
{
    TCP_SOCKET_KTOR, TCP_SOCKET_POSIX
};

var useTcpSocketType = TcpSocketChoice.TCP_SOCKET_POSIX

actual fun awaitAvailable(cnxns: Collection<iTcpSocket>, timeout: Long): Boolean
{
    if (useTcpSocketType == TcpSocketChoice.TCP_SOCKET_KTOR)
    {
        return org.nexa.libnexakotlin.socketKtor.awaitAvailable(cnxns, timeout)
    }
    else return org.nexa.libnexakotlin.socketPosix.awaitAvailable(cnxns, timeout)
}

actual fun TcpSocket(sendCtxt: CoroutineContext, receiveCtxt: CoroutineContext):iTcpSocket
{
    val ret = if (useTcpSocketType==TcpSocketChoice.TCP_SOCKET_KTOR)
        org.nexa.libnexakotlin.socketKtor.TcpSocket(sendCtxt, receiveCtxt)
    else
        org.nexa.libnexakotlin.socketPosix.TcpSocketPosix(sendCtxt, receiveCtxt)
    return ret
}
