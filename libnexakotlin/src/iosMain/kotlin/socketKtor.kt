package org.nexa.libnexakotlin.socketKtor

import org.nexa.libnexakotlin.*
import io.ktor.network.selector.SelectorManager
import io.ktor.network.sockets.Socket
import io.ktor.network.sockets.aSocket
import io.ktor.network.sockets.isClosed
import io.ktor.network.sockets.openReadChannel
import io.ktor.network.sockets.openWriteChannel
import io.ktor.network.tls.tls
import io.ktor.utils.io.ByteReadChannel
import io.ktor.utils.io.ByteWriteChannel
import io.ktor.utils.io.errors.IOException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.nexa.threads.Gate
import org.nexa.threads.Mutex
import org.nexa.threads.iGate
import org.nexa.threads.millisleep
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.cancellation.CancellationException

val selectorLock = Mutex("ktorSelLock")
val selectorPerCtxt: MutableMap<CoroutineContext, SelectorManager> = mutableMapOf()

fun getSelectorFor(coCtxt: CoroutineContext): SelectorManager
{
    return selectorLock.lock {
        var ret = selectorPerCtxt[coCtxt]
        if (ret == null)
        {
            ret = SelectorManager(coCtxt)
            selectorPerCtxt[coCtxt] = ret
        }
        ret
    }
}


//actual
fun awaitAvailable(cnxns: Collection<iTcpSocket>, timeout: Long): Boolean
{
    var awaitGate = Objectify<iGate?>(Gate("awaitAvailable"))
    var avail = 0
    if (cnxns.size == 0)
    {
        millisleep(timeout.toULong())
        return false
    }
    val scope = CoroutineScope((cnxns.first() as TcpSocket).sendCtxt)
    try
    {
        // Listen for activity on any connection
        for (ic in cnxns)
        {
            val c = ic as TcpSocket
            if ((c.availableForRead) > 0 ) return true
            c.awaitContent(scope) {
                avail++
                awaitGate.obj?.wake()
            }
        }
        awaitGate.obj?.delayuntil(timeout, { avail != 0})
    }
    finally
    {
        val tmp = awaitGate.obj
        awaitGate.obj = null
        scope.cancel()
        tmp?.finalize()
    }
    return (avail > 0)
}

//actual
class TcpSocket // actual
constructor(val sendCtxt: CoroutineContext, val receiveCtxt: CoroutineContext):iTcpSocket
{
    var selectorManager = getSelectorFor(receiveCtxt)
    var sockBuilder = aSocket(selectorManager).tcp()

    var sock: Socket? = null
    var inp: ByteReadChannel? = null
    var out: ByteWriteChannel? = null

    //actual
    override fun connect(name: String, port: Int, connectTimeout: Int, sockOptions: (SocketOptions.() -> Unit)?)
    {
        val so = SocketOptions(true, 65536, 65536, 3000)
        sockOptions?.invoke(so)

        runBlocking(sendCtxt) {
            withTimeout(connectTimeout.toLong())
            {
                sock = sockBuilder.connect(name, port, {
                    this.noDelay = so.noDelay
                    this.receiveBufferSize = so.receiveBufferSize
                    this.sendBufferSize = so.sendBufferSize
                }) // NOTE: timeout is not included as in libbitcoincashkotlin, timeout)
                if (so.tls) sock = sock!!.tls(sendCtxt)
                inp = sock!!.openReadChannel()
                out = sock!!.openWriteChannel()
            }
        }
    }

    //actual
    override fun isAlive():Boolean
    {
        val s = sock
        if (s == null) return true // we call a connection that has not initialized yet alive
        if (s.isClosed) return false
        if (inp?.isClosedForRead == true) return false
        if (out?.isClosedForWrite == true) return false
        return true
    }

    //actual
    override fun close()
    {
        try
        {
            inp?.cancel(null)
        }
        catch (_: IOException)
        {
        }  // possibly already shutdown
        try
        {
            out?.close(null)
        }
        catch (e: IOException)
        {
        }
        try
        {
            sock?.close()
            sock = null
        }
        catch (_: IOException)
        {
        }
    }

    //actual
    override fun finalize()
    {
        close()
    }

    /** Returns number of bytes that can be read without blocking */
    //actual
    override val availableForRead: Int
        get()
        {
            val input = inp ?: return 0
            val available = try
            {
                input.availableForRead
            }
            catch (e: IOException)
            {
                // Note: Although the available() documentation (https://docs.oracle.com/javase/8/docs/api/java/io/InputStream.html#available--)
                // implies that this exception is only thrown on close  it appears to be thrown at undetermined non-fatal times.
                // So model the exception as if no data is currently available.
                //LogIt.warning(sourceLoc() + ": IO exception getting available bytes on socket")
                0
            }
            return available
        }

    //actual
    override fun readAvailable(dst: ByteArray, offset: Int, length: Int): Int
    {
        val amt = runBlocking(receiveCtxt) { inp?.readAvailable(dst,offset,length) ?: -1 }
        return amt
    }

    /** Writes all src bytes and suspends until all bytes written. Causes flush if buffer filled up or when autoFlush Crashes if channel get closed while writing
     */
    //actual
    override fun writeFully(src: ByteArray, offset: Int, length: Int)
    {
        val tmp = out
        if (tmp != null)
        {
            runBlocking(sendCtxt) {
                tmp.writeFully(src, offset, length)
                tmp.flush()
            }
        }
    }

    /** force any writes to actually be sent */
    //actual
    override fun flush()
    {
        val tmp = out
        if (tmp != null) tmp.flush()
    }

    //actual
    override val localAddress: SocketAddress
        get()
        {
            return SocketAddress(sock?.localAddress?.toString() ?: "")
        }
    //actual
    override val remoteAddress: SocketAddress
        get()
        {
            return SocketAddress(sock?.remoteAddress?.toString() ?: "")
        }

    fun awaitContent(scope: CoroutineScope, cb: () -> Unit)
    {
        val input = inp ?: return
        launch(scope)
        {
            try
            {
                input.awaitContent()
            }
            catch (e: CancellationException)
            {
                return@launch
            }
            catch (e: Exception) { }

            try { cb() } catch(e: Exception) {}
        }
    }

}
