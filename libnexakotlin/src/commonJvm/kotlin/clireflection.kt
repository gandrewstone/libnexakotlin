package org.nexa.libnexakotlin

import java.util.EnumSet
import kotlin.reflect.KAnnotatedElement
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.KProperty1
import kotlin.reflect.full.*
import kotlin.time.DurationUnit
import kotlin.time.TimeSource

val MAX_SHOW_COLLECTION_SIZE = 5

actual val lineSeparator: String = "\n"

object FieldFunction {}
object FieldContinues {}
data class Nvh(val name: String, val value: Any?, val help: String)

typealias FieldDict = MutableMap<String, Nvh?>

fun showAny(obj: Any?, indent:Int, maxString:Int, delimiter:String="\n", dispValue:(String)->String={it}, dispHelp:(String)->String={ " (" + it + ")" }): String
{
    when (obj)
    {
        is String    -> return if (obj.length>0) obj.toString() else "<empty>"
        is ByteArray -> return if (obj.size > 0) obj.toHex() else "<empty>"
        is Nvh              -> return (obj.value.toString() + (if (obj.help.isNotBlank()) dispHelp(obj.help) else ""))
        is MutableMap<*, *> -> {
            val fd = obj as? FieldDict
            if (fd != null)
            {
                val sj = StringJoiner(delimiter)
                for (item in fd)
                {
                    val line = " ".repeat(indent) + item.key + " : " + showAny(item.value, indent+2, maxString)
                    sj.add( line)
                }
                return sj.toString()
            }
        }
        else                -> return obj.toString()
    }
    return obj.toString()
}


actual fun show(p: KProperty1<*, *>, obj: Any): String
{
    if (p.returnType.classifier == MutableMap::class)
    {
        val sz = (p.getter.call(obj) as MutableMap<*, *>).size
        if (sz > MAX_SHOW_COLLECTION_SIZE)
            return "{ ${(p.getter.call(obj) as MutableMap<*, *>).size} elements }"
    }
    if (p.returnType.classifier == Map::class)
    {
        val sz = (p.getter.call(obj) as Map<*, *>).size
        if (sz > MAX_SHOW_COLLECTION_SIZE)
            return "{ ${(p.getter.call(obj) as Map<*, *>).size} elements }"
    }
    if (p.returnType.classifier == MutableList::class)
    {
        try
        {
            val sz = (p.getter.call(obj) as MutableList<*>).size
            if (sz > MAX_SHOW_COLLECTION_SIZE)
                return "[ ${(p.getter.call(obj) as MutableList<*>).size} elements ]"
        }
        catch(e:ClassCastException)  // internal EmptyList can't be cast and can't be checked for
        {
            return "[]"
        }
    }
    if (p.returnType.classifier == List::class)
    {
        try
        {
            val sz = (p.getter.call(obj) as List<*>).size
            if (sz > MAX_SHOW_COLLECTION_SIZE)
                return "[ ${(p.getter.call(obj) as List<*>).size} elements ]"
        }
        catch(e:ClassCastException)  // internal EmptyList can't be cast and can't be checked for
        {
            return "[]"
        }
    }
    if (p.returnType.classifier == MutableSet::class)
    {
        val sz = (p.getter.call(obj) as MutableSet<*>).size
        if (sz > MAX_SHOW_COLLECTION_SIZE)
            return "{ ${(p.getter.call(obj) as MutableSet<*>).size} elements }"
    }
    if (p.returnType.classifier == Set::class)
    {
        val sz = (p.getter.call(obj) as Set<*>).size
        if (sz > MAX_SHOW_COLLECTION_SIZE)
            return "{ ${(p.getter.call(obj) as Set<*>).size} elements }"
    }

    return p.getter.call(obj).toString()
}

actual fun showProperties(obj: Any, cls: KClass<*>, level: Display, style: DisplayStyles, depth: Int, indent: Int): String
{
    val fieldSep = if (style.contains(DisplayStyle.OneLine)) ", " else "\n"

    val ret = StringBuilder()
    for (p in cls.declaredMemberProperties)
    {
        val d = p.findAnnotation<cli>()
        if (d != null)
        {
            if (d.display.v <= level.v)
            {
                //val pcls = p.getter.call(obj)!!::class
                val pcls = p.returnType.classifier
                // p.returnType.withNullability(false).findAnnotation<cli>()
                // p.returnType.withNullability(false).classifier!!
                if ((pcls != null) && ((pcls as KClass<*>).findAnnotation<cli>() == null))
                {
                    //val v = (pcls as KClass<*>).declaredMemberExtensionFunctions
                    var value = show(p, obj)
                    if (value.length > 0) value = " = " + value
                    ret.append("  ".repeat(indent) + p.name + ":" + showType(p.returnType.toString(), style) + value + showHelp(d.help, style, indent + 1) + fieldSep)
                }
                else
                {
                    p.getter.call(obj)?.let {
                        ret.append("  ".repeat(indent) + p.name + showHelp(d.help, style, indent + 1) + ": " + showObject(it,
                            level,
                            style,
                            depth - 1,
                            indent + 1) + fieldSep)
                    }
                }
            }
        }
    }
    return ret.toString()
}

fun shouldDisplay(obj: KAnnotatedElement, level: Display): Boolean
{
    val d = obj.findAnnotation<cli>() ?: return false
    return (d.display.v <= level.v)
}

fun getClassApis(cls: KClass<*>, level: Display = Display.User, style: DisplayStyles = EnumSet.of(DisplayStyle.OneLine)): String?
{
    var uniqueApis = mutableSetOf<String>()
    try
    {
        for (fn in cls.declaredMemberFunctions)
        {
            if (!shouldDisplay(fn, level)) continue
            uniqueApis.add(fn.name)
        }
    }
    catch(e:UnsupportedOperationException)
    {
    }

    try
    {
        for (fn in cls.declaredMemberExtensionFunctions)
        {
            if (!shouldDisplay(fn, level)) continue
            uniqueApis.add(fn.name)
        }
    }
    catch(e:UnsupportedOperationException)
    {
    }
    if (uniqueApis.size == 0) return null
    return uniqueApis.toSortedSet().toList().joinToString(", ")
}

actual fun showApis(obj: Any, level: Display, style: DisplayStyles): String?
{
    val ret = mutableListOf<String>()
    try
    {
        for (st in obj::class.allSuperclasses)
        {
            if (!shouldDisplay(st, level)) continue
            getClassApis(st, level, style)?.let { ret.add(it) }
        }
    }
    catch (e: java.lang.UnsupportedOperationException)
    {

    }
    getClassApis(obj::class, level, style)?.let { ret.add(it) }
    if (ret.size == 0) return null
    return ret.joinToString(", ")
}

fun showFunction(fn: KFunction<*>, level: Display = Display.User, style: DisplayStyles = DEFAULT_STYLE, indent:Int=0): Pair<String?,String>
{
    val sep = "\n"
    val paramSep = "\n"
    val d = fn.findAnnotation<cli>()
    if (d != null)
    {
        if (d.display.v <= level.v)
        {
            val help = showHelp(d.help, style, indent+1, "")
            val s = StringBuilder()
            val pNames = mutableListOf<String>()
            val pStyledNames = mutableListOf<String>()
            var memberOf = ""
            for (p in fn.parameters)
            {
                val dp = p.findAnnotation<cli>()
                if (p.name == null) memberOf = showType(p.type.toString(), style) + "."
                else
                {
                    s.append("  ".repeat(indent + 2) + p.name + " : " + showType(p.type.toString(), style) + showHelp(dp?.help, style, indent + 3, ""))
                    s.append(paramSep)
                    val n: String = p.name + ":" + p.type
                    pNames.add(n)
                    val n2: String = p.name + ":"+ showType(p.type.toString(), style)
                    pStyledNames.add(n2)
                }
            }
            val callsig = fn.name + "(" + pNames.joinToString(", ") + ")"
            return Pair(callsig, "  ".repeat(indent) + memberOf + fn.name + "(" + pStyledNames.joinToString(", ") + ") -> " + showType(fn.returnType.toString(), style) + help + sep + s.toString())
        }
    }
    return Pair(null,"")
}

fun showPropertyOf(obj: Any, p: KProperty1<*, *>, level: Display, style: DisplayStyles, depth: Int, indent: Int): String
{
    val fieldSep = if (style.contains(DisplayStyle.OneLine)) ", " else "\n"

    val ret = StringBuilder()

    val d = p.findAnnotation<cli>()
    if (d != null)
    {
        if (d.display.v <= level.v)
        {
            val pcls = p.returnType.classifier
            if ((pcls != null) && ((pcls as KClass<*>).findAnnotation<cli>() == null))
            {
                    //val v = (pcls as KClass<*>).declaredMemberExtensionFunctions
                    var value = show(p, obj)
                    if (value.length > 0) value = " = " + value
                    ret.append("  ".repeat(indent) + p.name + ":" + showType(p.returnType.toString(), style) + value + showHelp(d.help, style, indent + 1) + fieldSep)
            }
            else
            {
                    p.getter.call(obj)?.let {
                        ret.append("  ".repeat(indent) + p.name + showHelp(d.help, style, indent + 1) + ": " + showObject(it,
                            level,
                            style,
                            depth - 1,
                            indent + 1) + fieldSep)
                    }
            }
        }
    }
    return ret.toString()
}

fun getClassApiHelp(cls: KClass<*>, level: Display = Display.User, style: DisplayStyles = EnumSet.of(DisplayStyle.OneLine)): String?
{
    val sep = if (style.contains(DisplayStyle.ObjectPerLine) || style.contains(DisplayStyle.FieldPerLine)) "\n" else ", "

    var uniqueApis = sortedMapOf<String, String>()
    try
    {
        for (fn in cls.declaredMemberFunctions)
        {
            if (!shouldDisplay(fn, level)) continue
            val (callsig, desc) = showFunction(fn, level, style)
            if (callsig != null) uniqueApis[callsig] = desc
        }
    }
    catch(e:UnsupportedOperationException)
    {
    }

    try
    {
        for (fn in cls.declaredMemberExtensionFunctions)
        {
            if (!shouldDisplay(fn, level)) continue
            val (callsig, desc) = showFunction(fn, level, style)
            if (callsig != null) uniqueApis[callsig] = desc
        }
    }
    catch(e:UnsupportedOperationException)
    {
    }

    if (uniqueApis.size == 0) return null
    return uniqueApis.values.joinToString(sep)
}

fun getApiHelp(obj: Any, level: Display = Display.User, style: DisplayStyles = EnumSet.of(DisplayStyle.OneLine)): String?
{
    val sep = if (style.contains(DisplayStyle.ObjectPerLine) || style.contains(DisplayStyle.FieldPerLine)) "\n" else ", "

    val ret = mutableListOf<String>()
    try
    {
        for (st in obj::class.allSuperclasses)
        {
            if (!shouldDisplay(st, level)) continue
            getClassApiHelp(st, level, style)?.let { ret.add(it) }
        }
    }
    catch (e: java.lang.UnsupportedOperationException)
    {

    }
    getClassApiHelp(obj::class, level, style)?.let { ret.add(it) }
    if (ret.size == 0) return null
    return ret.joinToString(sep)
}

@OptIn(kotlin.time.ExperimentalTime::class)
actual fun showObject(
    obj: Any,
    level: Display,
    style: DisplayStyles,
    depth: Int,
    indent: Int,
    maxString: Int
): String
{
    if (obj is String) return "\"" + obj + "\""
    if (obj is ByteArray)
    {
        val tmp = obj.toHex().ellipsis(maxString)
        if (tmp.length == 0) return "(empty)"
        return "h" + tmp.ellipsis(maxString)
    }

    if (obj is TimeSource.Monotonic.ValueTimeMark)
    {
        val lastTime = if (obj.elapsedNow().inWholeSeconds < 5*60) obj.elapsedNow().toString(DurationUnit.SECONDS) else obj.elapsedNow().toString(DurationUnit.MINUTES)
        return lastTime + " ago"
    }
    if (isPrimitive(obj))
        return obj.toString()
    var idt = indent
    val ret = StringBuilder()
    val fieldSep = if (style.contains(DisplayStyle.OneLine)) ", " else "\n"
    // val fieldPerNewLine = if (style.contains(DisplayStyle.FieldPerLine)) "\n" else ""
    val objectSep = if (style.contains(DisplayStyle.ObjectPerLine) || style.contains(DisplayStyle.FieldPerLine)) "\n" else ", "
    val objectOpener = if (style.contains(DisplayStyle.OneLine)) "{" else "\n"
    val objectCloser = if (style.contains(DisplayStyle.OneLine)) "}" else ""

    try
    {
        if (obj is Iterable<*>)
        {
            val iter = obj.iterator()
            var i = 0
            ret.append("  ".repeat(idt) + "[" + fieldSep)
            while (iter.hasNext() && i < MAX_SHOW_COLLECTION_SIZE)
            {
                val v = iter.next()
                ret.append(showObject(v ?: throw Exception("Object v is null"), level, style, depth - 1, idt + 1) + fieldSep)
                i++
            }
            if (iter.hasNext()) ret.append("  ".repeat(idt + 1) + "..." + fieldSep)
            ret.append("  ".repeat(idt) + "]" + fieldSep)
        }
    }
    catch(e:UnsupportedOperationException)
    {
    }

    if (style.contains(DisplayStyle.FieldPerLine)) idt += 1

    try
    {
        // Handle annotations in the primary constructor
        obj::class.primaryConstructor?.let { primaryCtor ->
            val vplist: List<KParameter> = primaryCtor.valueParameters
            for (kp in vplist)
            {
                val d = kp.findAnnotation<cli>()
                if (d != null)
                {
                    if (d.display.v <= level.v)
                    {
                        val p = obj::class.memberProperties.find { it.name == kp.name }
                        var value = p?.getter?.call(obj) ?: "null"
                        ret.append(formatForDisplay(style, idt, kp.name ?:"",
                            showType(kp.type.toString(), style or DisplayStyle.TypeSeparator),
                            showHelp(d.help, style, indent + 1),
                            showObject(value, level, style, depth - 1, idt + 1) )
                            + objectSep)
                    }
                }
            }
        }

        /*
        for (p in obj::class.declaredMemberProperties)
        {
            val d = p.findAnnotation<cli>()
            if (d != null)
            {
                if (d.display.v <= level.v)
                {
                    val subObj = p.getter.call(obj)
                    // decide whether to display details of subobjects
                    if ((subObj != null) && (d.delve + depth > 0))
                        ret.append(formatForDisplay(style, idt, p.name,
                            objectType(subObj, style or DisplayStyle.TypeSeparator),
                            showHelp(d.help, style, indent + 1),
                            showObject(subObj, level, style, depth - 1, idt + 1)
                        ) + objectSep)
                    // or its summary
                    else ret.append("  ".repeat(idt) + p.name + " = " + subObj + fieldSep)
                }
            }
        }
         */
        val dct = dictify(obj,level,depth, true, true, true)
        ret.append(showAny(dct, indent, maxString))
    }
    catch(e:UnsupportedOperationException)
    {
    }

    if (ret.length > 0)  // If the object has contents, wrap it with {} in one line mode
    {
        ret.append(objectCloser);
        ret.insert(0, objectOpener);
    }
    else  // If the object has nothing, print out its toString representation.  For example, enum classes trigger this clause
    {
        return obj.toString()
    }
    return ret.toString()
}

@OptIn(kotlin.time.ExperimentalTime::class)
fun dictify(
    obj: Any,
    level: Display = Display.User,
    depth: Int = 10,
    resolve: Boolean = true,
    help: Boolean = false,
    toString: Boolean = true):Any?
{
    when (obj)
    {
        is String    -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is ByteArray -> return if (resolve) { if (toString) obj.toHex() else obj } else null
        is Int       -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is Long      -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is Float     -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is Double    -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is Char      -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is Boolean   -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is Byte      -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is Short     -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is UInt     -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is ULong     -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is UByte     -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is UShort     -> return if (resolve) { if (toString) obj.toString() else obj } else null
    }

    return dictifyType(obj::class, obj, level, depth, superTypes = true)
}

fun dictifyType(kc: KClass<out Any>, obj: Any, level: Display = Display.User,depth: Int = 10,
                superTypes:Boolean=true,
                resolve: Boolean = true,
                help: Boolean = false,
                toString: Boolean = true
):FieldDict?
{
    if (depth < 0) return null
    val ret = mutableMapOf<String, Nvh?>()
    for (p in kc.declaredMemberProperties)
    {
        val d = p.findAnnotation<cli>()
        if (d != null)
        {
            if (d.display.v <= level.v)
            {
                val subObj = p.getter.call(obj)
                // decide whether to display details of subobjects
                if ((subObj != null) && (d.delve + depth > 0))
                {
                    val v = dictify(subObj, level, depth - 1, resolve, help, toString)
                    ret[p.name] = Nvh(p.name, v, d.help)
                }
                else ret[p.name] = null
            }
        }
    }

    for (p in kc.memberFunctions)
    {
        val d = p.findAnnotation<cli>()
        if (d != null)
        {
            if (d.display.v <= level.v)
            {
                ret[p.name] = Nvh(p.name, FieldFunction, d.help)
            }
        }
    }

    for (p in kc.memberExtensionFunctions)
    {
        val d = p.findAnnotation<cli>()
        if (d != null)
        {
            if (d.display.v <= level.v)
            {
                ret[p.name] = Nvh(p.name, FieldFunction, d.help)
            }
        }
    }

    if (superTypes) for (st in kc.allSuperclasses)
    {
        val name = st.qualifiedName ?: ""
        if (!(name.contains("internal") && name.contains("kotlin")))
        {
            val sd = dictifyType(st, obj, level, depth, superTypes = false)  // every grandparent is also one of our supertypes so no need to recursively supertype
            sd?.let { ret.putAll(it) }
        }
    }

    return ret
}