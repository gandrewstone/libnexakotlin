package org.nexa.libnexakotlin

import io.ktor.network.sockets.aSocket
import java.net.Socket
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.nexa.threads.Mutex
import org.nexa.threads.millisleep
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.net.InetSocketAddress
import java.nio.channels.Selector
import java.nio.channels.SocketChannel
import javax.net.ssl.SSLSocket
import javax.net.ssl.SSLSocketFactory
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.cancellation.CancellationException

/*  todo java nio implementaion so select works
val selector = Selector.open()
actual fun connect(name: String, port: Int, connectTimeout: Int, sockOptions: (SocketOptions.() -> Unit)?)
{
    val so = SocketOptions(true, 65536, 65536, 5000)
    sockOptions?.invoke(so)

    val s = SocketChannel.open()
    s.socket().tcpNoDelay = so.noDelay
    s.socket().receiveBufferSize = so.receiveBufferSize
    s.socket().sendBufferSize = so.sendBufferSize
    s.socket().soTimeout = so.timeout
    s.connect(InetSocketAddress(name, port))
    s.finishConnect()
    sock = s
    inp = s.getInputStream()
    out = s.getOutputStream()

}
*/


actual fun awaitAvailable(cnxns: Collection<iTcpSocket>, timeout: Long): Boolean
{
    //return true
    // Java sockets does not have a select equivalent so we do a backoff poll.
    val end = if (timeout == Long.MAX_VALUE) timeout else millinow() + timeout
    var waitAmt = 1UL
    while(millinow() < end)
    {
        for (c in cnxns)
        {
            if (!c.isAlive()) return true
            if (c.availableForRead > 0) return true
        }
        millisleep(waitAmt)
        if (waitAmt < 15UL) waitAmt++
    }
    return false
}

actual fun TcpSocket(sendCtxt: CoroutineContext, receiveCtxt: CoroutineContext):iTcpSocket
{
    return TcpSocketJvm()
}

class TcpSocketJvm constructor():iTcpSocket
{
    var sock: Socket? = null
    var inp: InputStream? = null
    var out: OutputStream? = null
    var tls: Boolean = false

    override fun connect(name: String, port: Int, connectTimeout: Int, sockOptions: (SocketOptions.() -> Unit)?)
    {
        val so = SocketOptions(true, 65536, 65536, 5000)
        sockOptions?.invoke(so)
        val s = if (so.tls)
        {
            tls = true
            val sslSock = SSLSocketFactory.getDefault().createSocket() as SSLSocket
            sslSock
        }
        else
        {
            val s = Socket()
            s
        }
        s.tcpNoDelay = so.noDelay
        s.receiveBufferSize = so.receiveBufferSize
        s.sendBufferSize = so.sendBufferSize
        s.soTimeout = so.timeout
        s.connect(InetSocketAddress(name, port), connectTimeout)

        sock = s
        inp = s.getInputStream()
        out = s.getOutputStream()
    }

    override fun isAlive():Boolean
    {
        val s = sock
        if (s == null) return true // we call a connection that has not initialized yet alive
        if (s.isClosed) return false
        if (!s.isConnected) return false
        return true
    }

    override fun close()
    {
        // Exceptions mean already shutdown
        try { inp?.close() } catch(_:Exception) {}
        inp = null
        try { out?.close() } catch(_:Exception) {}
        out = null
        try { sock?.close() } catch (_: Exception) { }
        sock = null
    }

    override fun finalize()
    {
        close()
    }

    /** Returns number of bytes that can be read without blocking */
    override val availableForRead: Int
        get()
        {
            if (tls) return -1  // Available does not work for TLS/SSL sockets
            val input = inp ?: return 0
            val available = try
            {
                input.available()
            }
            catch (e: IOException)
            {
                logThreadException(e, "available error")
                // Note: Although the available() documentation (https://docs.oracle.com/javase/8/docs/api/java/io/InputStream.html#available--)
                // implies that this exception is only thrown on close  it appears to be thrown at undetermined non-fatal times.
                // So model the exception as if no data is currently available.
                //LogIt.warning(sourceLoc() + ": IO exception getting available bytes on socket")
                0
            }
            return available
        }

    override fun readAvailable(dst: ByteArray, offset: Int, length: Int): Int
    {
        val amt = inp?.read(dst,offset,length) ?: -1
        return amt
    }

    /** Writes all src bytes and suspends until all bytes written. Causes flush if buffer filled up or when autoFlush Crashes if channel get closed while writing
     */
    override fun writeFully(src: ByteArray, offset: Int, length: Int)
    {
        val tmp = out
        if (tmp != null)
        {
            tmp.write(src, offset, length)
            tmp.flush()
        }
    }

    /** force any writes to actually be sent */
    override fun flush()
    {
        out?.flush()
    }

    override val localAddress: SocketAddress
        get()
        {
            return SocketAddress(sock?.localAddress?.toString() ?: "")
        }
    override val remoteAddress: SocketAddress
        get()
        {
            return SocketAddress(sock?.inetAddress?.toString() ?: "")
        }
}
