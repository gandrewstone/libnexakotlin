package org.nexa.libnexakotlin

import io.ktor.http.decodeURLQueryComponent
import io.ktor.http.encodeURLParameter
import kotlinx.datetime.*
import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig

actual val supportsTLS: Boolean = true

actual fun PlatformHttpClient(block: HttpClientConfig<*>.() -> Unit): HttpClient
{
    return HttpClient(block)
}

class NativeLinuxLogging(override val module: String):iLogging
{ override fun error(s: String)
    {
       println("[$module] ERROR:    $s")
    }

    override fun warning(s:String)
    {
        println("[$module] WARNING: $s")
    }
    override fun info(s:String)
    {
        println("[$module] INFO:    $s")
    }
}

/** Returns seconds since the epoch */
actual fun epochSeconds(): Long = Clock.System.now().epochSeconds

/** Returns milliseconds since the epoch */
actual fun epochMilliSeconds(): Long = Clock.System.now().toEpochMilliseconds()

actual fun String.urlEncode(): String
{
    return encodeURLParameter(true)
}

fun Byte.urlEncode(): String {
    return "%" + byteArrayOf(this).toHex()
}

actual fun String.urlDecode():String
{
    return decodeURLQueryComponent(plusIsSpace = true)
}

actual fun sourceLoc(depth: Int): String
{
    return ""
}

actual fun GetLog(module: String): iLogging = NativeLinuxLogging(module)

actual fun generateBip39Seed(wordseed: String, passphrase: String, size: Int): ByteArray
{
    val salt = ("mnemonic".toCharArray() + passphrase.toCharArray())
    return salt.map { it.code.toByte() }.toByteArray()
        .let { saltb ->
            PBEKeySpecCommon(wordseed.toCharArray(), saltb, 2048, 512).let { pbeKeySpec ->
                SecretKeyFactoryCommon.getInstance("PBKDF2WithHmacSHA512", FallbackProvider()).let { keyFactory ->
                    keyFactory.generateSecret(pbeKeySpec).encoded.also {
                        pbeKeySpec.clearPassword()
                    }
                }
            }
        }
}

/** Return false if this node has no internet connection, or true if it does or null if you can't tell */
actual fun iHaveInternet(): Boolean?
{
    return null
}

private var isInitialized = false
