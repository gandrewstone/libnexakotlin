
CREATE TABLE NexaBlockHeaderTbl (
    hash BLOB NOT NULL PRIMARY KEY, -- Stored as BLOB in db, retrieved as ByteArray
    hashPrevBlock BLOB, -- Stored as BLOB in db, retrieved as ByteArray
    diffBits INTEGER NOT NULL, -- Stored as INTEGER in db, retrieved as Long
    hashAncestor BLOB, -- Stored as BLOB in db, retrieved as ByteArray
    hashTxFilter BLOB, -- Stored as BLOB in db, retrieved as ByteArray
    time INTEGER NOT NULL, -- Stored as INTEGER in db, retrieved as Long
    height INTEGER NOT NULL, -- Stored as INTEGER in db, retrieved as Long
    chainWork BLOB NOT NULL, --  AS BigInteger NOT NULL,
    size INTEGER NOT NULL, -- Stored as INTEGER in db, retrieved as Long
    feePoolAmt INTEGER NOT NULL, -- Stored as INTEGER in db, retrieved as Long
    utxoCommitment BLOB, -- Stored as BLOB in db, retrieved as ByteArray
    minerData BLOB, -- Stored as BLOB in db, retrieved as ByteArray
    nonce BLOB, -- Stored as BLOB in db, retrieved as ByteArray
    hashMerkleRoot BLOB, -- Stored as BLOB in db, retrieved as ByteArray
    txCount INTEGER NOT NULL -- Stored as INTEGER in db, retrieved as Long
);

insert:
INSERT INTO NexaBlockHeaderTbl(hash, hashPrevBlock, diffBits, hashAncestor, hashTxFilter, time, height, chainWork, size, feePoolAmt, utxoCommitment, minerData, nonce, hashMerkleRoot, txCount)
VALUES(?, ?, ?,  ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);

upsert:
INSERT OR REPLACE INTO NexaBlockHeaderTbl(hash, hashPrevBlock, diffBits, hashAncestor, hashTxFilter, time, height, chainWork, size, feePoolAmt, utxoCommitment, minerData, nonce, hashMerkleRoot, txCount)
VALUES(?, ?, ?,  ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);


update:
UPDATE NexaBlockHeaderTbl
SET hashPrevBlock = :hashPrevBlock,
    diffBits = :diffBits,
    hashAncestor = :hashAncestor,
    hashTxFilter = :hashTxFilter,
    time = :time,
    height = :height,
    chainWork = :chainWork,
    size = :size,
    feePoolAmt = :feePoolAmt,
    utxoCommitment = :utxoCommitment,
    minerData = :minerData,
    nonce = :nonce,
    hashMerkleRoot = :hashMerkleRoot,
    txCount = :txCount
WHERE hash = :hash;

getAll:
SELECT NexaBlockHeaderTbl.*
FROM NexaBlockHeaderTbl;

getAtHeight:
SELECT NexaBlockHeaderTbl.*
FROM NexaBlockHeaderTbl
WHERE height IN (:heights);

getByHeight:
SELECT NexaBlockHeaderTbl.*
FROM NexaBlockHeaderTbl
WHERE height = :height;

getByHash:
SELECT NexaBlockHeaderTbl.*
FROM NexaBlockHeaderTbl
WHERE hash = :hash;

getByLatestHeight:
SELECT a.*
FROM NexaBlockHeaderTbl a
LEFT OUTER JOIN NexaBlockHeaderTbl b
ON a.height < b.height
WHERE b.height IS NULL;

getByMostWork:
SELECT a.*
FROM NexaBlockHeaderTbl a
LEFT OUTER JOIN NexaBlockHeaderTbl b
ON a.chainWork < b.chainWork
WHERE b.chainWork IS NULL;

deleteAll:
DELETE FROM NexaBlockHeaderTbl;