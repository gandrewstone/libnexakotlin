// Copyright (c) 2024 Bitcoin Unlimited
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.

@file:OptIn(ExperimentalUnsignedTypes::class)

package org.nexa.libnexakotlin

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel

private val LogIt = GetLog("BU.dest")

private val EmptyByteArray = byteArrayOf()
private val EmptySecret = UnsecuredSecret(byteArrayOf())  // TODO make a special object that asserts if you use

/** A destination for payments.  This includes all the information needed to send and spend from this destination.
It is often assumed that Bitcoin addresses are payment destinations, but this is not true.  You really need an output script, and also (if P2SH) a redeem script.
The reason why P2PKH addresses "work" as payment destinations is because they imply a specific script.  But "baking in" this assumption will make the wallet a
lot less flexible.

A note on terminology:

For legacy and terminology compatibility with other chains "Locking" and "Unlocking" scripts will refer to the entire output and input "scripts" in the transaction.  So in nexa they are not really scripts (even though they are serialized in the same format), they really a pile of data items.

The "template script" is the main script that controls spending, created by the covenant author or the holder (if not covenanted).
The "constraint script" is additional constraining arguments that the owner is allowed to apply to the template.
The "satisfier script" is the final args that "satisfy" (by correct execution) the template/constraint script combo.

So the locking script contains the group info, the hash of the template script, the hash of the hidden constraint script (actually args), and additional visible constraint script (actually args).
On the spending side, the "unlocking script" contains the actual template script, the actual constraint script, and the satisfier script.
 */
@cli(Display.Simple, "A destination for payments.  This includes all the information needed to send and spend, although you may need to unlock the wallet when spending.  If you do not know the spending information (its someone else's address), use the PayAddress class.")
abstract class PayDestination(val chainSelector: ChainSelector) : BCHserializable
{
    companion object
    {
        fun GetPubKey(privateKey: ByteArray): ByteArray = libnexa.getPubKey(privateKey)

        /** Deserializes a PayDestination of the correct derived class */
        fun from(ser:BCHserialized, chainSelector: ChainSelector):PayDestination
        {
            val destType = ser.debyte().toInt()
            val idx = ser.deint64()
            return when (destType)
            {
                Pay2PubKeyTemplateDestination.DEST_TYPE -> {
                    val ret = Pay2PubKeyTemplateDestination(chainSelector, ser)
                    ret.index = idx
                    ret
                }
                Pay2PubKeyHashDestination.DEST_TYPE ->
                {
                    val ret = Pay2PubKeyHashDestination(chainSelector, ser)
                    ret.index = idx
                    ret
                }
                InteractiveMultisigDestination.DEST_TYPE ->
                {
                    val ret = InteractiveMultisigDestination(chainSelector, ser)
                    ret.index = idx
                    ret
                }
                // Pay2ScriptHashDestination.DEST_TYPE -> Pay2ScriptHashDestination(chainSelector, it)
                // MultisigDestination.DEST_TYPE -> MultisigDestination(chainSelector, it)
                else -> throw DeserializationException("destination type not handled")
            }
        }
    }

    /** serialize any derived class & the type indicator */
    fun serializeTypeAndDerived(st: SerializationType):BCHserialized
    {
        assert(st == SerializationType.DISK)
        return BCHserialized(st).addUint8(derivedType).addInt64(index).add(serializeDerived((st)))
    }
    /** serialize any derived class & the type indicator */
    fun serializeTypeAndDerived(ser: BCHserialized):BCHserialized
    {
        assert(ser.format == SerializationType.DISK)
        return ser.addUint8(derivedType).addInt64(index).add(serializeDerived(ser.format))
    }

    abstract fun serializeDerived(format: SerializationType): BCHserialized

    // Store this object into a BCHserialized stream and return it
    override fun BCHserialize(format: SerializationType): BCHserialized = serializeTypeAndDerived(format)

    // Load this object with data from the passed stream
    override open fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        throw NotImplementedError()
    }

    /** If a Bip44 wallet, this is be the BIP44 index.  Otherwise its the an incrementing integer every time a new destination is created */
    open var index: Long = 0
    /** What type of destination is this? */
    open val derivedType: Int = 0
    /** An indication to retire this address as soon as coins are received (typically used for change). RAM only */
    var oneUse:Boolean = false

    /** All derived classes that require the execution of a potentially interactive protocol to spend should set this to false and implement member fns */
    open val atomicSpending: Boolean = true

    /** If atomicSpending is false, this is how you initiate whatever protocol is needed to sign this transaction.
     * This should only be called if this is your own spending proposal (it is expected that the user has already agreed to this spend) */
    open fun beginProposal(tx:iTransaction, inputIdx: Int): SpendingProposal?
    {
        return null  // a proposal is not needed
    }

    /** Get the template script needed to spend this destination.  This script is committed to in the locking script via hash, and must be provided in full in the
     * unlocking script.  For P2SH blockchains, this will return the redeemScript since templates are a generalization of P2SH.
     * @return an empty script if this is not a template destination */
    @cli(Display.Simple, "Get the template script needed to spend this destination, or an empty script if not a template destination")
    open fun templateScript(): SatoshiScript = SatoshiScript(chainSelector)

    /** Get the [locking script](https://bitcoinunlimited.net/glossary/output_script) needed to send coins to this destination.  This script will contain spend constraints in the non-template case, or in the
     * template/P2SH case this script contains the hash of the template/redeem script.
     * @return the outputscript needed to constrain coins to this destination */
    open fun lockingScript(): SatoshiScript = SatoshiScript(chainSelector)

    //@Deprecated("use lockingScript")
    //open fun outputScript(): SatoshiScript = SatoshiScript(chainSelector)

    /** Get the [locking script](https://bitcoinunlimited.net/glossary/output_script) needed to send coins to this destination.  This script will contain spend constraints in the non-template/P2SH case, or in the
     * that case this script only constrains spending to execution of the template/redeem script and any additional constraints are located there.
     * @return the outputscript needed to constrain coins to this destination */
    //@cli(Display.Simple, """"Get the [constraint script](https://bitcoinunlimited.net/glossary/output_script) needed to send coins to this destination.  This is also called the "output script" or the "scriptPubKey".""")
    //fun constraintScript(): SatoshiScript = lockingScript()

    /** Get the [output script](https://bitcoinunlimited.net/glossary/output_script) needed to send tokens to this group destination.  This script will contain spend constraints in the non-P2SH case, or in the
     * [P2SH](https://bitcoinunlimited.net/glossary/P2SH) case this script only constrains spending to execution of the redeemScript and any additional constraints are located in the redeemScript
     * @return the outputscript needed to constrain coins to this destination */
    open fun groupedLockingScript(groupId: GroupId, groupAmount: Long): SatoshiScript = throw WalletNotSupportedException("this script destination cannot be grouped")

    /** Get the [output script](https://bitcoinunlimited.net/glossary/output_script) needed to send native coins to this destination.  This script will contain spend constraints in the non-P2SH case, or in the
     * [P2SH](https://bitcoinunlimited.net/glossary/P2SH) case this script only constrains spending to execution of the redeemScript and any additional constraints are located in the redeemScript
     * @return the outputscript needed to constrain coins to this destination */
    open fun ungroupedLockingScript(): SatoshiScript = lockingScript()

    /** Create a spend (input) script that will satisfy the constraints specified by the [lockingScript] and the [templateScript] (if applicable).  This script will contain the redeemScript in the P2SH case.
     * @param [params] provide needed, script-specific args in the params field, like the signature
     */
    open fun unlockingScript(vararg params: ByteArray): SatoshiScript = SatoshiScript(chainSelector)

    /** Create a spend (input) script that will satisfy the constraints specified by the [lockingScript] and the [templateScript] (if applicable).  This script will contain the redeemScript in the P2SH case.
     * Many scripts require signatures.  This API provides the data needed for this class to craft signatures, and provides additional derived-class defined params.
     * @param [spendingProposal] If this destination requires additional information to spend ([atomicSpending] == false), you cannot successfully call unlockingScript until you have a completed SpendingProposal
     */
    open fun unlockingScript(flatTx: ByteArray, inputIdx: Long, sigHashType: ByteArray, inputAmount: Long, spendingProposal: SpendingProposal? = null): SatoshiScript = SatoshiScript(chainSelector)


    @cli(Display.Simple, """Get the push-only script that solves the constraintScript.  This is a part of the total unlockingScript placed in transaction inputs""")
    open fun satisfierScript(flatTx: ByteArray, inputIdx: Long, sigHashType: ByteArray, inputAmount: Long, spendingProposal: SpendingProposal? = null): SatoshiScript = SatoshiScript(chainSelector)

    /**
     * Calculate the public key hash (PKH)
     */
    @cli(Display.Simple, """Return the public key hash (if this destination has a public key) otherwise return null""")
    fun pkh(): ByteArray?
    {
        pubkey?.let {
            return libnexa.hash160(it)
        }
        return null
    }

    @cli(Display.Simple, """Return the hash of the constraint args script (if this is a template destination) otherwise return null""")
    open fun constraintArgsHash(): ByteArray?
    {
        return null
    }

    /** Get the [P2SH](%budoc/glossary/P2SH) or [P2PKH](%budoc/glossary/P2PKH) address associated with this destination
     *  Note that only a subset of PayDestinations have addresses.  A PayAddress will only exist if this destination constrains spending to require a signature (in the P2PKH case) or a script (P2SH case)
     *  @return Payment address associated with this destination or null if no address exists
     */
    @cli(Display.Simple, """Return the address (if this destination has an address) otherwise return null.""")
    open val address: PayAddress?
        get() = null

    /** Return a set of bytes that can be put into a bloom filter to select any transaction that contains this destination */
    open val bloomFilterData: ByteArray?
        get() = null

    /** Get an output (constrained to this destination) suitable for placing in a transaction  */
    @cli(Display.Simple, """Get an output (constrained to this destination) suitable for placing in a transaction""")
    open fun output(satoshis: Long): iTxOutput
    {
        return txOutputFor(chainSelector, satoshis, ungroupedLockingScript())
    }

    /** Get the public key if a single signature is needed to spend this destination
     *   @return If no pubkey exists, returns null
     */
    @cli(Display.Simple, """Get the public key if a single signature is needed to spend this destination, otherwise null""")
    abstract val pubkey: ByteArray?

    /** Get the secret key if a single signature is needed to spend this destination
     *   @return If no secret is needed to spend, return null
     */
    @cli(Display.Simple, """Get the secret if a single signature is needed to spend this destination, otherwise null""")
    abstract val secret: Secret?
}

/** Represents a "standard" P2PKH payment destination
 *
 */
class Pay2PubKeyHashDestination(chainSelector: ChainSelector, override var secret: Secret, idx: Long) : PayDestination(chainSelector)
{
    init
    {
        index = idx
    }
    companion object
    {
        const val DEST_TYPE = 1 // For object identification when serialized
    }

    override val derivedType = DEST_TYPE

    override var pubkey = GetPubKey(secret.getSecret())
    var pubkeyHash = libnexa.hash160(pubkey)

    constructor(chainSelector: ChainSelector, stream: BCHserialized) : this(chainSelector, UnsecuredSecret(stream.deByteArray()),0)
    {
    }

    override fun equals(other: Any?): Boolean
    {
        return contentEquals(other)
    }

    fun contentEquals(other: Any?): Boolean
    {
        if (other == null) return false
        if (other is Pay2PubKeyHashDestination)
        {
            // Check pubkeys here because the secret may require unlocking
            return (pubkey contentEquals other.pubkey)
        }
        return false
    }

    fun updateSecret(s: Secret)
    {
        secret = s
        pubkey = GetPubKey(secret.getSecret())
        pubkeyHash = libnexa.hash160(pubkey)
    }

    override fun serializeDerived(format: SerializationType): BCHserialized
    {
        // check format so the secret isn't accidentally sent over the network
        if (format == SerializationType.DISK)
        {
            return BCHserialized(format) + variableSized(secret.getSecret())  // TODO encrypt secret
        }
        throw NotImplementedError()
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        val s = BCHserialized(SerializationType.UNKNOWN).deByteArray()
        updateSecret(UnsecuredSecret(s))
        return stream
    }

    override fun templateScript(): SatoshiScript
    {
        return SatoshiScript(chainSelector)
    }

    override fun lockingScript(): SatoshiScript
    {
        return SatoshiScript.p2pkh(pubkeyHash, chainSelector)
    }

/*
    fun spendScriptECDSA(flatTx: ByteArray, inputIdx: Long, sigHashType: Int, inputAmount: Long, vararg params: ByteArray): SatoshiScript
    {
        assert(params.size == 0)  // params is unused in this contract
        val sig = libnexa.signBchTxOneInputUsingECDSA(flatTx, sigHashType, inputIdx, inputAmount, ungroupedOutputScript().flatten(), secret.getSecret())
        val pubkey = pubkey // PayDestination.GetPubKey(secret.getSecret())
        return SatoshiScript(chainSelector, SatoshiScript.Type.PUSH_ONLY, OP.push(sig), OP.push(pubkey))
    }
*/
    override fun unlockingScript(flatTx: ByteArray, inputIdx: Long, sigHashType: ByteArray, inputAmount: Long, spendingProposal: SpendingProposal?): SatoshiScript
    {
        assert(spendingProposal == null)  // unused in this contract
        val sig = if (chainSelector.isBchFamily)
        {
            if (sigHashType.size != 0) throw UnimplementedException("Only sighash all is supported in BCH chain")
            libnexa.signBchTxOneInputUsingSchnorr(flatTx, BCH_SIGHASH_ALL, inputIdx, inputAmount, ungroupedLockingScript().flatten(), secret.getSecret())
        }
        else
        {
            libnexa.signTxOneInputUsingSchnorr(flatTx, sigHashType, inputIdx, inputAmount, ungroupedLockingScript().flatten(), secret.getSecret())
        }
        assert(pubkey contentEquals PayDestination.GetPubKey(secret.getSecret()))
        return SatoshiScript(chainSelector, SatoshiScript.Type.PUSH_ONLY, OP.push(sig), OP.push(pubkey))
    }

    override val bloomFilterData: ByteArray?
        get() = pubkeyHash

    override val address: PayAddress
        get() = PayAddress(chainSelector, PayAddressType.P2PKH, pubkeyHash)
}

/** Represents a "standard" P2PKT payment destination
 *
 */
class Pay2PubKeyTemplateDestination(chainSelector: ChainSelector, override var secret: Secret, idx: Long) : PayDestination(chainSelector)
{
    init
    {
        index = idx
    }

    companion object
    {
        const val DEST_TYPE = 2 // For object identification when serialized
    }

    override val derivedType = DEST_TYPE

    override var pubkey:ByteArray? = null
        get() {
            if (field == null) field = GetPubKey(secret.getSecret())
            return field
        }
    fun pubkeyOrThrow():ByteArray
    {
        return pubkey ?: GetPubKey(secret.getSecret())
    }

    var groupId: GroupId? = null
    var scriptCode = SatoshiScript(chainSelector, SatoshiScript.Type.SATOSCRIPT, OP.FROMALTSTACK, OP.CHECKSIGVERIFY).flatten()

    constructor(chainSelector: ChainSelector, stream: BCHserialized) : this(chainSelector, EmptySecret, 0)
    {
        BCHdeserialize(stream)
    }

    fun updateSecret(s: Secret, pubK: ByteArray? = null)
    {
        secret = s
        if ((pubK != null) && (pubK.size > 0)) pubkey = pubK
        else pubkey = null
    }

    override fun equals(other: Any?): Boolean
    {
        return contentEquals(other)
    }

    fun contentEquals(other: Any?): Boolean
    {
        if (other == null) return false
        if (other is Pay2PubKeyTemplateDestination)
        {
            // Check pubkeys here because the secret may require unlocking
            return (pubkey contentEquals other.pubkey)
        }
        return false
    }

    override fun serializeDerived(format: SerializationType): BCHserialized
    {
        // check format so the secret isn't accidentally sent over the network
        if (format == SerializationType.DISK)
        {
            // serialize the pubkey in case the secret is encrypted so we don't have continual access to it.
            val pub:ByteArray = pubkey ?: EmptyByteArray
            return BCHserialized(format).add(variableSized(secret.getSecret())).add(variableSized(pub))
        }
        throw NotImplementedError()
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
         if (stream.format == SerializationType.DISK)
         {
             val s = stream.deByteArray()
             val p = stream.deByteArray()
             updateSecret(UnsecuredSecret(s), p)
         }
        else throw NotImplementedError()
        return stream
    }

    override fun templateScript(): SatoshiScript
    {
        return SatoshiScript(chainSelector)
    }

    override fun lockingScript(): SatoshiScript
    {
        return ungroupedLockingScript()
    }

    override fun ungroupedLockingScript(): SatoshiScript
    {
        return SatoshiScript.ungroupedP2pkt(chainSelector, pubkeyOrThrow())
    }

    override fun groupedLockingScript(groupId: GroupId, groupAmount: Long): SatoshiScript
    {
        return SatoshiScript.gp2pkt(chainSelector, groupId, groupAmount, pubkeyOrThrow())
    }

    @cli(Display.Simple, """Return the template args hash (if this is a template destination) otherwise return null""")
    override fun constraintArgsHash(): ByteArray
    {
        val constraintArgsScript = SatoshiScript(chainSelector, SatoshiScript.Type.PUSH_ONLY, OP.push(pubkeyOrThrow()))
        val constraintArgsHash = libnexa.hash160(constraintArgsScript.toByteArray())
        return constraintArgsHash
    }

    override fun unlockingScript(flatTx: ByteArray, inputIdx: Long, sigHashType: ByteArray, inputAmount: Long, spendingProposal: SpendingProposal?): SatoshiScript
    {
        assert(spendingProposal == null)  // params is unused in this contract
        val sig = libnexa.signTxOneInputUsingSchnorr(flatTx, sigHashType, inputIdx, inputAmount, scriptCode, secret.getSecret())
        val args = SatoshiScript(chainSelector, SatoshiScript.Type.PUSH_ONLY, OP.push(pubkeyOrThrow())).flatten()
        return SatoshiScript(chainSelector, SatoshiScript.Type.PUSH_ONLY, OP.push(args), OP.push(sig))
    }

    override val bloomFilterData: ByteArray?
        get()
        {
            val constraintArgsScript = SatoshiScript(chainSelector, SatoshiScript.Type.PUSH_ONLY, OP.push(pubkeyOrThrow()))
            return libnexa.hash160(constraintArgsScript.toByteArray())
        }

    override val address: PayAddress
        get()
        {
            val gid = groupId
            val scr: SatoshiScript =
              if (gid != null) SatoshiScript.gp2pkt(chainSelector, gid, 0, pubkeyOrThrow())  // group amount 0 in this case implies that the address does not specify an amount (which would be weird)
              else SatoshiScript.ungroupedP2pkt(chainSelector, pubkeyOrThrow())
            return PayAddress(chainSelector, PayAddressType.TEMPLATE, scr.asSerializedByteArray())
        }
}


open class Pay2TemplateDestination(chain: ChainSelector):PayDestination(chain)
{
    companion object
    {
        const val DEST_TYPE = 5 // For object identification when serialized
    }

    override val derivedType = DEST_TYPE

    override var secret: Secret? = null // Not applicable for this destination
    override val pubkey: ByteArray? = null  // Not applicable for this destination

    var group: GroupInfo? = null

    // One of these 2 must be null, the other set to a value
    protected var wellKnownTemplate: OP? = null
    protected var template: SatoshiScript? = null

    // If null, there is no hidden constraint arguments
    protected var constraint: SatoshiScript? = null
    protected var visibleConstraint = SatoshiScript(chainSelector)

    var useHash160ForTemplate = true
    var useHash160ForConstraintArgs = true

    /** Get the [P2SH](%budoc/glossary/P2SH) or [P2PKH](%budoc/glossary/P2PKH) address associated with this destination
     *  Note that only a subset of PayDestinations have addresses.  A PayAddress will only exist if this destination constrains spending to require a signature (in the P2PKH case) or a script (P2SH case)
     *  @return Payment address associated with this destination or null if no address exists
     */
    @cli(Display.Simple, """Return the address (if this destination has an address) otherwise return null.""")
    override val address: PayAddress
        get()
        {
            val scr: SatoshiScript = lockingScript()
            return PayAddress(chainSelector, PayAddressType.TEMPLATE, scr.asSerializedByteArray())
        }

    /** Return a set of bytes that can be put into a bloom filter to select any transaction that contains this destination */
    override val bloomFilterData: ByteArray?
        get()
        {
            val cas = constraint
            val tmpl = template
            if (cas != null)
            {
                val constraintArgsScriptHash = cas.let { if (useHash160ForConstraintArgs) it.scriptHash160() else it.scriptHash().hash }
                return constraintArgsScriptHash
            }
            else if (tmpl != null)
            {
                val templateScriptHash = tmpl.let { if (useHash160ForTemplate) it.scriptHash160() else it.scriptHash().hash }
                return templateScriptHash
            }
            else // There is nothing that can filter for this tx, its too generic (its very likely broken)
                throw DataMissingException("constraint and template missing")
        }

    override fun serializeDerived(format: SerializationType): BCHserialized
    {
        val ret = BCHserialized(format)
        //ret.addNullable(group)
        ret.addNullableVariableSized(wellKnownTemplate?.v)
        ret.addNullable(template)
        ret.addNullable(constraint)
        ret.addNullable(visibleConstraint)
        ret.add(useHash160ForTemplate).add(useHash160ForConstraintArgs)
        return ret
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        if (stream.format == SerializationType.DISK)
        {
            group = null
            val ba:ByteArray? = stream.denullByteArray()
            wellKnownTemplate = ba?.let { OP(ba)}
            template = stream.deNullable { SatoshiScript(chainSelector, it) }
            constraint = stream.deNullable { SatoshiScript(chainSelector, it) }
            visibleConstraint = stream.deNullable { SatoshiScript(chainSelector, it) } ?: SatoshiScript(chainSelector)
            useHash160ForTemplate = stream.deboolean()
            useHash160ForConstraintArgs = stream.deboolean()
        }
        else throw NotImplementedError()
        return stream
    }


    override fun templateScript(): SatoshiScript
    {
        template?.let { return it }
        wellKnownTemplate?.let {
            return when(it)
            {
                P2PKT_ID -> SatoshiScript(chainSelector, SatoshiScript.Type.SATOSCRIPT, OP.FROMALTSTACK, OP.CHECKSIGVERIFY)
                else -> throw DataMissingException("unknown well known template")
            }
        }
        throw DataMissingException("template not specified")
    }

    override fun lockingScript(): SatoshiScript
    {
        val templateScriptHash = (wellKnownTemplate?.let { it.v }) ?: template?.let { if (useHash160ForTemplate) it.scriptHash160() else it.scriptHash().hash } ?: throw DataMissingException("template missing")
        val constraintArgsScriptHash = constraint?.let { if (useHash160ForConstraintArgs) it.scriptHash160() else it.scriptHash().hash }
        val script = SatoshiScript.p2t(chainSelector, templateScriptHash, constraintArgsScriptHash, visibleConstraint)
        return script
    }

    override fun groupedLockingScript(groupId: GroupId, groupAmount: Long): SatoshiScript
    {
        val templateScriptHash = (wellKnownTemplate?.let { it.v }) ?: template?.let { if (useHash160ForTemplate) it.scriptHash160() else it.scriptHash().hash } ?: throw DataMissingException("template missing")
        val constraintArgsScriptHash = constraint?.let { if (useHash160ForConstraintArgs) it.scriptHash160() else it.scriptHash().hash } ?: OP.PUSHFALSE.v
        val script = SatoshiScript.p2t(chainSelector, templateScriptHash, constraintArgsScriptHash, visibleConstraint, groupId, groupAmount)
        return script
    }

    /** Create a spend (input) script that will satisfy the constraints specified by the [lockingScript] and the [templateScript] (if applicable).  This script will contain the redeemScript in the P2SH case.
     * @param [params] provide needed, script-specific args in the params field, like the signature
     */
    override fun unlockingScript(vararg params: ByteArray): SatoshiScript
    {
        throw NotImplementedError()
    }

    /** Create a spend (input) script that will satisfy the constraints specified by the [lockingScript] and the [templateScript] (if applicable).  This script will contain the redeemScript in the P2SH case.
     * Many scripts require signatures.  This API provides the data needed for this class to craft signatures, and provides additional derived-class defined params.
     * @param [params] provide needed, script-specific args in the params field
     */
    override fun unlockingScript(flatTx: ByteArray, inputIdx: Long, sigHashType: ByteArray, inputAmount: Long, spendingProposal: SpendingProposal?): SatoshiScript
    {
        // override this based on your template
        return SatoshiScript(chainSelector)
        /* TODO

        val ret = SatoshiScript(chainSelector)
        template?.let { ret.add(OP.push(it.toByteArray())) }
        constraint?.let { ret.add(OP.push(it.toByteArray())) }
        val satisfier =  satisfierScript(flatTx, inputIdx, sigHashType, inputAmount, *params)
        if (satisfier != null )
            return ret + satisfier
        else return ret

         */
    }

    @cli(Display.Simple, """Get the push-only script that solves the template with the constraint args script.  This is a part of the total unlockingScript placed in transaction inputs""")
    override fun satisfierScript(flatTx: ByteArray, inputIdx: Long, sigHashType: ByteArray, inputAmount: Long, spendingProposal: SpendingProposal?): SatoshiScript
    {
        // override this based on your template
        return SatoshiScript(chainSelector)
    }


}

fun formMultisig(cs:ChainSelector, requiredSigs: Int, dests:Array<Bytes>): SatoshiScript
{
    assert(requiredSigs <= dests.size) // or it is impossible to unlock
    assert(requiredSigs > 0) // Well if its 0 its anyone can spend which is likely not intended
    val ss = SatoshiScript(cs, SatoshiScript.Type.SATOSCRIPT, OP.push(requiredSigs))
    dests.forEach { ss.add(OP.push(it.data))}
    ss.add(OP.push(dests.size)).add(OP.CHECKMULTISIGVERIFY)
    return ss
}


// by sending increasing integers down the wakeup channel, this lets you track wakeup notices to actual wakeups (for debugging)
var MultisigWakeupCount = 0

class MultisigSpendingProposal(tx:iTransaction, val dest: InteractiveMultisigDestination, input:Int, onAccept: SpendingProposal.()->Unit)
:SpendAnInput(tx, dest, input, onAccept)
{
    var launchIn: CoroutineScope? = newSpendingProposalScope()
    val flatUnsignedTx = tx.toByteArray()
    val sigs: HashMap<Bytes,ByteArray> = hashMapOf()  // key is a pubkey, value is the corresponding sig
    val wakeup = Channel<Int>(10)
    var sigHashType = NexaSigHashType().all().build()
    fun addSig(pubkey: Bytes, sig: ByteArray)
    {
        val d = (destination as InteractiveMultisigDestination)
        if (!d.pubkeys.contains(pubkey))
            throw IllegalArgumentException("Added nonexistent pubkey to MultisigSpendingProposal")

        // If ever we are able to extend the sigs we know, echo those to the other participants.
        // This is inefficient because we echo stuff we receive from others.  But also that serves to echo a
        // consolidation of all known sigs in case a participant missed someone else's message.  If the number of
        // multisig participants grows, this redundant echoing may become a problem, in which case, move
        // issueCompletionRequest() in "accept" after dest.addMySigs(this) to only echo when I send my own sigs.
        if (!sigs.contains(pubkey))
        {
            val wakeId = MultisigWakeupCount++
            LogIt.info(sourceLoc() + ": initiate coroutine wake up ${wakeId}: adding sig ${pubkey.data.toHex()} to ${sigs}")
            // TODO check that the sig is valid
            sigs[pubkey] = sig
            launch(launchIn) { issueCompletionRequest() }
            wakeup.trySend(wakeId)
        }
    }

    suspend fun issueCompletionRequest()
    {
        //val tx = txFor(dest.chainSelector, BCHserialized(SerializationType.NETWORK,flatTx))
        LogIt.info(sourceLoc() + ": issueCompletionRequest")
        val reason = "spend multisig" // TODO
        val msg = BCHserialized(SerializationType.NETWORK)
        msg.addUint8(2) // 2 means make a transaction proposal
        msg.addExactBytes(flatUnsignedTx)  // This is already serialized
        msg.add(reason)  // TODO
        msg.addInt32(input)
        msg.addMap(sigs, { BCHserialized(msg.format).addVariableSized(it.data)}, { BCHserialized(msg.format).addVariableSized(it)} )
        dest.comms.send(msg.toByteArray())
    }

    override suspend fun go()
    {
        issueCompletionRequest()
        while (valid && (sigs.size < dest.minSigs))
        {
            val wakeId = wakeup.receive()
            LogIt.info(sourceLoc() + ": Go coroutine wake up (${wakeId}): msig ${dest.address.toString()} got ${sigs.size} of ${dest.minSigs} ")
        }
        if (!valid) return
        LogIt.info("enough signatures have accumulated (${sigs.size} of ${dest.minSigs}).  We are ready to sign this input ${input}.")
        val script = dest.unlockingScript(flatUnsignedTx, input.toLong(), sigHashType, tx.inputs[input].spendable.amount,this)
        assert(script.size != 0)  // The only thing blocking script production is # of sigs, but we just checked above that we have enough
        tx.inputs[0].script = script
        completed = true
        val tmp = launchIn
        launchIn = null
        tmp?.let { returnSpendingProposalScope(it) }
        LogIt.info("Input ${input} signed. Multisig protocol complete.")
    }

    override fun accept()
    {
        super.accept()
        dest.addMySigs(this)
    }
}

class InteractiveMultisigDestination(
    chain: ChainSelector,
    /** Secret shared by all participants of this interaction, used for identifying and encrypting communications */
    var convoSecret:ByteArray,
    var minSigs: Int,  // mininum number of signatures (n)
    var pubkeys: Array<Bytes>,    // Order defines order in script, count defines total number of multisig signers (m)
    var privkeys: Array<Secret?>,  // Pass null for privkeys you don't know
    var comms:ProtocolCommunication
) : Pay2TemplateDestination(chain)
{
    companion object
    {
        const val DEST_TYPE = 7 // For object identification when serialized
    }
    override val derivedType = DEST_TYPE
    override val atomicSpending: Boolean = false

    /** LOGGING/DEBUG ONLY: Returns what position in the multisig pubkey or sigs list is the one I control.
     * Since technically this destination could control more than one pubkey, do not use in algorithms.
     * Instead, iterate through the privkeys list to find which privkeys are non-null.
     */
    var logMyPosition: Int = -1
        protected set

    init
    {
        template = if (minSigs==0) null else formMultisig(chain, minSigs, pubkeys)
        constraint = null  // no constraint args (they are effectively in the template)
        visibleConstraint = SatoshiScript(chain)

        for ((idx, pk) in privkeys.withIndex()) if (pk != null) logMyPosition = idx
    }

    constructor(chainSelector: ChainSelector, stream: BCHserialized) : this(chainSelector, byteArrayOf(), 0, arrayOf(), arrayOf(), NoProtocolCommunication())
    {
        BCHdeserialize(stream)
    }

    override fun serializeDerived(format: SerializationType): BCHserialized
    {
        // check format so the secret isn't accidentally sent over the network
        if (format == SerializationType.DISK)
        {
            // TODO encrypt secret
            val ret = BCHserialized(format).add(super.serializeDerived(format))
            ret.addVariableSized(convoSecret).addInt32(minSigs)
            ret.addList(pubkeys.toList(), { item, ser -> ser.addVariableSized(item.data) })
            ret.addList(privkeys.toList(), { item, ser -> ser.addNullableVariableSized(item?.getSecret())})
            return ret
        }
        else throw NotImplementedError()
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
         if (stream.format == SerializationType.DISK)
         {
             super.BCHdeserialize(stream)
             //LogIt.info(sourceLoc() + ": deser at ${stream.deserPos}")
             convoSecret = stream.deByteArray()
             minSigs = stream.deint32()
             pubkeys = stream.delist { Bytes(it.deByteArray()) }.toTypedArray()
             privkeys = stream.delist {
                 val tmp = it.denullByteArray()
                 tmp?.let { UnsecuredSecret(it) }
             }.toTypedArray()
         }
        else throw NotImplementedError()
        return stream
    }

    var monitoring = false
    fun beginMonitoring(wallet: Wallet)
    {
        monitoring = true
        launch {
            while(monitoring)
            {
                val (incoming, created) = comms.receive()
                val msg = BCHserialized(SerializationType.NETWORK,incoming)
                val msgType = msg.debyte()
                LogIt.info("Interactive activity for destination: $logMyPosition.  Message Type: $msgType")
                if (msgType == 2.toByte())  // 2 means make a transaction proposal
                {
                    val tx = txFor(chainSelector, msg)
                    val reason = msg.deString()
                    val input = msg.deint32()
                    val sigs: HashMap<Bytes,ByteArray> = hashMapOf()
                    msg.demap(sigs, { Bytes(it.deByteArray())}, { it.deByteArray()})
                    val pp = (wallet as CommonWallet).getProposal(tx.idem)
                    if (pp != null && pp.mine)  // If I made this proposal there is nothing for me to do
                    {
                    }
                    else
                    {
                        // When I get a spend proposal, the 1st thing to do is create the overarching "job" of spending this transaction
                        // And then add in this particular multisig as a child job
                        val txp = wallet.addProposal(tx, reason, false)

                        val pp = txp.jobForInputOrAdd(input) {
                            MultisigSpendingProposal(tx, this, input) {
                                val inp = tx.inputs[input]
                                val txo = inp.spendable.outpoint?.let { wallet.getTxo(it) }
                                if (txo != null && txo.payDestination != null)
                                {
                                    LogIt.info("$logMyPosition: I can spend this input")
                                    val scr = inp.script
                                    println("$logMyPosition: current script is ${scr.toAsm()}")
                                }
                                else
                                {
                                    LogIt.info("$logMyPosition: I cannot spend this input")
                                    // TODO or should we spend what we can?
                                    invalidate("$logMyPosition: Cannot spend input ${input}")
                                }
                            }
                        } as? MultisigSpendingProposal

                        // If the input isn't a MultisigSpendingProposal then a participant is misbehaving
                        for (s in sigs) // Add the sigs that we were provided
                        {
                            LogIt.info("Destination: $logMyPosition.  Adding sig for pubkey: ${s.key.toHex()}")
                            pp?.addSig(s.key, s.value)
                        }
                    }
                }
            }
        }
    }

    fun stopMonitoring()
    {
        monitoring = false
        // launch { comms.send(byteArrayOf()) }
    }

    fun addMySigs(msp: MultisigSpendingProposal)
    {
        val sigHashType = msp.sigHashType
        val inputIdx = msp.input

        val inputAmount:Long = msp.tx.inputs[inputIdx].spendable.amount
        val tmpl = template?.toByteArray() ?: throw IllegalStateException("Multisig destination is not formed (template is missing)")

        for ((priv, pub) in privkeys.zip(pubkeys))
        {
            if (priv != null)
            {
                val secret = priv.getSecret()
                val sig: ByteArray = libnexa.signTxOneInputUsingSchnorr(msp.flatUnsignedTx, sigHashType, inputIdx.toLong(), inputAmount, tmpl, secret)
                msp.addSig(pub, sig)
            }
        }
    }

    /** If atomicSpending is false, this is how you initiate whatever protocol is needed to sign this transaction.
     * This should only be called if this is your own spending proposal (it MUST BE that the user has already agreed to this spend) */
    override fun beginProposal(tx:iTransaction, inputIdx:Int): SpendingProposal
    {
        var ret = MultisigSpendingProposal(tx,this, inputIdx) {}
        addMySigs(ret)
        return ret
    }


    override fun unlockingScript(flatTx: ByteArray, inputIdx: Long, sigHashType: ByteArray, inputAmount: Long, spendingProposal: SpendingProposal?): SatoshiScript
    {
        if (spendingProposal == null) throw IllegalStateException("no spending proposal in multisig")
        if (spendingProposal.accepted == false) throw IllegalStateException("spending proposal is not accepted")
        val sp = (spendingProposal as? MultisigSpendingProposal) ?: throw IllegalStateException("Incompatible spending proposal")
        if (sp.sigs.size < minSigs) throw IllegalStateException("spending proposal has not accumulated enough signatures")
        var tmpl = templateScript().flatten()
        var spendMap = 0
        var sigCount = 0
        var mapBit = 1
        val sigs = mutableListOf<OP>()
        for ((idx,pub) in pubkeys.withIndex())
        {
            /*  We should have put our own info in sigs
            val priv = privkeys[idx]
            if (priv != null) // ok this pub is mine
            {
                val secret = priv.getSecret()  // TODO put my own sig in the spending proposal so that I don't have to get the secret again (potentially asking for unlock)
                val sig: ByteArray = libnexa.signTxOneInputUsingSchnorr(flatTx, sigHashType, inputIdx, inputAmount, tmpl, secret)
                sigs.add(OP.push(sig))
                spendMap = spendMap or mapBit
                sigCount += 1
                // println("sighashtype: 0x${sigHashType.toString(16)}, input: ${inputIdx} amount: ${inputAmount} constraintScript: ${sc.toHex()} tx: ${flatTx.toHex()}\npriv: ${secret.toHex()} -> sig: ${sig.toHex()}")
                if (sigCount == minSigs) break // all done
            }
            else
            {
             */
            val sig = sp.sigs[pub]
            if (sig != null)
            {
                sigs.add(OP.push(sig))
                spendMap = spendMap or mapBit
                sigCount += 1
                // println("sighashtype: 0x${sigHashType.toString(16)}, input: ${inputIdx} amount: ${inputAmount} constraintScript: ${sc.toHex()} tx: ${flatTx.toHex()}\npriv: ${secret.toHex()} -> sig: ${sig.toHex()}")
                if (sigCount == minSigs) break // all done
            }

            mapBit = mapBit * 2
        }

        if (sigCount < minSigs)  // we have not signed enough, ask our peers
        {
            LogIt.info("ask peers to complete tx")
            TODO()  // In the reformulation unlockingScript should not be called until you have all needed info
            // TODO for tx that gather UTXOs from the same multisig, this is going to ask multiple times because each input is being handled separately

            //val pp = issueCompletionRequest(flatTx, inputIdx.toInt())
            // TODO
            return SatoshiScript(chainSelector)  // I can't solve this at this time
        }

        // OK I have everything I need so spend it
        val ret = SatoshiScript(chainSelector)
        ret.add(OP.push(tmpl))
        ret.add(OP.push(spendMap))
        ret.add(*(sigs.toTypedArray()))
        return ret
    }

    override fun equals(other: Any?): Boolean
    {
        return contentEquals(other)
    }

    fun contentEquals(other: Any?): Boolean
    {
        if (other == null) return false
        if (other is InteractiveMultisigDestination)
        {
            // Check pubkeys here because the secret may require unlocking
            if (pubkeys.size != other.pubkeys.size) return false
            for ((p0,p1) in pubkeys zip other.pubkeys)
                if (p0 != p1) return false
        }
        return true
    }

    override fun templateScript(): SatoshiScript
    {
        return formMultisig(chainSelector, minSigs, pubkeys)
    }
}


