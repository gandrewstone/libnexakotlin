// Copyright (c) 2024 Bitcoin Unlimited
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.

@file:OptIn(ExperimentalUnsignedTypes::class)
package org.nexa.libnexakotlin

private val LogIt = GetLog("BU.dest")

/** Represents an arbitrary P2SH destination
 */
open class Pay2ScriptHashDestination(chainSelector: ChainSelector, idx: Long) : PayDestination(chainSelector)
{
    init
    {
        index = idx
    }
    companion object
    {
        const val DEST_TYPE = 3 // For object identification when serialized
    }

    override val derivedType = DEST_TYPE

    override var secret: Secret? = null // Not applicable for this destination
    override val pubkey: ByteArray? = null  // Not applicable for this destination

    var redeemScript = SatoshiScript(chainSelector)

    val redeemScriptHash: ByteArray
        get() = libnexa.hash160(templateScript().flatten())

    constructor(chainSelector: ChainSelector, redeem: SatoshiScript, index: Long) : this(chainSelector, index)
    {
        redeemScript = redeem
    }

    override fun serializeDerived(format: SerializationType): BCHserialized
    {
        throw NotImplementedError()
    }

    override fun equals(other: Any?): Boolean
    {
        return contentEquals(other)
    }

    open fun contentEquals(other: Any?): Boolean
    {
        if (other == null) return false
        if (other is Pay2ScriptHashDestination)
        {
            return (redeemScript contentEquals other.redeemScript)
        }
        return false
    }

    override fun templateScript(): SatoshiScript
    {
        return redeemScript
    }

    override fun lockingScript(): SatoshiScript
    {
        //return BCHscript(chainSelector, OP.HASH160, OP.push(address.data), OP.EQUAL)
        return SatoshiScript.p2sh(address.data, chainSelector)
    }

    override fun groupedLockingScript(groupId: GroupId, groupAmount: Long): SatoshiScript
    {
        throw(WalletNotSupportedException("Blockchain does not support group tokens"))  // Must be, because NEXA obsoleted P2SH
    }

    /**
     *  @param [params] Provide all the data needed to spend the template script.  Each parameter will be pushed to the stack in the order passed (so the last parameter ends up on the top of the stack).
     */
    override fun unlockingScript(vararg params: ByteArray): SatoshiScript
    {
        var redeem = templateScript().flatten()
        val ret = SatoshiScript(chainSelector)
        for (p in params)
        {
            ret.add(OP.push(p).v)
        }
        ret.add(OP.push(redeem).v)
        return ret
    }

    override val bloomFilterData: ByteArray?
        get() = redeemScriptHash

    override val address: PayAddress
        get() = PayAddress(chainSelector, PayAddressType.P2SH, templateScript().scriptHash160())
}


/** Represents a P2PK (pay to public key) destination wrapped in a P2SH script
 */
class Pay2ScriptPubKeyDestination(chainSelector: ChainSelector, override val secret: Secret, idx: Long) : PayDestination(chainSelector)
{
    init
    {
        index = idx
    }

    override val pubkey = GetPubKey(secret.getSecret())
    val pubkeyHash = libnexa.hash160(pubkey)

    val redeemScriptHash: ByteArray
        get() = libnexa.hash160(templateScript().flatten())

    override fun equals(other: Any?): Boolean
    {
        return contentEquals(other)
    }

    override fun serializeDerived(format: SerializationType): BCHserialized
    {
        throw NotImplementedError()
    }

    fun contentEquals(other: Any?): Boolean
    {
        if (other == null) return false
        if (other is Pay2ScriptPubKeyDestination)
        {
            // Check pubkeys here because the secret may require unlocking
            return (pubkey contentEquals other.pubkey)
        }
        return false
    }

    override fun templateScript(): SatoshiScript
    {
        var ret = SatoshiScript(chainSelector, SatoshiScript.Type.SATOSCRIPT, OP.DUP, OP.HASH160, OP.push(pubkeyHash), OP.EQUALVERIFY, OP.CHECKSIG)
        return ret
    }

    override fun lockingScript(): SatoshiScript = SatoshiScript.p2sh(redeemScriptHash, chainSelector)
    override fun groupedLockingScript(groupId: GroupId, groupAmount: Long): SatoshiScript
    {
        if (chainSelector == ChainSelector.BCH) throw(WalletNotSupportedException("Blockchain does not support group tokens"))

        // If this group holds native tokens, then it must either be an authority or have 0 tokens
        assert(!groupId.isFenced() || (groupAmount <= 0))
        // TODO: NEXA groups
        assert(false)
        //return BCHscript.gp2sh(groupId, groupAmount, redeemScriptHash, chainSelector)
        return SatoshiScript(chainSelector)
    }

    /**
     *  @param [params] Provide a single parameter which is the signature derived from [secret] of the transaction that is including this spend script
     */
    override fun unlockingScript(vararg params: ByteArray): SatoshiScript
    {
        var redeem = templateScript().flatten()
        return SatoshiScript(chainSelector, SatoshiScript.Type.PUSH_ONLY, OP.push(redeem).v, pubkey, params[0])
    }

    override val bloomFilterData: ByteArray?
        get() = redeemScriptHash

    override val address: PayAddress
        get() = PayAddress(chainSelector, PayAddressType.P2PKH, pubkeyHash)
}

class MultisigP2SHDestination(
    chain: ChainSelector,
    val minSigs: Int,  // mininum number of signatures (n)
    val pubkeys: Array<ByteArray>,    // Order defines order in script, count defines total number of multisig signers (m)
    val privkeys: Array<Secret?>,  // Pass null for privkeys you don't know
    index: Long
) : Pay2ScriptHashDestination(chain, index)
{
    override val derivedType = DEST_TYPE

    init
    {
        redeemScript = templateScript()
    }

    /*
    companion object
    {
        const val DEST_TYPE = 4 // For object identification when serialized

        @cli(Display.Dev, "Construct a Multisig payment destination from its redeem script and some wallets.  If you do not have enough wallets to fully sign, then the resulting satisfier script will not be fully signed (but could be given to another wallet to finish (TBD)).")
        fun fromRedeem(redeemScript: SatoshiScript, vararg wallets: Wallet): PayDestination
        {
            val redeem = redeemScript.parsed()
            val pubkeys = mutableListOf<ByteArray>()
            val privkeys = mutableListOf<Secret?>()
            val minSigs = scriptNumFrom(redeem[0])!!
            for (item in redeem)  // all data items in the redeem scripts are the wallet pubkeys
            {
                val pubkey = scriptDataFrom(item)
                if (pubkey != null)  // If its null, its not data (not a pubkey)
                {
                    pubkeys.add(pubkey)
                    var found = false
                    for (w in wallets)
                    {
                        val privkey = w.pubkeyToSecret(pubkey)
                        if (privkey != null)
                        {
                            privkeys.add(privkey)
                            found = true
                            break
                        }
                    }
                    if (!found)
                    {
                        privkeys.add(null)
                    }
                }
            }
            return MultisigDestination(redeemScript.chainSelector, minSigs.toInt(), pubkeys.toTypedArray(), privkeys.toTypedArray())
        }
    }
    */

    override fun serializeDerived(format: SerializationType): BCHserialized
    {
        throw NotImplementedError()
    }


    override fun equals(other: Any?): Boolean
    {
        return contentEquals(other)
    }

    override fun contentEquals(other: Any?): Boolean
    {
        if (other == null) return false
        if (other is MultisigP2SHDestination)
        {
            // Check pubkeys here because the secret may require unlocking
            if (pubkeys.size != other.pubkeys.size) return false
            for ((p0,p1) in pubkeys zip other.pubkeys)
                if (!(p0 contentEquals p1)) return false
        }
        return true
    }

    override fun templateScript(): SatoshiScript
    {
        val ret = SatoshiScript(chainSelector) + OP.push(minSigs.toLong()).v
        for (pk in pubkeys)
            ret.add(OP.push(pk).v)
        ret.add(OP.push(pubkeys.size.toLong()).v)
        ret.add(OP.CHECKMULTISIG.v)
        return ret
    }

    fun spendScriptECDSA(flatTx: ByteArray, inputIdx: Long, sigHashType: Int, inputAmount: Long, vararg params: ByteArray): SatoshiScript
    {
        assert(params.size == 0)  // No params needed for multisig
        val ret = SatoshiScript(chainSelector)

        ret.add(OP.push(0))
        for (priv in privkeys)
        {
            if (priv != null)
            {
                val sc = templateScript().flatten()
                val secret = priv.getSecret()
                val sig: ByteArray = libnexa.signBchTxOneInputUsingECDSA(flatTx, sigHashType, inputIdx, inputAmount, sc, secret)
                //val sig: ByteArray = Wallet.signOneInputUsingSchnorr(flatTx, sigHashType, inputIdx, inputAmount, sc, secret)
                ret.add(OP.push(sig))
                // println("sighashtype: 0x${sigHashType.toString(16)}, input: ${inputIdx} amount: ${inputAmount} constraintScript: ${sc.toHex()} tx: ${flatTx.toHex()}\npriv: ${secret.toHex()} -> sig: ${sig.toHex()}")
            }
        }

        var redeem = templateScript().flatten()
        ret.add(OP.push(redeem))
        return ret
    }

    override fun unlockingScript(flatTx: ByteArray, inputIdx: Long, sigHashType: ByteArray, inputAmount: Long, spendingProposal: SpendingProposal?): SatoshiScript
    {
        val ret = SatoshiScript(chainSelector)

        var spendMap = 0
        var privCount = 0
        for (priv in privkeys.reversed())
        {
            spendMap = spendMap shl 1
            if (priv != null)
            {
                spendMap = spendMap or 1
                privCount++
            }
        }

        ret.add(OP.push(spendMap))
        for (priv in privkeys)
        {
            if (priv != null)
            {
                val sc = templateScript().flatten()
                val secret = priv.getSecret()
                // TODO distinguish NEXA vs BCH
                val sig: ByteArray = libnexa.signTxOneInputUsingSchnorr(flatTx, sigHashType, inputIdx, inputAmount, sc, secret)
                ret.add(OP.push(sig))
                // println("sighashtype: 0x${sigHashType.toString(16)}, input: ${inputIdx} amount: ${inputAmount} constraintScript: ${sc.toHex()} tx: ${flatTx.toHex()}\npriv: ${secret.toHex()} -> sig: ${sig.toHex()}")
            }
        }

        var redeem = templateScript().flatten()
        ret.add(OP.push(redeem))
        return ret
    }
}


class MultisigDestination(
    chain: ChainSelector,
    val minSigs: Int,  // mininum number of signatures (n)
    val pubkeys: Array<Bytes>,    // Order defines order in script, count defines total number of multisig signers (m)
    val privkeys: Array<Secret?>,  // Pass null for privkeys you don't know
) : Pay2TemplateDestination(chain)
{
    companion object
    {
        const val DEST_TYPE = 6 // For object identification when serialized
    }
    override val derivedType = DEST_TYPE

    init
    {
        template = formMultisig(chain, minSigs, pubkeys)
        constraint = null  // no constraint args (they are effectively in the template)
        visibleConstraint = SatoshiScript(chain)
    }

    override fun serializeDerived(format: SerializationType): BCHserialized
    {
        throw NotImplementedError()
    }


    override fun equals(other: Any?): Boolean
    {
        return contentEquals(other)
    }

    fun contentEquals(other: Any?): Boolean
    {
        if (other == null) return false
        if (other is MultisigDestination)
        {
            // Check pubkeys here because the secret may require unlocking
            if (pubkeys.size != other.pubkeys.size) return false
            for ((p0,p1) in pubkeys zip other.pubkeys)
                if (p0 != p1) return false
        }
        return true
    }

    override fun templateScript(): SatoshiScript
    {
        return formMultisig(chainSelector, minSigs, pubkeys)
        /* old
        val ret = SatoshiScript(chainSelector) + OP.push(minSigs.toLong()).v
        for (pk in pubkeys)
            ret.add(OP.push(pk).v)
        ret.add(OP.push(pubkeys.size.toLong()).v)
        ret.add(OP.CHECKMULTISIG.v)
        return ret
         */
    }

    override fun unlockingScript(flatTx: ByteArray, inputIdx: Long, sigHashType: ByteArray, inputAmount: Long, spendingProposal: SpendingProposal?): SatoshiScript
    {
        val ret = SatoshiScript(chainSelector)

        var tmpl = templateScript().flatten()
        ret.add(OP.push(tmpl))

        var spendMap = 0
        var sigCount = 0
        /*
        var privCount = 0
        for (priv in privkeys.reversed())
        {
            spendMap = spendMap shl 1
            if (priv != null)
            {
                spendMap = spendMap or 1
                privCount++
            }
        }

         */
        var mapBit = 1
        val sigs = mutableListOf<OP>()
        for (priv in privkeys)
        {
            if (priv != null)
            {
                val secret = priv.getSecret()
                val sig: ByteArray = libnexa.signTxOneInputUsingSchnorr(flatTx, sigHashType, inputIdx, inputAmount, tmpl, secret)
                sigs.add(OP.push(sig))
                spendMap = spendMap or mapBit
                sigCount += 1
                // println("sighashtype: 0x${sigHashType.toString(16)}, input: ${inputIdx} amount: ${inputAmount} constraintScript: ${sc.toHex()} tx: ${flatTx.toHex()}\npriv: ${secret.toHex()} -> sig: ${sig.toHex()}")
                if (sigCount == minSigs) break // all done
            }
            mapBit = mapBit * 2
        }
        ret.add(OP.push(spendMap))
        ret.add(*(sigs.toTypedArray()))
        return ret
    }
}