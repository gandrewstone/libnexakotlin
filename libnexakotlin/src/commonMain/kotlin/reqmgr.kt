package org.nexa.libnexakotlin

import io.ktor.network.sockets.SocketTimeoutException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.newFixedThreadPoolContext
import kotlinx.serialization.json.JsonElement
import org.nexa.threads.*
import kotlin.coroutines.CoroutineContext

private val LogIt = GetLog("BU.req")

open class BlockNotForthcoming(val blockHash: Hash256) : BlockchainException(appI18n(RblockNotForthcoming) + " " + blockHash.toHex(), appI18n(RblockNotForthcoming), ErrorSeverity.Expected)
open class HeadersNotForthcoming(val blockHash: Hash256) : BlockchainException(appI18n(RheadersNotForthcoming) + " " + blockHash.toHex(), appI18n(RheadersNotForthcoming), ErrorSeverity.Expected)

// typically we'd close this connection
open class BadHeadersProvided(blockHash: Hash256) : HeadersNotForthcoming(blockHash)

open class RequestedPrehistoryHeader(blockHash: Hash256, val lastBlock: iBlockHeader?) : HeadersNotForthcoming(blockHash)
{
    override fun toString(): String
    {
        return super.toString() + "Prehistory block: " + blockHash.toHex()
    }
}


//? The request manager handles all interactions between this client and the rest of the network.
//  Other software layers should not concern itself with individual nodes or connections.  They ask the Request Manager for data, and the RM determines where and how to get it
@cli(Display.Simple, "Manage requests & responses from the blockchain network")
class RequestMgr(@cli(Display.Simple, "Access the blockchain network directly") val net: CnxnMgr, val genesisBlockHash: Hash256)
{
    var MAX_RECENT_BLOCK_CACHE = DEFAULT_MAX_RECENT_BLOCK_CACHE
    var MAX_RECENT_MERKLE_BLOCK_CACHE =  DEFAULT_MAX_RECENT_MERKLE_BLOCK_CACHE // bigger because merkle blocks are a lot smaller
    var MAX_RECENT_TX_CACHE = DEFAULT_MAX_RECENT_TX_CACHE
    var MAX_RECENT_HEADER_CACHE = DEFAULT_MAX_RECENT_HEADER_CACHE
    val blkRequestsLock = org.nexa.threads.Mutex()

    @cli(Display.Dev, "In-progress block header requests")
    val blkRequests: MutableMap<Hash256, MutableSet<(iBlockHeader)->Unit>> = mutableMapOf()

    @cli(Display.Dev, "Recently received block cache")
    val recentBlocks: MutableMap<Hash256, iBlock> = mutableMapOf()
    val blockArrival = mutableListOf<Hash256>()

    @cli(Display.Dev, "Recently received merkle block cache")
    val recentMerkleBlocks: MutableMap<Hash256, iMerkleBlock> = mutableMapOf()
    protected val merkleBlockArrival = mutableListOf<Hash256>()

    @cli(Display.Dev, "Recently received block header cache")
    val recentHeaders: MutableMap<ByteArray, iBlockHeader> = mutableMapOf()
    protected val headerArrival = mutableListOf<Hash256>()
    val recentHeaderLock = Gate("recent header lock")  // both merkle and normal

    var allowFilteredBlock = true  // Set to true to allow bloom filtered blocks (merkle blocks)

    protected val coCtxt: CoroutineContext = newFixedThreadPoolContext(NUM_BLOCKCHAIN_PROCESSING_THREADS, "blockchain")
    protected val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)

    //<! woken when any header arrives
    // val headerWaiter = CoCond<List<out iBlockHeader>>(coScope)

    @cli(Display.Dev, "Recently received transaction cache")
    val recentTxs: MutableMap<Hash256, iTransaction> = mutableMapOf()
    val txArrival = mutableListOf<Hash256>()

    val partialBlocks = mutableMapOf<Hash256, iMerkleBlock?>()

    val callbacksLock = org.nexa.threads.Mutex()
    val unconfTxCallbacks = mutableListOf<(List<iTransaction>) -> Unit>()

    val txBlkSync = Gate()

    @cli(Display.Dev, "Clears the cache of all recent merkle blocks")
    fun clearMerkleBlockCache()
    {
        synchronized(recentHeaderLock)
        {
            partialBlocks.clear()
            recentMerkleBlocks.clear()
            merkleBlockArrival.clear()
        }
    }

    @cli(Display.Dev, "Add a callback whenever an unconfirmed tx arrived")
    fun addUnconfirmedTxHandler(cb: (List<iTransaction>) -> Unit)
    {
        callbacksLock.lock { unconfTxCallbacks.add(cb) }
    }

    @cli(Display.Simple, "Get details about a transaction, given its hash")
    fun getTxDetails(txHash: Hash256) = getTxDetails(txHash.toHex())

    @cli(Display.Simple, "Get details about a transaction")
    fun getTxDetails(txHash: String): JsonElement
    {
        val ec = net.getElectrum()
        try
        {
            // TODO retry on timeout
            val ret = ec.getTxDetails(txHash, ELECTRUM_REQ_TIMEOUT)
            return ret
        }
        finally
        {
            net.returnElectrum(ec)
        }
    }

    @cli(Display.Simple, "Get a transaction, given its hash")
    fun getTx(txHash: Hash256) = getTx(txHash.toHex())

    @cli(Display.Simple, "Get a transaction, given its hex-encoded hash")
    fun getTx(txHash: String): iTransaction
    {
        val ec = net.getElectrum()
        try
        {
            val ret = ec.getTx(txHash, ELECTRUM_REQ_TIMEOUT) // TODO retry on timeout
            return ret
        }
        finally
        {
            net.returnElectrum(ec)
        }
    }


    /** Get a list of block headers after a certain location.
     * @param loc get headers after this location
     * @exception HeadersNotForthcoming After multiple retries, no headers found
     * */
    @cli(Display.User, "Get list of block headers after a certain position")
    fun getBlockHeadersAfter(loc: BlockLocator, stopAt: Hash256 = Hash256()): MutableList<out iBlockHeader>
    {
        var tries = 0
        val locLogId = if (loc.have.size > 0) loc.have[0].toHex() else stopAt.toHex()
        while (tries < 2)  // After a few tries, I need to give up and try some other headers -- for example, its possible that a longer chain appeared so nobody is serving this chain
        {
            tries += 1

            val waiter = Gate()

            var rcvdHeaders: MutableList<out iBlockHeader>? = null
            var rcvdFrom: P2pClient? = null
            val cb: (hdrs: MutableList<out iBlockHeader>, P2pClient) -> Boolean = { hdrs, node ->

                waiter.wake() {
                    rcvdHeaders = hdrs
                    rcvdFrom = node
                }
                true
            }

            var count = 0
            val alreadyUsed: MutableSet<P2pClient> = mutableSetOf()
            val allUsed: MutableSet<P2pClient> = mutableSetOf()
            try
            {
                var node:P2pClient? = null
                while ((rcvdHeaders == null) && (count < 10))
                {
                    val start = millinow()
                    count += 1
                    if (count < 7)
                    {
                        node = net.getAnotherNode(alreadyUsed)
                        var delay = 0
                        if (node != null)
                        {
                            LogIt.info(sourceLoc() + " " + net.name + " " + node.logName + ": Send getheaders for $locLogId")

                            try
                            {
                                node.getHeaders(loc, stopAt, cb)
                            }
                            catch (e: SocketTimeoutException)  // try some other node
                            {
                                node.cleanupExclusiveHeaders(cb)
                                LogIt.info(sourceLoc() + " " + net.name + " socket exception: " + e.toString())
                                delay = 50
                            }
                            catch (e: NetTimeoutException)  // try some other node
                            {
                                node.cleanupExclusiveHeaders(cb)
                                LogIt.info(sourceLoc() + " " + net.name + " timeout getting headers: " + e.toString())
                                delay = 50
                            }
                            catch (e: P2PDisconnectedException)
                            {
                                node.cleanupExclusiveHeaders(cb)
                                LogIt.info(sourceLoc() + " " + net.name + " disconnected exception: " + e.toString())
                                node.close()
                                net.report(node)
                            }
                            catch (e: Exception)
                            {
                                node.cleanupExclusiveHeaders(cb)
                                handleThreadException(e, "unknown exception getting headers", sourceLoc())
                                node.close()
                            }
                            alreadyUsed.add(node)
                            allUsed.add(node)
                            if (delay != 0) millisleep(delay.toULong())
                        }
                    }
                    else if (alreadyUsed.size == 0)  // We couldn't get a single node -- we have no connections
                    {
                        if (loc.have.size > 0) throw HeadersNotForthcoming(loc.have[0])
                        else throw HeadersNotForthcoming(stopAt)
                    }
                    val earlyExit = waiter.delayuntil(3000) { rcvdHeaders != null }
                    if (!earlyExit)
                    {
                        if (rcvdHeaders == null)
                        {
                            if (alreadyUsed.size == 0) LogIt.info(sourceLoc() + " " + net.name + " getheaders waiting for connection")
                            else
                            {
                                LogIt.info(sourceLoc() + " " + net.name + " " + node?.logName + ": getheaders delay no response from ${alreadyUsed.size} nodes, when attempting send getheaders for ${locLogId}")
                                node?.latencyDatapoint(10000)  // big punishment for no response
                                if (node == null) alreadyUsed.clear()  // nothing left to use, so I better reuse some node
                            }
                        }
                    }
                    else
                    {
                        node?.latencyDatapoint(millinow() - start)
                        node?.cleanupExclusiveHeaders(cb)
                    }
                }
            }
            finally
            {
                waiter.finalize()
                for (node in allUsed) node.cleanupExclusiveHeaders(cb)
            }

            val headers = rcvdHeaders

            if (headers == null) // if we get nothing, after some time try other nodes
            {

                var names = ""
                for (node in alreadyUsed) names += node.logName + ", "
                LogIt.warning(sourceLoc() + " " + net.name + " " + names + " didn't receive headers after ${locLogId}")
                millisleep(50U)
            }
            else
            {
                // Not even our checkpoint is common with this node -- its on a fork so disconnect
                if (headers.size > 0)
                {
                    val first = headers[0]
                    if (((first.height == 1L) && (first.hashPrevBlock != genesisBlockHash))
                        ||  ((first.height == 0L) && (first.hash != genesisBlockHash)))
                    {
                        LogIt.warning(sourceLoc() + " " + net.name + " " + rcvdFrom?.logName + " Returned ${headers.size} headers starting at the genesis block. It must be a separate fork")
                        LogIt.warning(sourceLoc() + " " + net.name + " Request was " + loc.toString())
                        LogIt.warning(sourceLoc() + " " + net.name + " " + headers[0].height + " " + headers[0].toString())
                        rcvdFrom?.close()
                        // We don't want to give up if we get bad headers from a node, we should try some other node
                        // throw P2PDisconnectedException(rcvdFrom?.logName + " Contains no common post-checkpoint blocks -- must be a separate fork")
                    }
                }

                return headers
            }

        }
        if (loc.have.size > 0) throw HeadersNotForthcoming(loc.have[0])
        else throw HeadersNotForthcoming(stopAt)
    }



    /** Get a list of block headers after a certain block
     * @param hash get headers after this one
     * @exception HeadersNotForthcoming After multiple retries, no headers found
     * */
    @cli(Display.User, "Get list of block headers after a certain block")
    fun getBlockHeaders(hash: Hash256): MutableList<out iBlockHeader>
    {
        val loc = BlockLocator()
        loc.add(hash)

        LogIt.info(sourceLoc() + " " + net.name + ": Send getheaders for ${hash.toHex()}")
        var hdrs = getBlockHeadersAfter(loc, hash)
        return hdrs
    }

    /** Get a list of block headers after a certain block
     * @param hash get headers after this one
     * @exception HeadersNotForthcoming After multiple retries, no headers found
     * */
    @cli(Display.User, "Get list of block headers after a certain block")
    fun getBlockHeader(hash: ByteArray): iBlockHeader?
    {
        val loc = BlockLocator()
        // LogIt.info(sourceLoc() + " " + net.chainName + ": Send getheaders for ${hash.toHex()}")
        var hdrs = getBlockHeadersAfter(loc, Hash256(hash))
        if (hdrs.size == 0) return null
        return hdrs[0]
    }
    @cli(Display.User, "Get list of block headers after a certain block")
    fun getBlockHeader(hash: Hash256): iBlockHeader?
    {
        val loc = BlockLocator()
        // LogIt.info(sourceLoc() + " " + net.chainName + ": Send getheaders for ${hash.toHex()}")
        var hdrs = getBlockHeadersAfter(loc, hash)
        if (hdrs.size == 0) return null
        return hdrs[0]
    }


    /** Start a thread to get a list of block headers after a certain block, and then to call a continuation function.
     * If headers are not available (after a bunch of retries), call the continuation with an empty list
     * @param hash get headers after this one
     * @param then continuation function
     * */
    fun asyncGetBlockHeaders(hash: Hash256, then: (MutableList<out iBlockHeader>) -> Unit)
    {
        Thread(net.name + "_getBlockHeaders") {
            val loc = BlockLocator()
            loc.add(hash)
            LogIt.info(sourceLoc() + " " + net.name + ": Async send getheaders for ${hash.toHex()}")
            try
            {
                var hdrs = getBlockHeadersAfter(loc, hash)
                then(hdrs)
            }
            catch(e: Exception)
            {
                then(mutableListOf())  // call with empty list to indicate failure
            }
        }
    }


    fun getBlockHeaderByHash(hash: Hash256): iBlockHeader = getBlockHeaderByHash(hash.hash)
    /** Get a particular block header
     * @param hash block id
     */
    @cli(Display.User, "Get specific block header")
    fun getBlockHeaderByHash(hash: ByteArray): iBlockHeader
    {
        // Return this header if we've already gotten it
        while (true)
        {
            try
            {
                (synchronized(recentHeaderLock) { recentHeaders[hash] })?.let { return it }

                val hdr = getBlockHeader(hash)
                if (hdr != null)
                {
                    return hdr
                }
                millisleep(50U)
            }
            catch (e: SocketTimeoutException)
            {
                millisleep(50U)
            }
            catch (e: P2PDisconnectedException)
            {
                millisleep(50U)
            }
            catch (e: Exception)
            {
                handleThreadException(e)
                millisleep(500U)
            }
        }
    }

    /** Asynchronously pre-request a block whose transactions I'm sure I'll need */
    fun earlyRequestTxInBlock(blocks: Collection<Hash256>)
    {
        while (true)
        {
            val node = try
            {
                net.getNode(0)
            }
            catch (e: P2PNoNodesException) // we can't prerequest the block if we don't have any connections so just return
            {
                return
            }

            try
            {
                synchronized(txBlkSync)
                {
                    //val needed = blocks.filter { recentBlocks[it.data] == null }.map { Inv(Inv.Types.BLOCK, it) }.toMutableList()
                    val needed = mutableListOf<Inv>()
                    for (bhash in blocks)
                    {
                        if (recentBlocks[bhash] == null && recentMerkleBlocks[bhash] == null)
                        {
                            if (allowFilteredBlock && node.bloomCount == net.bloomCount)  // If we are ok with merkle blocks and the filter is up to date then ask for one
                            {
                                if (!partialBlocks.contains(bhash)) // For a pre-request, don't request it again if we have an open request
                                {
                                    partialBlocks[bhash] = null
                                    needed.add(Inv(Inv.Types.FILTERED_BLOCK, bhash))
                                }
                            }
                            else
                                needed.add(Inv(Inv.Types.BLOCK, bhash))
                        }
                    }

                    if (needed.size > 0)
                    {
                        node.sendGetData(needed)
                        // val blocksString = needed.map { it.id.toHex() }.joinToString(" ")
                        // LogIt.info(sourceLoc() + " " + net.chainName + ": Early request of ${needed.size} merkle blocks ${blocksString}")
                    }
                }
                return
            }
            catch (e: P2PDisconnectedException)
            {
                node.close()
                net.report(node)
            }
            catch (e: Exception)
            {
                millisleep(50U)
            }
        }
    }

    val EMPTY_ITX_LIST = listOf<iTransaction>()
    fun requestTxInBlock(blockHash: Hash256): List<iTransaction>
    {
        var waiter: iGate? = null
        var blk: iBlockHeader? = null
        val cb:(iBlockHeader)->Unit = { it:iBlockHeader->
            // LogIt.info("got block callback for ${blockHash.toHex()}")
            blkRequestsLock.synchronized {
                waiter?.wake { blk = it }
            }
        }
        try
        {
            waiter = Gate("txRequest")
            blkRequestsLock.synchronized {
                val v = blkRequests.getOrPut(blockHash, { mutableSetOf() })
                // LogIt.info("Total pending: ${blkRequests.size}.  Requests pending for ${blockHash.toHex()}: ${v.size}")
                v.add(cb)
            }

            var actuallySentCount = 0

            val alreadyTriedNodes = mutableSetOf<P2pClient>()
            while (true) // Keep asking from a node until we get a response
            {
                var wait = BLOCK_REQ_TIMEOUT
                // first check to see if we have already received it
                val bl = synchronized(txBlkSync) {
                    // LogIt.info("recentBlocks: ${recentMerkleBlocks.size}")
                    recentBlocks[blockHash]
                }
                if (bl != null)
                {
                    LogIt.info(sourceLoc() + " " + net.name + ": returning cached block ${bl.height}:${bl.hash.toHex()}")
                    return bl.txes
                }
                val mbl = synchronized(txBlkSync) {
                    // LogIt.info("recentMerkleBlocks: ${recentMerkleBlocks.size}")
                    recentMerkleBlocks[blockHash]
                }
                if (mbl != null)
                {
                    LogIt.info(sourceLoc() + " " + net.name + ": returning cached merkle block ${mbl.height}:${mbl.hash.toHex()}:")
                    return mbl.txes
                }

                var node = net.getAnotherNode(alreadyTriedNodes)
                var reqStart = millinow()
                var noCnxnsLogged = false

                try
                {
                    if (node != null)
                    {
                        // node?.let { LogIt.info(sourceLoc() + " " +it.logName + ": Requesting block: " + blockHash.toHex() + " from: ${node.logName}") }

                        alreadyTriedNodes.add(node)
                        // If we've installed the latest bloom, ask for a filtered block
                        // otherwise drop back to a full block
                        if (allowFilteredBlock && node.bloomCount == net.bloomCount)
                        {
                            // the commented code is an option.  It's saying "if we've prerequested (but not received) then wait for that to come in"
                            // however if it hasn't come in yet, its probably best just to ask someone else (it certainly is from looking at the perf)
                            //if ((count!=1) || (!partialBlocks.contains(blockHash)))
                            node.sendGetData(listOf(Inv(Inv.Types.FILTERED_BLOCK, blockHash)))
                        }
                        else
                            node.sendGetData(listOf(Inv(Inv.Types.BLOCK, blockHash)))
                        // LogIt.info(sourceLoc() + ": Send filtered block request to ${node.logName} for block ${blockHash.toHex()}")
                        actuallySentCount++
                    }
                    else
                    {
                        // If the node is null, we'll wait anyway, in the assumption that the block might still come in from a prior request
                        // but if its the first request (if we are waiting for a connection to be made), then delay for less time.
                        if (net.p2pCnxns.size == 0)
                        {
                            if (!noCnxnsLogged)  // just log this once
                            {
                                LogIt.info(sourceLoc() + " " + net.name + ": Cannot send filtered block request: no connections")
                                noCnxnsLogged = true
                            }
                            wait = 300
                        }
                        else
                        {
                            if (actuallySentCount==0)
                            {
                                if (net.p2pCnxns.size >= 2)
                                    wait = 100
                                else wait = 500
                            }
                            else
                            {
                                wait = BLOCK_REQ_TIMEOUT/2
                                val s = StringBuilder()
                                alreadyTriedNodes.forEach { s.append(it.logName); s.append(" ") }
                                LogIt.info(sourceLoc() + " " + net.name + ": No nodes to send filtered block request to.  Already tried $s")
                            }
                        }
                        alreadyTriedNodes.clear() // Restart the try loop
                    }

                    // node?.let { LogIt.info(sourceLoc() + " " + node.logName + " Waiting for block: " + blockHash.toHex()) }
                    waiter.delayuntil(wait) { blk != null}
                    val end = millinow()
                    node?.let { it.latencyDatapoint(end-reqStart)}
                }
                catch (e: DeserializationException)
                {
                    logThreadException(e)
                    node?.misbehavingBadMessage(5)
                }
                catch (e: TimeoutCancellationException) // Node did not respond to us
                {
                    logThreadException(e)
                    if (node != null)
                    {
                        val end = millinow()
                        node.latencyDatapoint(end-reqStart)
                        // TODO: if too many nodes are silent, the problem may be that we are following a minority chain
                        node.misbehavingQuiet(1)
                        LogIt.info(sourceLoc() + " " + node.logName + ": block request timeout for block " + blockHash.toHex())
                    }
                }
                catch (e: P2PDisconnectedException)
                {
                    logThreadException(e)
                    if (node != null)
                    {
                        node.close()  // CnxnMgr detects closed connections and cleans them up
                        net.report(node)
                    }
                }
                catch (e: Exception)  // Try some other node
                {
                    val end = millinow()
                    node?.let { it.latencyDatapoint(end-reqStart)}
                    logThreadException(e)
                    millisleep(100U)
                }

                val b = blk
                blk = null
                if (b == null)
                {
                    if (actuallySentCount >= 2)  // Nodes silently drop requests for blocks off the main chain, so after a bit we need to give up
                    {
                        node?.let { LogIt.info(sourceLoc() + " " + it.logName + ": Gave up on block " + blockHash.toHex()) }
                        throw BlockNotForthcoming(blockHash)
                    }
                    node?.let { LogIt.info(sourceLoc() + " " +it.logName + ": Requesting block again: " + blockHash.toHex()) }
                }
                else
                {
                    if (b is iBlock)
                        return b.txes.toMutableList()
                    else if (b is iMerkleBlock)
                    {
                        if (b.txes.size > 0)
                        {
                            LogIt.info("merkle block ${b.hash}:${b.height} ${b.txes.size} tx fully assembled")
                            val tmp = mutableListOf<iTransaction>()  // I need to take a copy to get b cleaned up on iOS
                            for (i in b.txes) tmp.add(i)
                            b.finalize()
                            return tmp
                        }
                        else
                        {
                            b.finalize()
                            return EMPTY_ITX_LIST
                        }
                    }
                }
            }
        }
        finally
        {
            // clean up my request callback
            blkRequestsLock.synchronized {
                val tmp = blkRequests.get(blockHash)
                if (tmp == null)
                {
                    LogIt.info(sourceLoc() + " " + this.net.name + " Not possible blkRequests missing data")
                }
                else if (tmp.remove(cb))
                {
                    if (tmp.size == 0)
                    {
                        blkRequests.remove(blockHash)
                    }
                }
                else
                {
                    LogIt.info(sourceLoc() + " " + this.net.name + " Not possible blkRequests missing my callback")
                }
                val tmp2 = waiter
                waiter = null
                tmp2?.finalize()
            }
        }
    }

    /*
    suspend fun coRequestTxInBlock(blockHash: Guid): List<iTransaction>
    {
        val waiter = Channel<iBlockHeader>()
        try
        {
            blkRequestsLock.withLock {
                blkRequests.getOrPut(blockHash.data, { mutableSetOf() }).add { launch { waiter.send(it)} }
            }

            var blk: iBlockHeader? = null

            var count = 0
            val alreadyTriedNodes = mutableSetOf<P2pClient>()
            while (blk == null) // Keep asking from a node until we get a response
            {
                count += 1

                // first check to see if we have already received it
                blk = recentBlocks[blockHash.data]
                if (blk != null)
                {
                    //LogIt.info(sourceLoc() + " " + net.chainName + ": returning cached block ${blk.hash.toHex()}:${blk.height}")
                    return blk.txes
                }
                blk = synchronized(txBlkSync) { recentMerkleBlocks[blockHash.data] }
                if (blk != null)
                {
                    //LogIt.info(sourceLoc() + " " + net.chainName + ": returning cached merkle block ${blk.hash.toHex()}:${blk.height}")
                    return blk.txes
                }

                var node = net.getAnotherNode(alreadyTriedNodes)
                try
                {
                    if (node != null)
                    {
                        alreadyTriedNodes.add(node)
                        // If we've installed the latest bloom, ask for a filtered block
                        // otherwise drop back to a full block
                        if (allowFilteredBlock && node.bloomCount == net.bloomCount)
                            node.sendGetData(listOf(Inv(Inv.Types.FILTERED_BLOCK, blockHash)))
                        else
                            node.sendGetData(listOf(Inv(Inv.Types.BLOCK, blockHash)))
                        //LogIt.info(sourceLoc() + ": Send filtered block request")
                    }  // If the node is null, we'll wait anyway, in the assumption that the block might still come in from a prior request
                    else
                    {
                        alreadyTriedNodes.clear() // Restart the try loop
                    }
                    withTimeout(BLOCK_REQ_TIMEOUT) {
                        blk = waiter.receive()
                    }
                }
                catch (e: DeserializationException)
                {
                    node?.misbehavingBadMessage(5)
                }
                catch (e: TimeoutCancellationException) // Node did not respond to us
                {
                    if (node != null)
                    {
                        // TODO: if too many nodes are silent, the problem may be that we are following a minority chain
                        node.misbehavingQuiet(1)
                        LogIt.info(sourceLoc() + " " + node.logName + ": block request timeout for block " + blockHash.toHex())
                    }
                }
                catch (e: P2PDisconnectedException)
                {
                    if (node != null)
                    {
                        node.close()  // CnxnMgr detects closed connections and cleans them up
                        net.report(node)
                    }
                }
                catch (e: Exception)  // Try some other node
                {
                    LogIt.info("socket exception")
                    delay(50)
                }

                val b = blk
                if (b == null)
                {
                    if (count >= 2)  // Nodes silently drop requests for blocks off the main chain, so after a bit we need to give up
                        throw BlockNotForthcoming(blockHash)
                    node?.let { LogIt.info(it.logName + ": Requesting block " + blockHash.toHex() + " again") }
                }
                else
                {
                    if (b is iBlock)
                        return b.txes.toMutableList()
                    else if (b is iMerkleBlock)
                    {
                        // LogIt.info("merkle block ${b.hash}:${b.height} fully assembled")
                        return b.txes
                    }
                }
            }

            throw BlockNotForthcoming(blockHash) // can't ever get here anyway
        }
        finally
        {

            blkRequestsLock.withLock {
                blkRequests[blockHash.data]?.remove(waiter)  // clean up
            }
        }
    }
    */

    /** Insert a partial block into the cache.  If a block is returned, its complete */
    fun insertPartialBlocks(blk: iMerkleBlock): iMerkleBlock?
    {
        var ret:iMerkleBlock? = null
        synchronized(txBlkSync)
        {
            val existingBlk = partialBlocks[blk.hash]
            if (existingBlk == null)
            {
                partialBlocks[blk.hash] = blk
            }
            else  // Merge these two merkle blocks
            {
                for (tx in blk.txes)
                    existingBlk.txArrived(tx)

                if (existingBlk.complete())   // It was completed so remove and return it
                {
                    partialBlocks.remove(blk.hash)
                    ret = existingBlk
                }
            }
        }
        return ret
    }

    fun onPartialBlocks(blks: List<iMerkleBlock>)
    {
        val readyBlocks = mutableListOf<iBlockHeader>()
        synchronized(txBlkSync)
        {
            for (blk in blks)
            {
                // val interestingTx = blk.txHashes.size + blk.txes.size
                // LogIt.info(sourceLoc() + " " + net.chainName + ": processing merkle block " + blk.hash.toHex() + " TX: " + interestingTx + " of " + blk.txCount)
                txloop@ for (tx in recentTxs)  // txes might come in before the merkle block it refers to
                {
                    if (blk.txArrived(tx.value))  // If we consumed the tx
                    {
                        if (blk.complete()) break@txloop
                    }
                }
                if (blk.complete()) readyBlocks.add(blk)
                else
                {
                    insertPartialBlocks(blk)?.let { readyBlocks.add(it) }
                }
            }
        }

        if (readyBlocks.size > 0)
        {
            // LogIt.info(sourceLoc() + ": ${readyBlocks.size} merkle blocks are ready")
            launch(CoroutineScope(coCtxt)) {
                try
                {
                    onBlocks(readyBlocks)
                }
                catch(e:Exception)
                {
                    logThreadException(e, "when providing completed partial block.")
                }
            }
        }
    }

    // If a block comes in, search through the list of requesters and provide it to them
    fun onBlocks(blks: List<iBlockHeader>)
    {
        for (blk in blks)
        {
            synchronized(txBlkSync)
            {
                if (blk is iBlock)
                {
                    recentBlocks[blk.hash] = blk  // Put this block into the recently requested cache
                    blockArrival.add(blk.hash)
                }
                else if (blk is iMerkleBlock)
                {
                    // val numTx = (blk as? NexaBlockHeader)?.txCount.toString() ?: "unknown"
                    // LogIt.info(sourceLoc() + " " + net.chainName + ": merkle block ready " + blk.hash.toHex() + " TX: " + blk.txes.size + " of " + numTx)
                    recentMerkleBlocks[blk.hash] = blk  // Put this block into the recently requested cache
                    merkleBlockArrival.add(blk.hash)
                }
                null
            }

            val waiters = blkRequestsLock.synchronized { blkRequests[blk.hash]?.let { it.toList() } }

            //val waiters = blkRequestsLock.withLock {
            //    blkRequests[blk.hash]?.let { it.toList() }
            //}

            if (waiters != null)
            {
                for (w in waiters)
                {
                    try
                    {
                        w(blk)
                    }
                    catch (e: Exception)
                    {
                        logThreadException(e, "in block waiter callback")
                    }
                }
            }
        }

        while (blockArrival.size > MAX_RECENT_BLOCK_CACHE)  // Remove older recent blocks
        {
            synchronized(txBlkSync)
            {
                recentBlocks.remove(blockArrival[0])
                blockArrival.removeAt(0)
            }
        }

        while (merkleBlockArrival.size > MAX_RECENT_MERKLE_BLOCK_CACHE)  // Remove older recent merkle blocks
        {
            synchronized(txBlkSync)
            {
                recentMerkleBlocks.remove(merkleBlockArrival[0])
                merkleBlockArrival.removeAt(0)
            }
        }
    }

    /** Called when transactions arrive */
    fun onTx(txes: List<iTransaction>)
    {
        val readyBlocks = mutableListOf<iBlockHeader>()
        val unconfTxes = mutableListOf<iTransaction>()

        synchronized(txBlkSync)
        {
            for (tx in txes)
            {
                // LogIt.info(sourceLoc() + " onTx: " + tx.idem.toHex())
                recentTxs[tx.idem] = tx
                txArrival.add(tx.idem)

                var txConfirmed = false
                /* Provide this transaction to any of our in-progress merkle blocks and if this transaction completes the merkle block then process it */
                merkle@ for ((hash, blk) in partialBlocks)
                {
                    if (blk != null && blk.txArrived(tx))
                    {
                        txConfirmed = true
                        if (blk.complete())
                        {
                            readyBlocks.add(blk)
                            partialBlocks.remove(hash)
                        }
                        break@merkle  // a particular tx will only be confirmed in one block
                    }
                }
                if (!txConfirmed) unconfTxes.add(tx)
            }

            // Clear out old values
            while (txArrival.size > MAX_RECENT_TX_CACHE)
            {
                recentTxs.remove(txArrival[0])
                txArrival.removeAt(0)
            }
        }

        if (unconfTxes.size > 0) onUnconfirmedTx(unconfTxes)
        if (readyBlocks.size > 0) launch(CoroutineScope(coCtxt)) {
            try
            {
                onBlocks(readyBlocks)
            }
            catch(e: Exception)
            {
                logThreadException(e, "in tx processor, block completed")
            }
        }
    }

    fun onUnconfirmedTx(txes: List<iTransaction>)
    {
        val unlockedCopy = callbacksLock.lock {
            unconfTxCallbacks.toTypedArray()
        }
        for (cb in unlockedCopy)
            cb(txes)
    }


    fun updateHeadersCache(hdr: iBlockHeader)
    {
        synchronized(recentHeaderLock)
        {
            recentHeaders[hdr.hash.hash] = hdr
            headerArrival.add(hdr.hash)
            while (headerArrival.size > MAX_RECENT_HEADER_CACHE)  // Remove items from queue
            {
                recentHeaders.remove(headerArrival[0].hash)
                headerArrival.removeAt(0)
            }
        }
    }
    fun updateHeadersCache(hdrs: List<iBlockHeader>)
    {
        synchronized(recentHeaderLock)
        {
            for (hdr in hdrs)
            {
                recentHeaders[hdr.hash.hash] = hdr
                headerArrival.add(hdr.hash)
                while (headerArrival.size > MAX_RECENT_HEADER_CACHE)  // Remove items from queue
                {
                    recentHeaders.remove(headerArrival[0].hash)
                    headerArrival.removeAt(0)
                }
            }
        }
    }

    init
    {
        net.addPartialBlockHandler { blockLst -> onPartialBlocks(blockLst) }
        net.addBlockHeadersHandler { headerLst ->
            if (headerLst.size > 0) updateHeadersCache(headerLst)
            // headerWaiter.wake(headerLst)
        }

        net.addBlockHandler { blkLst -> onBlocks(blkLst) }

        net.addTxHandler { txLst -> onTx(txLst) }
    }
}