package org.nexa.libnexakotlin

import com.ionspin.kotlin.bignum.decimal.BigDecimal
import com.ionspin.kotlin.bignum.decimal.DecimalMode
import com.ionspin.kotlin.bignum.decimal.RoundingMode

/** Satoshis (finest unit) per NEXA unit */
const val SATperNEX = 100L
/** Satoshis (finest unit) per microBCH unit */
const val SATperUBCH = 100L
/** Satoshis (finest unit) per BCH unit */
const val SATperBCH = 100L * 1000L * 1000L
/** The number of decimal places needed to express 1 satoshi in the "normal" currency units */
const val BchDecimals = 8
/** The number of decimal places needed to express 1 Satoshi (or equivalent) in the units used in the GUI */
const val uBchDecimals = 2
/** The number of decimal places needed to express 1 Satoshi (or equivalent) in the units used in the GUI */
const val NexaDecimals = 2
/** How many decimal places we need to do math without creating cumulative rounding errors */
const val CurrencyScale = 16

/** How all fiat currencies are displayed (2 decimal places) */
val FiatFormat = DecimalFormat("##,##0.00")
/** How the uBCH crypto unit is displayed (2 optional decimal places) */
val uBchFormat = DecimalFormat("##,###,##0.00")
/** How the mBCH crypto unit is displayed (5 optional decimal places) */
val mBchFormat = DecimalFormat("##,##0.#####")

/** How the NEXA crypto unit is displayed (2 optional decimal places) */
val NexaFormat = DecimalFormat("##,###,##0.00")
/** How the NEX crypto is displayed in input fields (2 optional decimal places, no thousands separator) */
val NexaInputFormat = DecimalFormat("#########0.00")
/** How to display BCH crypto unit */
val BchFormat = DecimalFormat("#######0.########")
/** How convert BigDecimals to strings in preparation for serialization/deserialization */
val CurrencySerializeFormat = DecimalFormat("###########0.########")

/** Tell the system details about how we want bigdecimal math handled */
val CurrencyMath = DecimalMode(32L, RoundingMode.ROUND_HALF_AWAY_FROM_ZERO, CurrencyScale.toLong())
/** Tell the system details about how we want bigdecimal math handled */
val NexaMathMode = DecimalMode(32L, RoundingMode.ROUND_HALF_AWAY_FROM_ZERO, NexaDecimals.toLong())
/** tell the system details about how we want bigdecimal math handled */
val uBchMathMode = DecimalMode(32L, RoundingMode.ROUND_HALF_AWAY_FROM_ZERO, uBchDecimals.toLong())

/** The decimal number 0, with the correct "currency" rounding mode [CurrencyMath]. Same as [CURRENCY_0]*/
val CURRENCY_ZERO = CurrencyDecimal(0)
/** The decimal number 0, with the correct "currency" rounding mode [CurrencyMath].  Same as [CURRENCY_ZERO] */
val CURRENCY_0 = CURRENCY_ZERO
/** The decimal number -1, with the correct "currency" rounding mode [CurrencyMath]. */
val CURRENCY_NEG1 = CurrencyDecimal(-1)
/** The decimal number 1, with the correct "currency" rounding mode [CurrencyMath]. */
val CURRENCY_1 = CurrencyDecimal(1)

/** Create a BigDecimal that is appropriate for currency mathematics (with lots of decimal places) */
fun CurrencyDecimal(a: Long) = BigDecimal.fromLong(a, CurrencyMath)

/** Create a BigDecimal that is appropriate for currency mathematics (with lots of decimal places) */
fun CurrencyDecimal(a: Int) = BigDecimal.fromInt(a, CurrencyMath)

/** Convert a BigDecimal of unknown math mode (rounding & precision) into one that is appropriate for currency mathematics (with lots of decimal places) */
fun CurrencyDecimal(b: BigDecimal): BigDecimal = BigDecimal.fromBigDecimal(b, CurrencyMath)


/** Create a BigDecimal that is appropriate for Nexa currency mathematics (with 2 decimal places) */
fun NexaDecimal(a: Long) = BigDecimal.fromLong(a, NexaMathMode)

/** Create a BigDecimal that is appropriate for Nexa currency mathematics (with 2 decimal places) */
fun NexaDecimal(a: Int) = BigDecimal.fromInt(a, NexaMathMode)


/** Create a BigDecimal that is appropriate for u-BCH (micro bitcoin cash) currency mathematics (with 2 decimal places) */
fun UbchDecimal(a: Long) = BigDecimal.fromLong(a, uBchMathMode)

/** Convert Nexa unit to Satoshis */
fun NexaToSat(a:Long) = a*100
/** Convert Nexa unit to Satoshis */
fun NexaToSat(a:BigDecimal):Long = (a *BigDecimal.fromInt(100)).toLong()

/** Convert Satoshi unit to Nexa */
fun SatToNexa(a:Long) = NexaDecimal(a)/100

/** Get a BigDecimal from a string */
fun BigDecimal.Companion.fromString(s: String, dm: DecimalMode):BigDecimal
{
    return parseStringWithMode(s, dm)
}

/** Get a BigDecimal from a string */
fun BigDecimal.Companion.fromNullString(s:String?, dm: DecimalMode=CurrencyMath):BigDecimal?
{
    if (s == null) return null
    return BigDecimal.fromString(s, dm)
}

/** Create a BigDecimal that is appropriate for currency mathematics (with lots of decimal places) */
fun CurrencyDecimal(a: String) = BigDecimal.fromString(a, CurrencyMath)

fun BigDecimal.toLong(): Long
{
    return this.longValue(false)
}

/** Convert a BigDecimal to an int */
fun BigDecimal.toInt(): Int
{
    return this.longValue(false).toInt()
}

/** Convert a BigDecimal to a double */
fun BigDecimal.toDouble(): Double
{
    return this.doubleValue(false)
}

/** Get this string as a BigDecimal currency value (using your default locale's number representation) */
expect fun String.toCurrency(chainSelector: ChainSelector? = null): BigDecimal
