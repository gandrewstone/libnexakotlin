// Copyright (c) 2024 Bitcoin Unlimited
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
@file:OptIn(ExperimentalUnsignedTypes::class)
package org.nexa.libnexakotlin

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.newFixedThreadPoolContext
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.coroutineContext

private val LogIt = GetLog("BU.dest")


interface ProtocolCommunication
{
    suspend fun start()  // Starts listening
    fun stop()
    suspend fun send(ba: ByteArray)
    //fun sendTo(ba: ByteArray)
    suspend fun receive(): Pair<ByteArray, ULong>
}

class NoProtocolCommunication():ProtocolCommunication
{
    override suspend fun start()
    {
        assert(false)
    }

    override fun stop()
    {
        assert(false)

    }

    override suspend fun send(ba: ByteArray)
    {
        assert(false)
    }

    override suspend fun receive(): Pair<ByteArray, ULong>
    {
        assert(false)
        return Pair(byteArrayOf(),0UL)
    }

}

open class TxSpendingProposal(val wallet: Wallet, val tx:iTransaction, override val info: String, _mine: Boolean, onAccept: SpendingProposal.()->Unit):
    SpendingProposal(onAccept)
{
    init
    {
        mine = _mine
        accepted = mine  // if its mine, assume its accepted (or the proposal would not have been created
    }

    override val title: String?
        get() = ""

    val children = mutableListOf<SpendingProposal>()

    fun addJob(job: SpendingProposal)
    {
        children.add(job)
    }

    fun jobForInputOrAdd(input: Int, addFn:()->SpendAnInput):SpendAnInput
    {
        for (job in children)
        {
            val j = (job as? SpendAnInput)
            if (j != null)
            {
                if (j.input == input) return j
            }
        }
        val newjob = addFn()
        addJob(newjob)
        return newjob
    }

    var jobs: CoroutineScope? = null

    override suspend fun go()
    {
        if (jobs == null)
        {
            while(true)
            {
                val j = CoroutineScope(coroutineContext)
                jobs = j

                var done = true
                for (c in children)
                {
                    if (!c.valid)
                    {
                        invalidate("child is invalid")
                        return
                    }
                    if (!c.completed) done = false
                }
                if (done) break

                for (c in children)
                {
                    if (!c.completed && c.valid) c.job = j.launch { c.go() }
                }

                for (c in children) c.job?.join()
                for (c in children) c.job = null
            }

            // All children are completed, so let's send this transaction
            completed = true
        }
    }

    override fun invalidate(reason: String)
    {
        for (c in children) c.invalidate(reason)
        super.invalidate(reason)
    }

    override fun accept() // Indicate that the user has accepted the execution of this proposal.  By default any proposals that are created by this machine are implicitly accepted.
    {
        if (valid)
        {
            accepted = true
            for (job in children)
                job.accept()
            onAccept(this)
        }
        else throw IllegalStateException("cannot accept invalid proposal")
    }
}

open class SpendAnInput(val tx:iTransaction, val destination: PayDestination, val input:Int, onAccept: SpendingProposal.()->Unit):
SpendingProposal(onAccept)
{
    init
    {
        if (input >= tx.inputs.size)
        {
            invalidate("incorrect proposal: input exceeds transaction")
        }
    }

    override val title: String?
        get() = null
    override val info: String?
        get() = null
}


val spendingProposalContext = newFixedThreadPoolContext(8, "spendingProposals")
/** Get a new scope to handle all coroutines related to a particular spending proposal */
fun newSpendingProposalScope(): CoroutineScope
{
    return CoroutineScope(spendingProposalContext)
}

fun returnSpendingProposalScope(cs: CoroutineScope)
{
    cs.cancel("spending proposal is complete")
}

abstract class SpendingProposal(val onAccept: SpendingProposal.()->Unit)
{
    val createdMs = millinow()

    var mine: Boolean = false
        protected set

    var valid = true  // call invalidate() to set
        protected set

    var accepted:Boolean = false  // call accept() to set
        protected set

    var completed = false  // Set to true when this spending proposal contains enough information to actually spend
        protected set

    var job: Job? = null

    abstract val title:String?
    abstract val info:String?

    open fun invalidate(reason: String)
    {
        accepted = false
        valid = false
    }

    open fun accept() // Indicate that the user has accepted the execution of this proposal.  By default any proposals that are created by this machine are implicitly accepted.
    {
        if (valid)
        {
            accepted = true
            onAccept(this)
        }
        else throw IllegalStateException("cannot accept invalid proposal")
    }

    open suspend fun go()
    {
        completed = true
    }

}


class CapdProtocolCommunication(val chain:Blockchain, protected val convoSecret: ByteArray,var coCtxt: CoroutineContext):ProtocolCommunication
{
    // The conversation ID does not need to be secure, anyone monitoring capd will see it
    // It just needs to probabilistically disambiguate conversations to minimize bandwidth (getting messages irrelevant to you)
    val convoId = libnexa.hash256(convoSecret + byteArrayOf(0)).sliceArray(0 until 4)

    var p2p:P2pClient? = null
    var capdMonitorId: Long? = null
    var capdRelayPriority: Double = 0.0

    val rcvChannel = Channel<CapdMsg>(100, BufferOverflow.DROP_OLDEST)

    override suspend fun start()
    {
        //println("CapdProtocolCommunication get p2p")
        val svr = chain.net.getp2p()
        // Not needed, fixed:
        // I need to create a separate connection for test because can't ask same connection for same data
        //while(chain.net.p2pCnxns.size == 0) delay(500)
        //val ap2p = chain.net.p2pCnxns.first()
        //val cname = ap2p.name
        //val cport = ap2p.port
        //val svr = P2pClient(ap2p.chainSelector, cname, cport, "$cname:$cport", multisigCoCtxt)
        //svr.connect(10000)
        //val th = Thread { while(svr.isAlive()) svr.process() }

        p2p = svr
        //println("CapdProtocolCommunication got p2p to ${svr.logName}, installing monitor for ${convoId.toHex()}")
        capdMonitorId = svr.installMsgMonitor(convoId) { launch(CoroutineScope(coCtxt)) {
            for(msg in it)
            {
                msg.data?.let {
                    if (millinow().toULong()/1000UL < (msg.createTime + msg.expiration))  // Do not bother if the message self-declares as expired
                    {
                        LogIt.info(sourceLoc() + ": Received & enqueued message of length: ${it.size} ")
                        rcvChannel.send(msg)
                    }
                }

            }
            LogIt.info(sourceLoc() + ": Monitor ${capdMonitorId?.toString(16)}: Enqueued ${it.size} capd messages")
        }}
        //println("CapdProtocolCommunication to ${svr.logName} installed monitor ${capdMonitorId?.toString(16)}")
        svr.reloadCapdInfo {
            capdRelayPriority = it.capdRelayPriority
        }
    }

    override fun stop()
    {
        capdMonitorId?.let { p2p?.removeMsgMonitor(it) }
    }

    override suspend fun send(ba: ByteArray)
    {
        val capdMsg = CapdMsg(convoId + ba)
        // capdMsg.difficultyBits = // TODO use the relay priority to solve the message
        capdMsg.solve(0)
        chain.net.broadcastMsg(capdMsg)
    }

    override suspend fun receive(): Pair<ByteArray, ULong>
    {
        val msg = rcvChannel.receive()
        LogIt.info(sourceLoc() + ": Capd comm processing msg")
        val ba = msg.data!!.drop(convoId.size).toByteArray()  // !! should never have been put into this rcvChannel
        return Pair(ba, msg.createTime)
    }
}
