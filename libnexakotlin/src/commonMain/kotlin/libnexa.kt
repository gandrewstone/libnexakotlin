package org.nexa.libnexakotlin

import kotlin.experimental.xor
import kotlin.random.Random

/** Network-defined bloom filter maximum size
 * TODO: associate this value with the blockchain
 */
public val MAX_BLOOM_FILTER_SIZE:Int = 36000

const val AES256_BLOCKSIZE = 16

/**  Bloom filter update flags
 * Must be consistent with same named fields in C++ code in bloom.h:bloomflags
 * */
public enum class BloomFlags(public val v: Int)
{
    // Must match the native code. DO NOT CHANGE!
    BLOOM_UPDATE_NONE(0),
    BLOOM_UPDATE_ALL(1),
    BLOOM_UPDATE_P2PUBKEY_ONLY(2)
}

// Payment Address abstraction
enum class PayAddressType(val v: Byte)
{
    // Must match C++ libnexa.cpp PayAddressType
    NONE(255.toByte()),
    P2PUBKEY(2),
    P2PKH(0),
    P2SH(1),
    GROUP(11),
    TEMPLATE(19), // Generalized pay to script template
    P2PKT(19);  // Pay to well-known script template 1 (pay-to-pub-key-template)  -- same type as template

    fun isValid():Boolean
    {
        return when (v)
        {
            255.toByte(), 2.toByte(), 0.toByte(), 1.toByte(), 11.toByte(), 19.toByte() -> true
            else -> false
        }
    }
}

fun LoadPayAddressType(v: Byte): PayAddressType
{
    return when (v)
    {
        255.toByte() -> PayAddressType.NONE
        2.toByte() -> PayAddressType.P2PUBKEY
        0.toByte() -> PayAddressType.P2PKH
        1.toByte() -> PayAddressType.P2SH
        11.toByte() -> PayAddressType.GROUP
        19.toByte() -> PayAddressType.TEMPLATE
        // 5.toByte() -> PayAddressType.P2PKT
        else -> throw PayAddressDecodeException("unknown address type")
    }
}

var aes256ivCount: Long = 0



@kotlin.ExperimentalUnsignedTypes
public interface LibNexa
{
    public companion object
    {
        public val GROUP_ID_MIN_SIZE:Int = 32
        public val GROUP_ID_MAX_SIZE:Int = 520  // stack size
    }
    /** Returns the minimum amount of native coins (finest unit) that must be in every UTXO for the passed blockchain */
    public fun minDust(chain: ChainSelector): Long

    public fun encode64(data: ByteArray): String
    public fun decode64(encoded: String): ByteArray

    /** Returns the double sha256 of data. Result is 32 bytes */
    public fun hash256(data: ByteArray): ByteArray
    /** Returns the sha256 of data. Result is 32 bytes */
    public fun sha256(data: ByteArray): ByteArray
    /** Calculates the RIPEMD160 of the SHA256 of data. Result is 20 bytes */
    public fun hash160(data: ByteArray): ByteArray

    /** Returns the double sha256 of data. Result is 32 bytes */
    public fun hash256(data: UByteArray): UByteArray
    /** Returns the sha256 of data. Result is 32 bytes */
    public fun sha256(data: UByteArray): UByteArray

    /** Calculates the RIPEMD160 of the SHA256 of data. Result is 20 bytes */
    public fun hash160(data: UByteArray): UByteArray

    /** Calculates the block hash given a serialized header.  Note that by convention hashes are displayed in byte-reverse order (as if a little-endian number was being displayed).
     * This return value is not reversed.  (hash.reversed().toHex() == "hex header from explorer") -> true
     */
    public fun blockHash(serializedBlockHeader: ByteArray): ByteArray
    public fun blockHash(serializedBlockHeader: UByteArray): UByteArray

    public fun txidem(serializedTx: ByteArray): ByteArray
    public fun txidem(serializedTx: UByteArray): UByteArray
    public fun txid(serializedTx: ByteArray): ByteArray
    public fun txid(serializedTx: UByteArray): UByteArray

    public fun verifyBlockHeader(chainSelector: ChainSelector, serializedBlockHeader: ByteArray): Boolean
    public fun verifyBlockHeader(chainSelector: ChainSelector, serializedBlockHeader: UByteArray): Boolean


    public fun signHashSchnorr(data: UByteArray, secret: UByteArray): UByteArray
    public fun signHashSchnorr(data: ByteArray, secret: ByteArray): ByteArray

    /** Given a private key, return the corresponding (secp256k1) public key -- that is
     * the privKey*G where G is the group generator */
    public fun getPubKey(privateKey: UByteArray): UByteArray
    public fun getPubKey(privateKey: ByteArray): ByteArray

    /** This function calculates the BIP44 key from the specified path.  BIP44 requires that purpose, coinType and account are "hardened".
     * This function will automatically harden those parameters if you pass unhardened values.
     * Therefore this function cannot be used for generalized (non-BIP44) child key derivation.
     *
     */
    public fun deriveHd44ChildKey(secretSeed: ByteArray, purpose: Long, coinType: Long, account: Long, change: Boolean, index: Int): Pair<ByteArray, String>

    /** Decodes a private key provided in Satoshi's original fnormat */
    public fun decodeWifPrivateKey(chainSelector: ChainSelector, secretWIF: String): ByteArray

    /** Returns the work -- that is the expected number of hashes to find a solution -- given the difficulty expressed in Bitcoin's "bits" notation */
    public fun getWorkFromDifficultyBits(nBits: Long): ByteArray
    public fun getWorkFromDifficultyBits(nBits: UInt): ByteArray
    public fun getWorkFromDifficultyBits(nBits: ULong): ByteArray

    public fun getDifficultyBitsFromWork(work:ByteArray): UInt


    /** Given an array of items, creates a bloom filter and returns it serialized.
         * Typical items are addresses (just the raw 20 bytes), transaction hashes, and outpoints
         * @param items Array<ByteArray> of bitstrings to place into the bloom filter
         * @param falsePosRate Desired Bloom false positive rate
         * @param capacity Number of elements that can be placed into this bloom while maintaining the falsePosRate.  If this is < items.count(), items.count() is used.  The reason to provide a larger capacity is to allow items to be added into the bloom filter later
         * @param maxSize Maximum size of the bloom filter -- if the capacity and falsePosRate result in a bloom that's larger than this, this size is used instead
         * @param flags  See @BloomFlags for possible fields
         * @param tweak Change tweak to create a different bloom filter.  Used to ensure that collision attacks only work against one filter or node
         * @return Bloom filter serialized as in the P2P network format
         */
    public fun createBloomFilter(items: Array<Any>, falsePosRate: Double, capacity: Int, maxSize: Int, flags: Int = 0, tweak: Int = 1): ByteArray

    /** Create an ECDSA signature for the passed transaction
         * @param txData transaction serialized for signing
         * @param sigHashType signature hash algorithm selection
         * @param inputIdx what input to sign
         * @param inputAmount how many satoshis this input contains
         * @param prevoutScript the input's constraint script
         * @secret 32 byte private key
         * @return signature in binary format
         */
    public fun signBchTxOneInputUsingECDSA(txData: ByteArray, sigHashType: Int, inputIdx: Long, inputAmount: Long, prevoutScript: ByteArray, secret: ByteArray): ByteArray

    /** Create a Schnorr signature for the passed transaction
         * @param txData transaction serialized for signing
         * @param sigHashType signature hash algorithm selection
         * @param inputIdx what input to sign
         * @param inputAmount how many satoshis this input contains
         * @param prevoutScript the input's constraint script
         * @secret 32 byte private key
         * @return signature in binary format
         */
    public fun signTxOneInputUsingSchnorr(txData: ByteArray, sigHashType: ByteArray, inputIdx: Long, inputAmount: Long, prevoutScript: ByteArray, secret: ByteArray): ByteArray

    /** Create a Schnorr signature for the passed transaction
         * @param txData transaction serialized for signing
         * @param sigHashType signature hash algorithm selection
         * @param inputIdx what input to sign
         * @param inputAmount how many satoshis this input contains
         * @param prevoutScript the input's constraint script
         * @secret 32 byte private key
         * @return signature in binary format
         */
    public fun signBchTxOneInputUsingSchnorr(txData: ByteArray, sigHashType: Int, inputIdx: Long, inputAmount: Long, prevoutScript: ByteArray, secret: ByteArray): ByteArray

    /** Sign a message using the same algorithm as the original bitcoin wallet's signmessage functionality
         * @message The raw bytes to be signed
         * @secret 32 byte private key
         * @return signature in binary format.  Call Codec.encode64 (BCHserialize.kt) to convert to the exact signature format used by the bitcoin wallet's signmessage */
    public fun signMessage(message: ByteArray, secret: ByteArray): ByteArray?

    /** Verify a message using the same algorithm as the original bitcoin wallet's signmessage functionality
         * @message The raw bytes of the message to be verified
         * @address The address raw bytes (without the type prefix)
         * @return The pubkey that was used to sign, if the signature is valid, otherwise a zero size array. */
    public fun verifyMessage(message: ByteArray, address: ByteArray, signature: ByteArray): ByteArray?


    // data is the flattened "raw" script bytes.  "raw" means not wrapped into a BCHserialized
    public fun encodeCashAddr(chainSelector: ChainSelector, type: PayAddressType, data: ByteArray): String

    // Returns an array with the first byte being the PayAddressType, and the rest the address bytes
    // This API is not stable.  It should be called only by the PayAddress wrapper class
    public fun decodeCashAddr(chainSelector: ChainSelector, addr: String): ByteArray

    public fun groupIdToAddr(chainSelector: ChainSelector, data: ByteArray): String

    public fun groupIdFromAddr(chainSelector: ChainSelector, addr: String): ByteArray

    public fun extractFromMerkleBlock(numTxes: Int, merkleProofPath: ByteArray, hashes: Array<ByteArray>): Array<ByteArray>?

    public fun secureRandomBytes(amt: Int): ByteArray

    public fun generateBip39SecretWords(bytes: ByteArray, wordList: Array<String> = englishWordList) = GenerateBip39SecretWords(bytes, wordList)

    /** Solve the passed CAPD message, modifying it.  Return true if it was solved. */
    public fun capdSolve(capdMsg: CapdMsg): Boolean

    /** Return true if this CAPD message is solved. */
    public fun capdCheck(capdMsg: CapdMsg): Boolean

    /** Return the hash of this message. */
    public fun capdHash(capdMsg: CapdMsg): ByteArray?

    /** AES256 encryption of arbitrary length messages.
     * For variable sized messages, use [encryptAES256]. Don't store your own length and use an if statement to
     * pick this or that function unless you've already stored the length.
     * @secret: Must be a 32 bytes
     * @salt: Hashed in conjunction with the secret to initialize the starting point of the algorithm (the iv).
     *    You SHOULD use a different salt for every reuse of the secret.  The salt may be public.
     * @includeSalt: (default true).  Include the salt in the returned ciphertext.  This makes decryption convenient.
     * @return ciphertext or an empty array.  The ciphertext length will be different than the plaintext
     * */
    //public fun encryptAES256(message: ByteArray, secret: ByteArray, salt:Long):ByteArray

    /** AES256 decryption if you KNOW the message length is a multiple of 16 (and you encrypted with [encryptAES256by16])
     * For variable sized messages, use [decryptAES256]. Don't store your own length and use an if statement to
     * pick this or that function unless you've already stored the length.
     * @salt: The same salt that was used for encryption.  If null, use salt included in the ciphertext
     *
     * @return plaintext
     * */
    // public fun decryptAES256(message: ByteArray, secret: ByteArray, salt:Long):ByteArray

    /** AES256 encryption of arbitrary length messages.  A random initialization vector (IV) is generated and included with the ciphertext.
     * For the maximum size efficiency when the IV can be inferred or calculated and the size is known use [encryptAES256by16]
     * @secret: Must be a 32 bytes
     * @salt: Hashed in conjunction with the secret to initialize the starting point of the algorithm (the iv).
     *    You SHOULD use a different salt for every reuse of the secret.  The salt may be public.
     * @includeSalt: (default true).  Include the salt in the returned ciphertext.  This makes decryption convenient.
     * @return ciphertext or an empty array.  The ciphertext length will be different than the plaintext
     * */

    public fun encryptAES256(plaintext: ByteArray, secret: ByteArray):ByteArray
    {
        /*
        // Get some random data if needed then each time increment and hash it to form the iv
        if (aes256iv == null) aes256iv = secureRandomBytes(AES256_BLOCKSIZE).toTypedArray()
        aes256iv!!.let {
            it[0] = it[0]++
            if (it[0] == 0.toByte())
            {
                it[1]++
                if (it[1] == 0.toByte())
                {
                    it[2]++
                    if (it[2] == 0.toByte())
                    {
                        it[3]++
                        if (it[3] == 0.toByte()) it[4]++
                    }
                }
            }
        }
         */

        // Use the double sha256 hash of the secret and the plaintext to create the IV.  We will also use this value as a commitment (checksum) to the encrypted
        // contents.  This solves a problem in the case where the majority of the bits in the message are "free".  In this case an attacker could provide a random
        // ciphertext.  Form this as a merkle tree in case someone wants to use it as a (weak) commitment to the secret or plaintext.

        // However, a deterministic IV would leak knowledge about message repeats because the ciphertext (and the IV) would be exactly the same.
        // We use a 2 byte value to further obfuscate message repeats
        val incr = aes256ivCount++
        val incrB = byteArrayOf((incr and 255).toByte(), ((incr shr 8) and 255).toByte())

        var iv = libnexa.hash256(incrB + libnexa.sha256(plaintext) + libnexa.sha256(secret)).sliceArray(0 until AES256_BLOCKSIZE)

        // And divulge it as part of the IV so the receiver knows what to use to reform the checksum.
        // Knowledge of this value does not help the attacker determine message repeats because they would have to know the sha256 of the plaintext
        // and the sha256 of the secret.  However, if those commitments are revealed, then an attacker could determine all repeats of the plaintext
        // whose commitment was revealed by calculating all possible IVs, which is only 2^16 possibilities.

        // This gives a 14 byte checksum and commitment, and a 2 byte message count/uniquifier.
        iv[0] = (incr and 255).toByte()
        iv[1] = ((incr shr 8) and 255).toByte()

        // We could choose a location for the overflow byte (say the end) and then if the plaintext is a multiple of AES_BLOCKSIZE
        // and the last byte is not within the overflow range 0-15 we don't need to include it.  But this means the length of the
        // cyphertext would leak some information (last 4 bits) about the plaintext.
        // Instead we'll just be inefficient and always append an overflow byte.
        var overflow = (plaintext.size+1) % AES256_BLOCKSIZE  // overflow will include itself

        // this random doesn't have to be crypto, we are just filling the pad out with trash
        // put the overflow length byte a the end rather than the beginning so some implementation could optimize by just chopping existing buffer
        val pt = plaintext + Random.Default.nextBytes(AES256_BLOCKSIZE-overflow) + byteArrayOf(overflow.toByte())
        check(pt.size % AES256_BLOCKSIZE == 0)
        return iv + encryptAES256by16(pt, secret, iv)
    }

    /** AES256 decryption if you encrypted with [encryptAES256].
     * @return plaintext
     * */
    public fun decryptAES256(ciphertext: ByteArray, secret: ByteArray):ByteArray
    {
        val iv = ciphertext.slice(0 until AES256_BLOCKSIZE).toByteArray()
        val ct = ciphertext.drop(AES256_BLOCKSIZE).toByteArray()

        val plaintextBlock = decryptAES256by16(ct, secret, iv)
        val overflow = plaintextBlock.last().toInt()
        if (overflow < 0 || overflow >= 16) return byteArrayOf()  // bad decryption
        val plaintext = plaintextBlock.dropLast((AES256_BLOCKSIZE - overflow)+1).toByteArray()
        val incrB = iv.slice(0 until 2).toByteArray()
        var hash = libnexa.hash256(incrB + libnexa.sha256(plaintext) + libnexa.sha256(secret)).sliceArray(0 until AES256_BLOCKSIZE)
        hash[0] = incrB[0]
        hash[1] = incrB[1]
        if (hash contentEquals iv) return plaintext
        return byteArrayOf()  // hash-as-a-checksum does not match
    }


    /** AES256 encryption if you KNOW the message length is a multiple of 16.  This is a lower level API that offers greater size
     * efficiency if you know what you are doing.
     * For variable sized messages, use [encryptAES256].
     * @plaintext size must be a multiple of 16
     * @secret must be 32 bytes
     * @initial must be 16 bytes, must be probabilistically unique for the same secret
     * @return ciphertext or an empty array.  The ciphertext length will be the same as the plaintext.
     * */
    public fun encryptAES256by16(plaintext: ByteArray, secret: ByteArray, initial:ByteArray):ByteArray

    /** AES256 decryption if you KNOW the message length is a multiple of 16 (and you encrypted with [encryptAES256by16])
     * For variable sized messages, use [decryptAES256].
     * @plaintext size must be a multiple of 16
     * @secret must be 32 bytes
     * @initial must be 16 bytes, must be probabilistically unique for the same secret*
     * @return plaintext
     * */
    public fun decryptAES256by16(ciphertext: ByteArray, secret: ByteArray, initial:ByteArray):ByteArray
}

// public expect fun initializeLibNexa(): LibNexa
public lateinit var libnexa: LibNexa
