@file:OptIn(ExperimentalUnsignedTypes::class)

package org.nexa.libnexakotlin

import io.ktor.http.Url
import kotlinx.coroutines.newFixedThreadPoolContext
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.*
import kotlin.coroutines.CoroutineContext

private val LogIt = GetLog("BU.token")

val tokenCoCtxt: CoroutineContext = newFixedThreadPoolContext(2, "tokenLoad")

const val TOKEN_GENESIS_INFO_VERSION = 1.toByte()
@Serializable
data class TokenGenesisInfo(val document_hash: String?, val document_url: String?, val height: Long,
                            val name: String?, val ticker: String?, val token_id_hex: String,
                            val txid: String, val txidem: String,
                            val decimal_places:Int?=null, val op_return:String?=null):BCHserializable
{
    companion object
    {
        fun deserialize(stream: BCHserialized): TokenGenesisInfo
        {
            val ver = stream.debyte()
            if (ver != TOKEN_GENESIS_INFO_VERSION) throw DeserializationException("Invalid version")
            return TokenGenesisInfo(
                stream.denullString(),
                stream.denullString(),
                stream.deint64(),

                stream.denullString(),  // name
                stream.denullString(),  // ticker
                stream.deString(), // token_id_hex

                stream.deString(),
                stream.deString(),

                stream.deint32(),
                stream.denullString()
            )
        }
    }
    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        var ret = BCHserialized(format).add(TOKEN_GENESIS_INFO_VERSION).add(document_hash).add(document_url).addInt64(height)
            .add(name).add(ticker).add(token_id_hex).add(txid).add(txidem).addInt32(decimal_places?: 0).add(op_return)
        return ret
    }



    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        throw RuntimeException("Cannot inline deserialize a data class. use the companion!")
    }

}


const val TOKEN_DESC_VERSION = 1.toByte()
@Serializable
data class TokenDesc(
  val ticker: String,
  val name: String? = null,
  val summary: String? = null,
  val description: String? = null,
  val legal: String? = null,
  val creator: String? = null,
  val category: String? = null,
  val contact: Map<String, String>? = null,
  val icon: String? = null, // absolute or relative url to icon file

  // NFT extensions
  val nftId: String? = null, // hex hash of the NFT data file
  val nftUrl: String? = null, // one location to access the file

  // Won't actually be deserialized in the json TDD. Placed here for easy use (and saved to disk)
  var signedSlice: ByteArray? = null,
  var tddHash: ByteArray? = null,
  var tddSig: ByteArray? = null,
  var tddAddr: String? = null,
  var pubkey: ByteArray? = null,

  var marketUri: String? = null,  // Derived from the location of the token desc doc or other sources

  var genesisInfo: TokenGenesisInfo? = null
):BCHserializable
{
    companion object
    {
        fun BCHdeserialize(stream: BCHserialized): TokenDesc
        {
            val ver = stream.debyte()
            if (ver != TOKEN_DESC_VERSION) throw DeserializationException("Invalid version")
            return TokenDesc(
                stream.deString(),
                stream.denullString(),
                stream.denullString(),
                stream.denullString(),  // description
                stream.denullString(),  // legal
                stream.denullString(),  // creator
                stream.denullString(),  // category
                stream.demap<String, String>({ it.deString()},{ it.deString()}),

                stream.denullString(), // icon

                stream.denullString(),
                stream.denullString(),  // nftUrl

                stream.denullByteArray(), // signedSlice
                stream.denullByteArray(),
                stream.denullByteArray(),
                stream.denullString(),
                stream.denullByteArray(),  // pubkey

                stream.denullString(),
                stream.deNullable { TokenGenesisInfo.deserialize(it) }
            )
        }
    }

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        var ret = BCHserialized(format).add(TOKEN_DESC_VERSION).add(ticker).add(name).add(summary).add(description).add(legal).add(creator).add(category)
        val tmp = if (contact != null)
        {
            BCHserialized.map(contact, { BCHserialized(format).add(it)}, {BCHserialized(format).add(it)})
        }
        else
        {
            BCHserialized.map(mapOf<String,String>(), { BCHserialized(format).add(it)}, {BCHserialized(format).add(it)})
        }
        ret.add(tmp).add(icon).add(nftId).add(nftUrl)
            .addNullableVariableSized(signedSlice).addNullableVariableSized(tddHash).addNullableVariableSized(tddSig)
            .add(tddAddr).addNullableVariableSized(pubkey).add(marketUri).addNullable(genesisInfo)
        return ret
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        throw RuntimeException("Cannot inline deserialize a data class. use the companion!")
    }

}


/** Decode a token description document JSON file into a TokenDesc structure, the hash of the proper part of the TDD, and its signature bytes.
 * check the signature and fill TokenDesc pubkey with the resulting pubkey if an address is provided, and the signature matches that address. */
fun decodeTokenDescDoc(s: String, addr:PayAddress?):TokenDesc
{
    val js = kotlinx.serialization.json.Json {
        ignoreUnknownKeys = true
    }
    val je = try
    {
        js.decodeFromString(JsonElement.serializer(), s)
    }
    catch(e:Exception)
    {
        println("error decoding: $s")
        throw(e)
    }

    val jsonArray: JsonArray = je.jsonArray
    val tdjo = jsonArray[0].jsonObject
    val td = kotlinx.serialization.json.Json.decodeFromJsonElement(TokenDesc.serializer(), tdjo)
    val tDict = s.slice(IntRange(s.indexOf("{"), s.lastIndexOf("}")))
    val signedSlice = tDict.toByteArray()
    td.signedSlice = signedSlice
    td.tddHash = libnexa.sha256(signedSlice)
    td.tddAddr = addr.toString()
    if (jsonArray.size > 1)
    {
        val sigStr: String = jsonArray[1].jsonPrimitive.content
        val sig = Codec.decode64(sigStr)
        td.tddSig = sig
    }
    else td.tddSig = null

    if (addr != null)  // if an address is provided, check for valid signature
    {
        val pub = libnexa.verifyMessage(signedSlice, addr.data, td.tddSig!!)  // If you passed an address, you are expecting the TDD to be signed
        td.pubkey = pub
    }
    return td
}

fun decodeTokenDescDoc(grpId:GroupId, s: String, tokenGenesisTx: iTransaction):TokenDesc
{
    var addr: PayAddress? = null
    for (out in tokenGenesisTx.outputs)
    {
            val gi = out.script.groupInfo(out.amount)
            if (gi != null)
            {
                if (gi.groupId == grpId)  // genesis of group must only produce 1 authority output so just match the groupid
                {
                    //assert(gi.isAuthority()) // possibly but double check subgroup creation
                    addr = out.script.address
                    break
                }
            }
    }
    return decodeTokenDescDoc(s, addr)
}



fun getTokenInfo(grpId:GroupId, getEc: ()->ElectrumClient, iHave: TokenDesc? = null):TokenDesc
{
    var ec = getEc()

    fun<T> ecRetry(count: Int, f:()->T): T
    {
        return retry(count) {
            try
            {
                f()
            }
            catch (e: ElectrumRequestTimeout)
            {
                // LogIt.info(sourceLoc() + ": Rostrum is inaccessible loading token info for ${grpId.toHex()}")
                if (it > count) throw e
                ec.close("token info timeout")
                ec = getEc()
                null
            }
            catch (e: IllegalStateException)  // Thrown when TCP TLS session is created but not supported
            {
                if (it > count) throw e
                ec.close("TLS not supported")
                ec = getEc()
                null
            }
        }
    }

    try
    {
        // Getting genesis info can be time consuming so give extra time for it
        LogIt.info(sourceLoc() + ": getting genesis for ${grpId}")
        val tgi = iHave?.genesisInfo ?: ecRetry(10) { ec.getTokenGenesisInfo(grpId.toHex(), 10000) }
        iHave?.genesisInfo = tgi

        if (iHave?.pubkey == null)
        {
            LogIt.info(sourceLoc() + ": getting genesis tx for ${grpId}")
            val gTx = ecRetry(10) { ec.getTx(tgi.txid) }
            LogIt.info(sourceLoc() + ": getting document for ${grpId} (${tgi.document_url})")
            tgi.document_url?.let {
                val doc = Url(it).readText(5000, context = tokenCoCtxt)
                LogIt.info(sourceLoc() + ": got document for ${grpId} (${tgi.document_url})")
                val tdd = try
                {
                    val tdd = decodeTokenDescDoc(grpId, doc, gTx)
                    LogIt.info(sourceLoc() + ": decoded doc for ${grpId}")
                    tdd
                }
                catch (e: Exception)
                {
                    logThreadException(e, "cannot decode token doc for ${grpId}")
                    throw (e)
                }
                tdd.genesisInfo = tgi
                return tdd
            }
            // No document URL so not much I can do
            return TokenDesc(tgi.ticker ?: "", tgi.name, genesisInfo = tgi)
        }
        return iHave
    }
    catch(e:RetryExceeded)
    {
        throw ElectrumRequestTimeout()
    }
}

