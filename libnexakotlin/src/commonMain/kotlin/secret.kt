// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.nexa.libnexakotlin
@cli(Display.Simple, "A wrapper to a secret so the secret may remain encrypted for as long as possible (or not)")
abstract class Secret
{
    abstract fun getSecret(): ByteArray
}

class UnsecuredSecret(private val secretBytes: ByteArray) : Secret()
{
    override fun getSecret(): ByteArray
    {
        return secretBytes
    }
}

/*
// TODO: make this class's accessor function request the secret from the encrypted wallet
class SecuredSecret(private val secretBytes:ByteArray):Secret()
{
    override fun getSecret():ByteArray
    {
        return secretBytes
    }
}
*/
