// Copyright (c) 2024 Bitcoin Unlimited
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
// This file implements the CAPD (Counterparty and Protocol Discovery (CAPD)) subprotocol described in https://spec.nexa.org/network/capd/
@file:OptIn(ExperimentalUnsignedTypes::class)
package org.nexa.libnexakotlin

import com.ionspin.kotlin.bignum.BigNumber
import com.ionspin.kotlin.bignum.integer.BigInteger
import com.ionspin.kotlin.bignum.integer.Sign

private val LogIt = GetLog("lnk.capd")

// Create a sanity check solve time to prevent the code from appearing to hang as it attempts to solve a very difficult capd.
// This number could change as processor capabilities increase.  But ultimately it would determine whether the system needs to
// ask a 3rd party to do the solve.
var CapdSolvableCutoff = BigInteger.fromLong(268501008)  // bits: 0x1e000fff, takes about a minute on a desktop 1 cpu

fun Int.contains(f:CapdMsg.Fields): Boolean
{
    return (this.toInt() and f.ordinal)>0
}

/** A CAPD message (see https://spec.nexa.org/network/capd/) */
class CapdMsg(/** The message contents */
              var data:ByteArray? = null): BCHserializable
{
    enum class Fields(v: Byte)
    {
        NOOP(0), EXPIRATION(1), RESCINDHASH(2);

        fun contains(f: Fields): Boolean
        {
            return (this.ordinal and f.ordinal)>0
        }
    }

    companion object
    {
        const val VERSION = 0

        /** Create from network-serialized bytes */
        fun deserialize(serialized: ByteArray):CapdMsg
        {
            val ret = CapdMsg(null)
            ret.BCHdeserialize(BCHserialized(SerializationType.NETWORK, serialized))
            return ret
        }
    }

    /** Create from network-serialized bytes */
    constructor(serialized: BCHserialized):this(null)
    {
        BCHdeserialize(serialized)
    }

    var fields: Fields = Fields.NOOP
    /** When this message was created (seconds since epoch) */
    var createTime: ULong = 0UL
    /** When this message expires (seconds since createTime) max_int means never */
    var expiration: UShort = UShort.MAX_VALUE
    /** When the preimage of this is published, this message expires, null means no preimage */
    var recindHash: ByteArray? = null

    /** the message's proof of work target.  This should be adjusted based on your node's capd info */
    var difficultyBits: UInt = 0x1e0fffffU
    /** needed to prove this message's POW */
    var nonce: ByteArray? = null


    /** Solve this capd message (in preparation for sending) with the provided valid time */
    fun solve(time: Long? = null): Boolean
    {
        val workBa = libnexa.getWorkFromDifficultyBits(difficultyBits)
        val work = BigInteger.fromByteArray(workBa,Sign.POSITIVE)
        if (work > CapdSolvableCutoff) throw CapdTooDifficult("CAPD too difficult: Expected work to solve is ${work.toString()}")
        // LogIt.info(sourceLoc() + "Capd solving difficulty: 0x${difficultyBits.toString(16)}")
        if (time != null) createTime = time.toULong()
        val solve = libnexa.capdSolve(this)
        return solve
    }

    /** Check whether this capd message is solved */
    fun check(): Boolean
    {
        return libnexa.capdCheck(this)

    }

    fun hash(): ByteArray?
    {
        return libnexa.capdHash(this)
    }

    fun work(): BigInteger
    {
        val workBa = libnexa.getWorkFromDifficultyBits(difficultyBits)
        return BigInteger.fromByteArray(workBa, Sign.POSITIVE)
    }

    fun workAsByteArray(): ByteArray
    {
        return libnexa.getWorkFromDifficultyBits(difficultyBits)
    }

    fun setWork(work: Long)
    {
        TODO()
    }


    fun toByteArray():ByteArray
    {
        return BCHserialize(SerializationType.NETWORK).toByteArray()
    }
    fun toUByteArray():UByteArray
    {
        return BCHserialize(SerializationType.NETWORK).toUByteArray()
    }

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        val rs = recindHash ?: byteArrayOf(20)

        if (format == SerializationType.SCRIPTHASH)
        {
            val ret = BCHserialized(format)
            ret.addNullableVariableSized(data)
            ret.addUint64(createTime)
            ret.addVariableSized(rs)
            ret.addUint16(expiration)
            ret.addUint32(difficultyBits)
            return ret
        }
        else
        {
            val ret = BCHserialized(format)
            ret.addUint8(fields.ordinal)
            ret.addUint64(createTime)
            ret.addUint32(difficultyBits)
            ret.addNullableVariableSized(nonce)
            if (fields.contains(Fields.EXPIRATION))
                ret.addUint16(expiration)
            if (fields.contains(Fields.RESCINDHASH))
                ret.addVariableSized(rs)
            ret.addNullableVariableSized(data)
            return ret
        }
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        val f = stream.deuint8()
        createTime = stream.deuint64().toULong()
        difficultyBits = stream.deuint32().toUInt()
        nonce = stream.denullByteArray()
        if (f.contains(Fields.EXPIRATION))
            expiration = stream.deuint16().toUShort()
        else expiration = UShort.MAX_VALUE
        if (f.contains(Fields.RESCINDHASH))
            recindHash = stream.deByteArray()
        else recindHash = null
        data = stream.deByteArray()
        return stream
    }
}


/** A CAPD message (see https://spec.nexa.org/network/capd/) */
class CapdQuery(
    var cookie: Long, /** Reply Cookie */
    var type: Int,
    var start: Long,
    var quantity: Long,
    var content: ByteArray
): BCHserializable
{
    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        val ret = BCHserialized(format)
        ret.addUint32(cookie)
        ret.addUint8(type)
        ret.addUint32(start)
        ret.addUint32(quantity)
        ret.addVariableSized(content)
        return ret
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        cookie = stream.deuint32()
        type = stream.deuint8()
        start = stream.deuint32()
        quantity = stream.deuint32()
        content = stream.deByteArray()
        return stream
    }

    fun toByteArray():ByteArray = BCHserialize(SerializationType.NETWORK).toByteArray()
    fun toUByteArray():UByteArray = BCHserialize(SerializationType.NETWORK).toUByteArray()
}