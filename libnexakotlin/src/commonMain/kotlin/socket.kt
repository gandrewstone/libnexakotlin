package org.nexa.libnexakotlin

import io.ktor.network.sockets.InetSocketAddress
import io.ktor.network.sockets.Socket
import io.ktor.network.sockets.SocketTimeoutException
import io.ktor.network.sockets.aSocket
import io.ktor.network.sockets.isClosed
import io.ktor.network.sockets.openReadChannel
import io.ktor.network.sockets.openWriteChannel
import io.ktor.network.selector.SelectorManager
import io.ktor.network.sockets.aSocket
import io.ktor.utils.io.ByteReadChannel
import io.ktor.utils.io.ByteWriteChannel
import io.ktor.utils.io.CancellationException
import io.ktor.utils.io.errors.IOException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.isActive
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.nexa.threads.Mutex
import kotlin.coroutines.CoroutineContext


class SocketOptions(var noDelay: Boolean, var receiveBufferSize: Int, var sendBufferSize: Int, var timeout: Int = 3000, var tls:Boolean=false, var keepalive:Boolean = false)


class SocketAddress(val addr: String)

/** Close a connection to trigger an early wake */
expect fun awaitAvailable(cnxns: Collection<iTcpSocket>, timeout: Long = Long.MAX_VALUE): Boolean

interface iTcpSocket
{
    /** Attempt to connect */
    fun connect(name: String, port: Int, connectTimeout: Int, sockOptions: (SocketOptions.() -> Unit)?=null)

    /** Return true if the underlying connection is ok */
    fun isAlive():Boolean
    /** Close the underlying connection */
    fun close()
    fun finalize()

    /** Returns number of bytes that can be read without blocking.  If the platform cannot
     * determine this (TLS), -1 is returned */
    val availableForRead: Int

    /** Reads all available bytes to dst buffer and returns immediately or suspends if no bytes available
    @return number of bytes were read or -1 if the channel has been closed
     */
    fun readAvailable(dst: ByteArray, offset: Int, length: Int): Int

    /** Writes all src bytes and suspends until all bytes written. Causes flush if buffer filled up or when autoFlush Crashes if channel get closed while writing
     */
    fun writeFully(src: ByteArray, offset: Int, length: Int): Unit

    /** force any writes to actually be sent */
    fun flush()

    val localAddress:SocketAddress

    val remoteAddress:SocketAddress
}

expect fun TcpSocket(sendCtxt: CoroutineContext, receiveCtxt: CoroutineContext):iTcpSocket

/*
expect class TcpSocket(sendCtxt: CoroutineContext, receiveCtxt: CoroutineContext):iTcpSocket
{
    /** Attempt to connect */
    override fun connect(name: String, port: Int, connectTimeout: Int, sockOptions: (SocketOptions.() -> Unit)?)

    /** Return true if the underlying connection is ok */
    override fun isAlive():Boolean
    /** Close the underlying connection */
    override fun close()
    override fun finalize()

    /** Returns number of bytes that can be read without blocking.  If the platform cannot
     * determine this (TLS), -1 is returned */
    override val availableForRead: Int

    /** Reads all available bytes to dst buffer and returns immediately or suspends if no bytes available
    @return number of bytes were read or -1 if the channel has been closed
    */
    override fun readAvailable(dst: ByteArray, offset: Int, length: Int): Int

    /** Writes all src bytes and suspends until all bytes written. Causes flush if buffer filled up or when autoFlush Crashes if channel get closed while writing
     */
    override fun writeFully(src: ByteArray, offset: Int, length: Int): Unit

    /** force any writes to actually be sent */
    override fun flush()

    override val localAddress:SocketAddress

    override val remoteAddress:SocketAddress
}
*/

fun iTcpSocket.writeFully(str: String)
{
    val ba = str.encodeToByteArray()
    writeFully(ba, 0, ba.size)
}