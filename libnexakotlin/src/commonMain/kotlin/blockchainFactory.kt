package org.nexa.libnexakotlin


/** Construct the proper derived-class transaction object for the passed blockchain */
fun txFor(cs: ChainSelector): iTransaction
{
    if (cs.isNexaFamily) return NexaTransaction(cs)
    if (cs.isBchFamily) return BchTransaction(cs)
    throw IllegalArgumentException()
}

/** Construct the proper derived-class transaction object for the passed blockchain, based on the passed serialized bytes */
fun txFor(cs: ChainSelector, ser: BCHserialized): iTransaction
{
    if (cs.isNexaFamily) return NexaTransaction(cs, ser)
    if (cs.isBchFamily) return BchTransaction(cs, ser)
    throw IllegalArgumentException()
}


/** Construct the proper derived-class outpoint object for the passed blockchain, based on the passed serialized bytes */
fun outpointFor(cs: ChainSelector, ser: BCHserialized): iTxOutpoint
{
    if (cs.isNexaFamily) return NexaTxOutpoint(ser)
    if (cs.isBchFamily) return BchTxOutpoint(ser)
    throw IllegalArgumentException()
}

/** Construct the proper derived-class outpoint object for the passed blockchain, based on the transaction identifier, and output index.
 * You must provide the proper identifier for this blockchain (e.g. use txid for BCH and txidem for Nexa). */
fun outpointFor(cs: ChainSelector, idem: Hash256, idx:Long): iTxOutpoint
{
    if (cs.isNexaFamily) return NexaTxOutpoint(idem, idx)
    if (cs.isBchFamily) return BchTxOutpoint(idem, idx)
    throw IllegalArgumentException()
}

/** Construct the proper derived-class transaction object for the passed blockchain, based on the passed serialized hex string */
fun txFromHex(cs: ChainSelector, hex: String, serializationType: SerializationType = SerializationType.NETWORK): iTransaction
{
    if (cs.isNexaFamily) return NexaTransaction.fromHex(cs, hex, serializationType)
    if (cs.isBchFamily) return BchTransaction.fromHex(cs, hex, serializationType)
    throw IllegalArgumentException()
}

/** Construct the proper derived-class block header object for the passed blockchain, based on the passed serialized hex string */
fun blockHeaderFromHex(cs: ChainSelector, hex: String, serializationType: SerializationType = SerializationType.NETWORK): iBlockHeader
{
    if (cs.isNexaFamily) return NexaBlockHeader.fromHex(hex, serializationType)
    if (cs.isBchFamily) return BchBlockHeader.fromHex(hex, serializationType)
    throw IllegalArgumentException()
}

/** Construct the proper derived-class block object for the passed blockchain, based on the passed serialized hex string */
fun blockFromHex(cs: ChainSelector, hex: String, serializationType: SerializationType = SerializationType.NETWORK): iBlock
{
    if (cs.isNexaFamily) return NexaBlock(cs, BCHserialized(hex.fromHex(), serializationType))
    if (cs.isBchFamily) return BchBlock(cs, BCHserialized(hex.fromHex(), serializationType))
    throw IllegalArgumentException()
}

/** Construct the proper derived-class transaction input object for the passed blockchain, for the passed Spendable object.
 *  This does not solve the input (create a valid input script), since that would require the complete transaction. */
fun txInputFor(s: Spendable): iTxInput
{
    if (s.chainSelector.isNexaFamily) return NexaTxInput(s)
    if (s.chainSelector.isBchFamily) return BchTxInput(s)
    throw IllegalArgumentException()
}

/** Construct the proper derived-class transaction input object for the passed blockchain.
 * This object is created with default values that then need to be set to an actual input for use.
 * */
fun txInputFor(cs: ChainSelector): iTxInput
{
    if (cs.isNexaFamily) return NexaTxInput(cs)
    if (cs.isBchFamily) return BchTxInput(cs)
    throw IllegalArgumentException()
}

/** Construct the proper derived-class transaction output object for the passed blockchain
 * This object is created with default values that then need to be set to an actual output for use.
 * */
fun txOutputFor(cs: ChainSelector): iTxOutput
{
    if (cs.isNexaFamily) return NexaTxOutput(cs)
    if (cs.isBchFamily) return BchTxOutput(cs)
    throw IllegalArgumentException()
}

/** Construct the proper derived-class transaction output object for the passed blockchain, and initialize it from the passed serialized data
 */
fun txOutputFor(cs: ChainSelector,ser: BCHserialized): iTxOutput
{
    if (cs.isNexaFamily) return NexaTxOutput(cs, ser)
    if (cs.isBchFamily) return BchTxOutput(cs, ser)
    throw IllegalArgumentException()
}

/** Construct the proper derived-class transaction output object for the passed blockchain, and initialize it from the passed data
 */
fun txOutputFor(cs: ChainSelector, amount: Long, script: SatoshiScript): iTxOutput
{
    if (cs.isNexaFamily) return NexaTxOutput(cs, amount, script)
    if (cs.isBchFamily) return BchTxOutput(cs, amount, script)
    throw IllegalArgumentException()
}

/** Construct the proper derived-class block header object for the passed blockchain, and initialize it from the passed serialized data
 */
fun blockHeaderFor(cs: ChainSelector, ser: BCHserialized): iBlockHeader
{
    if (cs.isNexaFamily) return NexaBlockHeader(ser)
    if (cs.isBchFamily) return BchBlockHeader(ser)
    throw IllegalArgumentException()
}

/** Construct the proper derived-class merkle block object for the passed blockchain, and initialize it from the passed serialized data
 */
fun merkleBlockFor(cs: ChainSelector, ser: BCHserialized): iMerkleBlock
{
    if (cs.isNexaFamily) return NexaMerkleBlock(cs, ser)
    if (cs.isBchFamily) return BchMerkleBlock(cs, ser)
    throw IllegalArgumentException()
}

/** Construct the proper derived-class block object for the passed blockchain, and initialize it from the passed serialized data
 */
fun blockFor(cs: ChainSelector, ser: BCHserialized): iBlock
{
    if (cs.isNexaFamily) return NexaBlock(cs, ser)
    if (cs.isBchFamily) return BchBlock(cs, ser)
    throw IllegalArgumentException()
}
