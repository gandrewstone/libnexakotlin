package org.nexa.libnexakotlin
import com.ionspin.kotlin.bignum.integer.toBigInteger
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.runBlocking
import org.nexa.threads.*
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.coroutineContext

private val LogIt = GetLog("BU.init")

var runningTheTests = false
val SimulationHostIP = "10.0.2.2"
var defaultDbPrefix = if (runningTheTests) "guitest_" else if (REG_TEST_ONLY == true) "unittest_" else "blkhdr_"

/** add or remove elements in this list to control the servers that we use to get electrum data by default */
val nexaElectrum = mutableListOf( IpPort("electrum.nexa.org", DEFAULT_NEXA_TCP_ELECTRUM_PORT),
    IpPort("electrum.wallywallet.org", DEFAULT_NEXA_TCP_ELECTRUM_PORT),
    IpPort("seeder.nexa.org", DEFAULT_NEXA_TCP_ELECTRUM_PORT, true), // because many exposed full nodes run rostrum
    IpPort("134.209.85.180", DEFAULT_NEXA_TCP_ELECTRUM_PORT),
    IpPort("24.199.106.119", DEFAULT_NEXA_TCP_ELECTRUM_PORT),
    IpPort("207.154.205.96", DEFAULT_NEXA_TCP_ELECTRUM_PORT),
    IpPort("144.126.219.17", DEFAULT_NEXA_TCP_ELECTRUM_PORT),
    IpPort("172.232.204.215", DEFAULT_NEXA_TCP_ELECTRUM_PORT),
    IpPort("172.234.253.184", DEFAULT_NEXA_TCP_ELECTRUM_PORT),
    IpPort("95.179.224.135", DEFAULT_NEXA_TCP_ELECTRUM_PORT),
    IpPort("149.28.232.224", DEFAULT_NEXA_TCP_ELECTRUM_PORT),
    IpPort("67.219.101.42", DEFAULT_NEXA_TCP_ELECTRUM_PORT),
    IpPort("198.13.61.217", DEFAULT_NEXA_TCP_ELECTRUM_PORT)
    )

/** add or remove elements in this list to control the servers that we use to get electrum data by default */
val nexaTestnetElectrum = mutableListOf(IpPort("testnet-electrum.nexa.org", DEFAULT_NEXATEST_SSL_ELECTRUM_PORT),
    IpPort("testnet-electrum.wallywallet.org", DEFAULT_NEXATEST_TCP_ELECTRUM_PORT),
    IpPort("testnet-seeder.nexa.org", DEFAULT_NEXATEST_TCP_ELECTRUM_PORT, true),  // because many exposed full nodes run rostrum
    )


var cnxnMgrLock = Mutex()
var cnxnMgrs: MutableMap<ChainSelector, CnxnMgr> = mutableMapOf()

var blockchainLock = Mutex()
var blockchains: MutableMap<ChainSelector, Blockchain> = mutableMapOf()



fun GetCnxnMgr(chain: ChainSelector, name: String? = null, start:Boolean = true): CnxnMgr
{
    return cnxnMgrLock.synchronized {
        LogIt.info(sourceLoc() + " " + "Get Cnxn Manager")
        val existing = cnxnMgrs[chain]
        if (existing != null) return@synchronized existing

        val n = name ?: chainToURI[chain] ?: "unknown"
        val result = when (chain)
        {
            ChainSelector.NEXATESTNET ->
            {
                val port = BlockchainPort[ChainSelector.NEXATESTNET]!!
                val cmgr = MultiNodeCnxnMgr(n, ChainSelector.NEXATESTNET, arrayOf(IpPort("68.183.223.81",port), IpPort("testnetseeder.nexa.org", port, true), IpPort("nexa-testnet-seeder.bitcoinunlimited.info", port, true)))
                cmgr.desiredConnectionCount = 2 // nexa testnet won't have lots of nodes
                cmgr
            }
            ChainSelector.NEXAREGTEST ->
            {
                val port = BlockchainPort[ChainSelector.NEXAREGTEST]!!
                val cmgr = MultiNodeCnxnMgr(n, ChainSelector.NEXAREGTEST, arrayOf(IpPort("127.0.0.1",port), IpPort(SimulationHostIP,port)))
                cmgr.desiredConnectionCount = 1 // Regtest will just be running on 1 node
                cmgr
            }
            ChainSelector.NEXA ->
            {
                val port = BlockchainPort[ChainSelector.NEXA]!!
                val cmgr = MultiNodeCnxnMgr(n, ChainSelector.NEXA, arrayOf(IpPort("seeder.nexa.org", port, true),IpPort("68.183.223.81",port),IpPort("159.203.179.109",port)))
                cmgr.desiredConnectionCount = 4
                cmgr
            }
            ChainSelector.BCHTESTNET ->
            {
                val port = BlockchainPort[ChainSelector.BCHTESTNET]!!
                val cmgr = MultiNodeCnxnMgr(n, ChainSelector.NEXATESTNET, arrayOf(IpPort("testnet4-seed-bch.bitcoinforks.org",port), IpPort("testnet4-seed-bch.toom.im",port),IpPort("seed.tbch4.loping.net", port)))
                cmgr.desiredConnectionCount = 2
                cmgr
            }
            ChainSelector.BCHREGTEST ->
            {
                val port = BlockchainPort[ChainSelector.BCHREGTEST]!!
                val cmgr = MultiNodeCnxnMgr(n, ChainSelector.BCHREGTEST, arrayOf(IpPort("127.0.0.1",port))) // arrayOf(SimulationHostIP, LanHostIP)
                cmgr.desiredConnectionCount = 1 // Regtest will just be running on 1 node
                cmgr
            }
            ChainSelector.BCH ->
            {
                val port = BlockchainPort[ChainSelector.BCH]!!
                val cmgr = MultiNodeCnxnMgr(n, ChainSelector.BCH, arrayOf(IpPort("seed.bitcoinunlimited.info", port),IpPort("seed.bchd.cash",port)))
                cmgr.desiredConnectionCount = 3
                cmgr
            }
            // ^ is exhaustive:  else -> throw BadCryptoException("bad crypto code")
        }
        result.getElectrumServerCandidate = { chain -> ElectrumServerOn(chain) }
        if (start) result.start()
        cnxnMgrs[chain] = result
        result
    }
}

fun ElectrumServerOn(chain: ChainSelector): IpPort
{
    return when (chain)
    {
        ChainSelector.BCH -> IpPort("electrum.seed.bitcoinunlimited.net", DEFAULT_BCH_TCP_ELECTRUM_PORT).resolve()
        ChainSelector.BCHREGTEST -> IpPort(SimulationHostIP, 60401)
        ChainSelector.NEXAREGTEST -> IpPort(SimulationHostIP, 30401)
        // TODO: point these IPs to a seeder
        ChainSelector.NEXA -> nexaElectrum.random().resolve()
        ChainSelector.NEXATESTNET -> nexaTestnetElectrum.random().resolve()
        else -> throw BadCryptoException()
    }
}

fun GetBlockchain(chainSelector: ChainSelector, cnxnMgr: CnxnMgr, name: String? = null, start:Boolean = true, fullSync:Boolean = true, dbPrefix: String = defaultDbPrefix): Blockchain
{
    var created = false
    var bc = blockchainLock.synchronized {
        LogIt.info(sourceLoc() + " " + "Get Blockchain")
        val existing = blockchains[chainSelector]
        if (existing != null) return@synchronized existing
        created = true
        val result = when (chainSelector)
        {
            ChainSelector.BCHTESTNET -> Blockchain(
              ChainSelector.BCHTESTNET,
              name ?: "TBCH",
              cnxnMgr,
              bchTestnetGb,
              Hash256("000000000003cab8d8465f4ea4efcb15c28e5eed8e514967883c085351c5b134"),
              Hash256("000000000005ae0f3013e89ce47b6f949ae489d90baf6621e10017490f0a1a50"),
              1348366,
              "52bbf4d7f1bcb197f2".toBigInteger(16),
              dbPrefix,
                fullSync
            )

            // Regtest for use alongside testnet
            ChainSelector.BCHREGTEST -> Blockchain(
              ChainSelector.BCHREGTEST,
              name ?: "RBCH",
              cnxnMgr,
              // If checkpointing the genesis block, set the prior block id to the genesis block as well
              bchRegTestGb,
              Hash256("0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206"),
              Hash256("0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206"),
              0,
              0.toBigInteger(),
              dbPrefix, fullSync
            )

            // Bitcoin Cash mainnet chain
            ChainSelector.BCH -> Blockchain(
              ChainSelector.BCH,
              name ?: "BCH",
              cnxnMgr,
              genesisBlockHash = bchGb,
              checkpointPriorBlockId = Hash256("000000000000000002195635b4b14a0054eeaf6d219c521078e1297425381c3a"),
              checkpointId = Hash256("000000000000000004c366e89454a7c071e6293d398b45652128a10d38d21675"),
              checkpointHeight = 750500,
              checkpointWork = "19bba64c36dab2acd254f1c".toBigInteger(16),
              dbPrefix, fullSync
            )

            // Nexa regtest chain
            ChainSelector.NEXAREGTEST -> Blockchain(
              ChainSelector.NEXAREGTEST,
              name ?: "RNEX",
              cnxnMgr,
              genesisBlockHash = nexaRegTestGb,
              checkpointPriorBlockId = Hash256(),
              checkpointId = nexaRegTestGb,
              checkpointHeight = 0,
              checkpointWork = 0.toBigInteger(),
              dbPrefix, fullSync
            )
            /*
            // Nexa testnet chain
            ChainSelector.NEXATESTNET -> Blockchain(
              ChainSelector.NEXATESTNET,
              name ?: "TNEX",
              cnxnMgr,
              genesisBlockHash = nexaTestnetGb,
              checkpointPriorBlockId = Hash256("a37262e06459fb1c2e9ede06d2f040566c1428b53a40034734c6b3ba286d0f8d"),
              checkpointId = Hash256("b927b4ad1db13c2c32124f833cabeb0a69c61502ad388ebd7f596fa883adb3ec"),
              checkpointHeight = 130000,
              checkpointWork = "484b4becf5".toBigInteger(16),
              context = context,
              dbPrefix
            )
             */
            // Nexa testnet chain
            ChainSelector.NEXATESTNET -> Blockchain(
                ChainSelector.NEXATESTNET,
                name ?: "TNEX",
                cnxnMgr,
                genesisBlockHash = nexaTestnetGb,
                checkpointPriorBlockId = Hash256("8ea4fc146d2d018d94c9b5f7001f73afaeaaa1893398d4734aef467a35a7cac7"),
                checkpointId = Hash256("0b5d3d62544306b55e4a1535208598d54293a157993c768c5a140af71581506b"),
                checkpointHeight = 522033,
                checkpointWork = "0349ac26579d3".toBigInteger(16),
                dbPrefix, fullSync
            )

            ChainSelector.NEXA -> Blockchain(
                ChainSelector.NEXA,
                name ?: "NEX",
                cnxnMgr,
                genesisBlockHash = nexaGb,
                checkpointPriorBlockId = Hash256("e5e66f74dba9b0afc7cef3411367d55afcbd7cf18c5b2bd38d61da1ca53e5861"),
                checkpointId = Hash256("bbce9a879bbeae02df00b18e8b3a872639919eab5e7375e20de119b34ed75fce"),
                checkpointHeight = 507136,
                checkpointWork = "0183177f522680d21f2".toBigInteger(16),
                dbPrefix, fullSync
            )

            // ^ is exhaustive so no need for: else -> throw BadCryptoException()
        }

        blockchains[chainSelector] = result
        if (start)
        {
            result.start()
        }
        result
    }
    // If we just instantiated it & caller wants it to be up and running, give it some time to do that
    if (created && start) millisleep(500U)
    return bc
}

/*
fun checkWalletFilename(name: String, allowNonExistent: Boolean = false, mustNotExist: Boolean = false):File
{
    var f = File(name)
    val fNoExt:File = if (f.extension == ".db")
    {
        File(f.nameWithoutExtension)  // opening is going to add it on
    }
    else
    {
        f
    }
    if (mustNotExist)
    {
        val fWithExt = File(fNoExt.absolutePath + ".db")
        if (fWithExt.exists())
        {
            throw Error("Wallet file already exists: " + fWithExt.absolutePath)
        }
    }
    if (allowNonExistent) return fNoExt
    val fWithExt = File(fNoExt.absolutePath + ".db")
    if (!fWithExt.exists())
    {
        throw Error("File does not exist: " + fWithExt.absolutePath)
    }
    return fNoExt
}

 */

/** Connect to, and return an existing blockchain.  Create the blockchain object and start it processing, if it does not already exist */
fun connectBlockchain(cs: ChainSelector): Blockchain
    {
        blockchains[cs]?.let { return it }

        val cm = GetCnxnMgr(cs, start = true)
        val bc = GetBlockchain(cs, cm, start = true)
        blockchains[cs] = bc
        return bc
    }

/** Create a new wallet.  A HD BIP44 compliant recovery key will be automatically generated.
 * @param name Name of this wallet.  The name will be used as the wallet's display name, log name, and part of the wallet's filename.
 * @param cs Blockchain to attach this wallet to
 */
fun newWallet(name: String, bc:Blockchain): Bip44Wallet
{
    val wdb: WalletDatabase = openWalletDB(name, bc.chainSelector)!!  // One DB file per wallet for the desktop
    val w = try { Bip44Wallet(wdb, name) }
            catch (_: DataMissingException)
            {
                Bip44Wallet(wdb, name, bc.chainSelector, NEW_WALLET)

            }
    w.addBlockchain(bc, -1, -1)  // Since this is a new ram wallet (new private keys), there cannot be any old blocks with transaction
    return w
}

/** Create a new wallet.  A HD BIP44 compliant recovery key will be automatically generated.
 * @param name Name of this wallet.  The name will be used as the wallet's display name, log name, and part of the wallet's filename.
 * @param cs Blockchain to attach this wallet to
 */
fun newWallet(name: String, cs:ChainSelector): Bip44Wallet
{
    var bc = blockchains[cs]
    if (bc == null) bc = connectBlockchain(cs)
    return newWallet(name, bc)
}

/** Create a new HD BIP44 wallet from an recovery phrase.
 * @param name Name of this wallet.  The name will be used as the wallet's display name, log name, and part of the wallet's filename.
 * @param recoveryKey Secret recovery phrase.  The caller should ensure that this phrase adheres to recovery phrase standards.  It should contain the correct number of words separated by a single space.
 * @param cs Blockchain to attach this wallet to
 */
fun recoverWallet(name: String, recoveryKey: String, bc:Blockchain): Bip44Wallet
{
    //val f = checkWalletFilename(name, true)
    val wdb: WalletDatabase = openWalletDB(name)!!  // One DB file per wallet for the desktop
    val w = Bip44Wallet(wdb, name, bc.chainSelector, recoveryKey)
    w.addBlockchain(bc, bc.checkpointHeight, 0)  // Since this is a recovered wallet, look for old tx (start as early as possible)
    w
    return w
}

/** Create a new HD BIP44 wallet from an recovery phrase.
 * @param name Name of this wallet.  The name will be used as the wallet's display name, log name, and part of the wallet's filename.
 * @param recoveryKey Secret recovery phrase.  The caller should ensure that this phrase adheres to recovery phrase standards.  It should contain the correct number of words separated by a single space.
 * @param cs Blockchain to attach this wallet to
 */
fun recoverWallet(name: String, recoveryKey: String, cs:ChainSelector): Bip44Wallet
{
    var bc = blockchains[cs]
    if (bc == null) bc = connectBlockchain(cs)
    return recoverWallet(name, recoveryKey, bc)
}

/** Open an existing HD BIP44 wallet.
 * @param name Name of this wallet.  The name will be used as the wallet's display name, log name, and part of the wallet's filename.
 */
fun openWallet(name: String): Bip44Wallet
{
    val wdb = openWalletDB(name)!!  // One DB file per wallet for the desktop
    val w = try
    {
        Bip44Wallet(wdb, name)
    }
    catch (e: DataMissingException)
    {
        throw DataMissingException("Old, corrupt, or not a wallet file: " + name)
    }
    var bc = blockchains[w.chainSelector]
    if (bc == null) bc = connectBlockchain(w.chainSelector)
    w.addBlockchain(bc, bc.checkpointHeight, null)
    return w
}

/** Open this wallet if it exists, or create a new HD BIP44 wallet */
fun openOrNewWallet(name: String, cs:ChainSelector): Bip44Wallet
{
    try
    {
        return openWallet(name)
    }
    catch(e:Exception)
    {
        // println("Creating new wallet: $name")
        return newWallet(name, cs)
    }
}