// Copyright (c) 2020 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.nexa.libnexakotlin

import kotlin.reflect.*

enum class Display(val v: Int)
{
    Simple(1), User(2), Dev(3), None(9999)
}

enum class DisplayStyle(val v: Int)
{
    FieldPerLine(1), ObjectPerLine(2), OneLine(3),
    Help(4), NoTypes(5), ShortTypes(6), TypeSeparator(7)
}

typealias DisplayStyles = Set<DisplayStyle>

infix fun DisplayStyles.or(e:DisplayStyle):DisplayStyles
{
    val ret = this.toMutableSet()
    ret.add(e)
    return ret
}
infix fun DisplayStyles.or(s: DisplayStyles):DisplayStyles
{
    val ret = this.toMutableSet()
    for (e in s) ret.add(e)
    return ret
}

val DEFAULT_STYLE = setOf(DisplayStyle.FieldPerLine)

/** Indicates that a member field or function should be displayed in the command line interface (if one is enabled).
 * Note that the CLI only works for JVM platforms right now, because it requires reflection.
 * @param display Provides a user "capability" level to differentiate between simple commands and more esoteric or dangerous ones.
 * @param help Provides a short description of the this function or field
 * @param delve Should this member be displayed recursively?  That is, should it be displayed when the parent, grandparent, or Nth parent is being displayed?  If delve > recursive depth, then the field will be fully shown
 */
annotation class cli(val display: Display, val help: String, val delve: Int = 0)

/** Show a particular property of an object (internal CLI API) */
expect fun show(p: KProperty1<*, *>, obj: Any): String

/** Show the properties of an object (internal CLI API) */
expect fun showProperties(obj: Any, cls: KClass<*>, level: Display, style: DisplayStyles, depth: Int = 0, indent: Int = 0): String

/** Show the an object (internal CLI API) */
expect fun showObject(
    obj: Any,
    level: Display = Display.User,
    style: DisplayStyles = DEFAULT_STYLE,
    depth: Int = 0,
    indent: Int = 0,
    maxString: Int = 300
): String
expect fun showApis(obj: Any, level: Display = Display.User, style: DisplayStyles = setOf(DisplayStyle.OneLine)): String?


/** Shows or doesn't show the help and formats it depending on the display style */
fun showHelp(s: String?, style: DisplayStyles, indent: Int, helpIdentifier: String = ""): String
{
    if (s == null) return ""
    if (!style.contains(DisplayStyle.Help)) return ""
    if (style.contains(DisplayStyle.FieldPerLine)) return "\n" + "  ".repeat(indent + 1) + helpIdentifier + s
    else return "  (" + s + ")"
}

@Suppress("UNUSED_PARAMETER")
fun <K, V> MutableMap<K, V>.show(level: Display = Display.User, style: DisplayStyles = DEFAULT_STYLE, indent: Int = 0): String?
{
    return "{ ${this.size} elements }"
}

/*
fun<K,V> Map<K,V>.show(level: Display = Display.User, style: DisplayStyle = DisplayStyle.FieldPerLine, indent:Int = 0): String?
{
    return "{ ${this.size} elements }"
}
*/

fun showType(typeName: String?, style: DisplayStyles = DEFAULT_STYLE): String
{
    if (style.contains(DisplayStyle.NoTypes)) return ""
    val typeSeparator = if (style.contains(DisplayStyle.TypeSeparator)) ": " else ""
    if (typeName != null)
    {
        if (!style.contains(DisplayStyle.ShortTypes))
        {
            return typeSeparator + typeName
        }
        else
        {
            // short types translates like this:  module.foo<something.bar>  -> foo
            return typeSeparator + typeName.split('<').first().split('.').last()
        }
    }
    return ""
}

fun objectType(obj: Any, style: DisplayStyles = DEFAULT_STYLE): String = showType(obj::class.qualifiedName, style)

fun isPrimitive(obj: Any?): Boolean
{
    if (obj == null) return true
    return obj is Int || obj is Long || obj is Byte || obj is Boolean || obj is UInt || obj is ULong || obj is UByte
}

fun formatForDisplay(style: DisplayStyles, indent: Int, name:String, type:String, help:String, value:String):String
{
    val ret = StringBuilder()
    val newline = lineSeparator
    if (value.contains(newline))  // if the value isn't simple, than print help (on the same line) then the object
    {
        ret.append("  ".repeat(indent) + name + type + help + value)
    }
    else  // otherwise print the simple value and then the help
    {
        ret.append("  ".repeat(indent) + name + type + " = " + value + help)
    }
    return ret.toString()
}

fun cliDump(
    obj: Any?,
    level: Display = Display.User,
    style: DisplayStyles = DEFAULT_STYLE,
    depth: Int = 2,
    indent: Int = 0
): String
{
    if (obj == null) return "(null)"
    return showObject(obj, level, style, depth, indent) + "\n[" + (showApis(obj, level, setOf(DisplayStyle.OneLine))
        ?: "") + "]\n"
}

