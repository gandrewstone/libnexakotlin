package org.nexa.libnexakotlin
import kotlin.test.*

import com.ionspin.kotlin.bignum.integer.BigInteger
import com.ionspin.kotlin.bignum.integer.Sign
import org.nexa.threads.microsleep
import org.nexa.threads.millisleep
import org.nexa.nexarpc.*
import org.nexa.threads.Gate
import org.nexa.threads.iGate
import kotlin.math.abs

private val LogIt = GetLog("BlockchainTests.kt")

/** Database name prefix, empty string for mainnet, set for testing */
private var testDbPrefix = "unittest_"

fun Double.nearly(other: Double, epsilon:Double):Boolean
{
    return abs(this - other) < epsilon
}

fun deleteBlockchainDb(name: String)
{
    deleteDatabase(testDbPrefix + name)
}

fun waitForCallback0(timeout: Long, fn: ((()->Unit)->Unit)):Boolean
{
    val g = Gate()
    var woke = false
    fn { woke = true; g.wake() }
    val ret = g.timedwaitfor(timeout, {woke == true}) {}
    return (ret != null)
}

fun<T> waitForCallback(timeout: Long, fn: (((arg:T)->Unit)->Unit)):Boolean
{
    val g = Gate()
    var woke = false
    fn { woke = true; g.wake() }
    val ret = g.timedwaitfor(timeout, {woke == true}) {}
    return (ret != null)
}


class BlockchainTests
{
    companion object
    {
        init
        {
            initializeLibNexa()
        }
    }

    @BeforeTest
    fun setup()
    {
        REG_TEST_ONLY = true
    }

    @Test
    fun showTip()
    {
        org.nexa.threads.DefaultThreadExceptionHandler = {
            println(it)
            handleThreadException(it)
        }

        val cm = GetCnxnMgr(ChainSelector.NEXAREGTEST, "regtest", true)
        cm.exclusiveNodes(setOf(FULL_NODE_IP))
        val bc = GetBlockchain(ChainSelector.NEXAREGTEST, cm, "regtest", true, true, testDbPrefix)
        var i = 0
        var tip = bc.nearTip
        if (tip == null)
        {
            while(!bc.isSynced)  // loop for 5 seconds grabbing headers
            {
                println("$i Height: ${bc.curHeight} Cnxns: ${bc.net.p2pCnxns.size}")
                millisleep(1000U)
                i++
            }
            tip = bc.nearTip
        }
        var ret = showObject(tip!!, Display.Dev, depth = 1)
        println(ret)
    }

    @Test
    fun create()
    {
        org.nexa.threads.DefaultThreadExceptionHandler = {
            println(it)
            handleThreadException(it)
        }
        try
        {
            deleteBlockchainDb("regtest")
        }
        catch(e: org.nexa.libnexakotlin.LibNexaException)
        {
            if (e.message?.contains("with the Android application context") ?: false) return
            else throw e
        }
        val cm = GetCnxnMgr(ChainSelector.NEXAREGTEST, "regtest", true)
        cm.exclusiveNodes(setOf(FULL_NODE_IP))
        val bc = GetBlockchain(ChainSelector.NEXAREGTEST, cm, "regtest", true, true, testDbPrefix)
        var i = 0
        while(!bc.isSynced)  // loop for 5 seconds grabbing headers
        {
            println("$i Height: ${bc.curHeight} Cnxns: ${bc.net.p2pCnxns.size}")
            millisleep(1000U)
            i++
        }
        val tip = bc.nearTip
        assertTrue { tip != null }
        val tipheight = tip!!.height
        assertTrue { tipheight > 0 }
        println("tip height ${tip.height}")

        val blk1 = bc.blockHeader(1)
        println("block 1 hash ${blk1.hash.toHex()}")
        assertEquals(blk1.hashPrevBlock.toHex(), "d71ee431e307d12dfef31a6b21e071f1d5652c0eb6155c04e3222612c9d0b371")  // regtest genesis block hash
        val startClose = millinow()
        println("close at ${startClose}")
        bc.stop()
        val elapsed = millinow() - startClose
        println("complete close took $elapsed ms.")
        check(elapsed < 3000)  // the close should be done in a few seconds (or the blockchain run thread is not responding quickly enough)
    }

    @Test
    fun fastSync()
    {
        org.nexa.threads.DefaultThreadExceptionHandler = {
            println(it)
            handleThreadException(it)
        }
        try
        {
            deleteBlockchainDb("regtest")
        }
        catch(e: org.nexa.libnexakotlin.LibNexaException)
        {
            if (e.message?.contains("with the Android application context") ?: false) return
            else throw e
        }
        val cm = GetCnxnMgr(ChainSelector.NEXAREGTEST, "regtest", true)
        cm.exclusiveNodes(setOf(FULL_NODE_IP))
        val bc = GetBlockchain(ChainSelector.NEXAREGTEST, cm, "regtest", true, false, testDbPrefix)
        var i = 0
        while(!bc.isSynced)  // loop for 5 seconds grabbing headers
        {
            println("$i Height: ${bc.curHeight} Cnxns: ${bc.net.p2pCnxns.size}")
            millisleep(1000U)
            i++
        }
        val tip = bc.nearTip
        assertTrue { tip != null }
        val tipheight = tip!!.height
        assertTrue { tipheight > 0 }
        println("tip height ${tip.height}")

        val blk1 = bc.blockHeader(1)
        println("block 1 hash ${blk1.hash.toHex()}")
        assertEquals(blk1.hashPrevBlock.toHex(), "d71ee431e307d12dfef31a6b21e071f1d5652c0eb6155c04e3222612c9d0b371")  // regtest genesis block hash
        val startClose = millinow()
        println("close at ${startClose}")
        bc.stop()
        val elapsed = millinow() - startClose
        println("complete close took $elapsed ms.")
        check(elapsed < 3000)  // the close should be done in a few seconds (or the blockchain run thread is not responding quickly enough)
    }

    @Test
    fun capd()
    {
        // Connect
        val cm = GetCnxnMgr(ChainSelector.NEXAREGTEST, "regtest", true)
        cm.exclusiveNodes(setOf(FULL_NODE_IP))
        val bc = GetBlockchain(ChainSelector.NEXAREGTEST, cm, "regtest", true, false, testDbPrefix)
        val rpc = openRpc()

        // Clear out any old test data
        rpc.capdClear()

        // Insert a message (
        val origMessage = "test message"
        val msg = CapdMsg(origMessage.toByteArray())
        msg.solve(0)
        println("capd nonce is: ${msg.nonce?.toHex()}")
        check(msg.nonce != null)
        check(msg.nonce?.size != 0)

        val hash = msg.hash()
        val hashStr = hash?.reversed()?.toHex()
        println("capd hash is: ${hashStr}")
        check(hashStr != null)
        check(hash != null)
        check(hash.size == 32)
        check(!(hash.contentEquals(ByteArray(32, {0}))))
        check(Hash256(hash).toHex() == hashStr) // Hash256 reverses

        val cxn = bc.net.getNode()
        cxn.send(msg)
        check(msg.check())

        waitFor(10000) { rpc.capdList().size > 0}

        // Check that the message got into the node
        val msgDetails = rpc.capdGet(HashId(hash))
        println("msg details from rpc: $msgDetails")
        check(msgDetails.hash == hash.reversed().toHex())
        val data:ByteArray = msgDetails.data.lowercase().fromHex()
        check(data.decodeUtf8() == origMessage)
        val msgs = rpc.capdList()
        check(msgs.size == 1)
        check(msgs[0] == HashId(hash))

        val capdInfo = rpc.capdInfo()
        println(capdInfo)

        // Test getting capd info
        waitForCallback(10000, { cxn.reloadCapdInfo(it)})
        check(cxn.capdLocalPriority.nearly(capdInfo.minPriority, 0.1))
        check(cxn.capdRelayPriority.nearly(capdInfo.relayPriority, 0.1))
        check(cxn.capdHighestPriority.nearly(capdInfo.maxPriority, 0.1))


        // Test installing a message handler
        val rcvWaiter = Gate()
        val rcvdMsgs = mutableListOf<CapdMsg>()
        val monitorId = cxn.installMsgMonitor(byteArrayOf(0x72, 0x27)) {
            rcvdMsgs.addAll(it)
            println("Received ${it.size} capd messages, total ${rcvdMsgs.size}")
            rcvWaiter.wake()
        }

        // Try a message I generate
        val msg2 = CapdMsg(byteArrayOf(0x72, 0x27) + "more data1".toByteArray())
        var solved = msg2.solve(0)
        check(solved)
        cxn.send(msg2)

        millisleep(1000U)
        val msg3 = CapdMsg(byteArrayOf(0x72, 0x27) + "more data3".toByteArray())
        solved = msg3.solve(0)
        check(solved)
        cxn.send(msg3)

        // TODO, CAPD RPC call is not applying the new message to its own filters.  (uncomment when full node is fixed)
        // Try a message externally generated
        //val rpcMsg = rpc.capdSend(byteArrayOf(0x72, 0x27) + "more data2".toByteArray())
        //val expectedMsgs = 3

        val expectedMsgs = 2

        if (rcvWaiter.timedwaitfor(10000,{ rcvdMsgs.size == expectedMsgs }) {} != null)
        {
            println("Received ${rcvdMsgs[0]}")
        }
        else
        {
            check(false, { "timeout waiting for capd message" })
        }

        // Make sure I got each message (order is unknown)
        val possibles = mutableSetOf<String>("more data1", "more data2", "more data3")
        for (i in rcvdMsgs)
        {
            i.data?.let {
                val s = it.drop(2).toByteArray().decodeUtf8()
                println(s)
                check(possibles.remove(s))
            }
        }

        cxn.removeMsgMonitor(monitorId)

        rpc.capdClear()
        val capdInfo2 = rpc.capdInfo()
        check(capdInfo2.size == 0L)


    }
}