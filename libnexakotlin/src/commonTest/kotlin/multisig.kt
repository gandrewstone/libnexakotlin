// Copyright (c) 2024 Bitcoin Unlimited
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.nexa.libnexakotlin
import com.ionspin.kotlin.bignum.decimal.BigDecimal
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.newFixedThreadPoolContext
import kotlinx.coroutines.runBlocking
import org.nexa.libnexakotlin.*
import org.nexa.libnexakotlin.simpleapi.NexaScript
import org.nexa.nexarpc.NexaRpc
import org.nexa.nexarpc.NexaRpcFactory
import org.nexa.threads.Gate
import org.nexa.threads.Thread
import org.nexa.threads.millisleep
import kotlin.coroutines.CoroutineContext
import kotlin.test.Test
import kotlin.test.BeforeTest
private val LogIt = GetLog("lnk.test.multisig")

val NexaDbg = "http://localhost:7998"

val multisigCoCtxt: CoroutineContext = newFixedThreadPoolContext(4, "multisigTest")


class LowestPairByteArray:Comparator<Pair<ByteArray, *>>
{
    override fun compare(pa: Pair<ByteArray, *>, pb: Pair<ByteArray, *>): Int
    {
        val a = pa.first
        val b = pb.first
        if (a == null && b == null) return 0
        if (a == null) return -1
        if (b == null) return 1
        val minSize = min(a.size, b.size)
        for (i in 0 until minSize)
        {
            val cmp = a[i].toPositiveInt().compareTo(b[i].toPositiveInt())
            if (cmp != 0) return cmp
        }
        return a.size.compareTo(b.size)
    }
}

class LowestDestAddress:Comparator<PayDestination>
{
    override fun compare(a: PayDestination, b: PayDestination): Int
    {
        if (a == null && b == null) return 0
        if (a == null) return -1
        if (b == null) return 1
        return LowestByteArray().compare(a.pubkey, b.pubkey)
    }
}

open class ContractFormationInvitation(val name: String, val convoSecret:ByteArray)

class MultisigFormationInvitation(name: String, convoSecret:ByteArray, val nsigs:Int, val npubs:Int):ContractFormationInvitation(name, convoSecret)


class MultisigFormationProtocol(val wallet: Wallet, val mfi:MultisigFormationInvitation)
{
    val comm = CapdProtocolCommunication(wallet.blockchain, mfi.convoSecret, multisigCoCtxt)
    suspend fun abort(error: String): Error
    {

        return Error(error)
    }
    // Should save the destination in the wallet when complete
    suspend fun form(): InteractiveMultisigDestination
    {
        // val dest = wallet.getNewDestination()
        val dest = (wallet as Bip44Wallet).getDestinationAtIndex(0)  // for debug always get the same one

        val pubkey = dest.pubkey ?: throw abort("cannot form pubkey")

        comm.start()

        // TODO: encrypt communications to prevent eavesdropping and also spoofing

        // simple protocol with message type (1 byte) and then data

        // send message type 1: exchange pubkeys
        comm.send(byteArrayOf(1) + pubkey)

        // Get everyone else's pubkeys
        var pubs = mutableListOf<Pair<ByteArray,Secret?>>(Pair(pubkey, dest.secret))
        while (pubs.size < mfi.npubs)
        {
            val (ba, epochTime) = comm.receive()
            if (ba[0] == 1.toByte())
            {
                val extpub = ba.drop(1).toByteArray()
                if (extpub contentEquals pubkey) continue // Ignore my own message
                if (extpub.size != pubkey.size) throw abort("incorrectly sized pubkey")
                pubs.add(Pair(extpub,null))
                LogIt.info(sourceLoc() + ": Received pubkey ${extpub.toHex()}  (${pubs.size} of ${mfi.npubs})")
            }
        }

        // Everybody sorts the pubkeys to achieve the same order
        pubs.sortWith(LowestPairByteArray())

        // Form the multisig
        val msdest = InteractiveMultisigDestination(wallet.chainSelector, mfi.convoSecret, mfi.nsigs, pubs.map{ Bytes(it.first) }.toTypedArray(),
            pubs.map{ it.second}.toTypedArray(), comm) // index isn't used, maybe hint at where our original dest came from? dest.index)

        // TODO insert this destination into the wallet for saving/tracking
        wallet.injectReceivingAddresses(listOf(msdest))

        return msdest
    }
}

fun createUnlockingScript(template: SatoshiScript?, constraint: SatoshiScript?, satisfier: SatoshiScript?): SatoshiScript
{
    val ret = SatoshiScript(satisfier?.chainSelector ?: template?.chainSelector ?: constraint?.chainSelector ?: throw IllegalArgumentException("Provide at least one script"))
    if (template != null) ret.add(OP.push(template.toByteArray()))
    if (constraint != null) ret.add(OP.push(constraint.toByteArray()))
    if (satisfier != null )
        return ret + satisfier
    else return ret
}


class MultisigTest
{
    lateinit var rpc: NexaRpc
    @BeforeTest
    fun init()
    {
        initializeLibNexa()
        rpc = openRpc()
    }

    fun formMultisig(cs:ChainSelector, requiredSigs: Int, dests:List<ByteArray>): NexaScript
    {
        assert(requiredSigs <= dests.size) // or it is impossible to unlock
        assert(requiredSigs > 0) // Well if its 0 its anyone can spend which is likely not intended
        val ss = NexaScript(OP.push(requiredSigs), chainSelector = cs)
        dests.forEach { ss.add(OP.push(it))}
        ss.add(OP.push(dests.size)).add(OP.CHECKMULTISIGVERIFY)
        return ss
    }


    /** This test forms a multisig address, sends some coin to it, and then spends it.
     * It does so "manually" -- that is, the contracts are formed right here in this test, given a "sign" primitive.
     */
    @Test
    fun testmanualmultisig()
    {
        val cs = ChainSelector.NEXAREGTEST
        // create 3 wallets to act as participants in the multisig operation
        val parts = arrayOf(openOrNewWallet("w0", cs),openOrNewWallet("w1", cs),openOrNewWallet("w2", cs))

        // now get a destination from each wallet to form the multisig address
        //val dests = parts.map { it.getNewDestination() }
        val gdests = parts.map { it.getDestinationAtIndex(0) }  // for debug always get the same one
        // sort the destinations so that all participants produce the same multisig script
        val dests = gdests.sortedWith(LowestDestAddress())

        // if the wallet does not offer destinations with a simple public key, then it can't be used for multisig
        val msigTemplateScript = formMultisig(cs,dests.size/2 + 1, dests.map { it.pubkey!! })
        //val msigTemplateScript = formMultisig(cs,1, listOf(dests.first().pubkey!!))
        //val msigTemplateScript = formMultisig(cs,1, dests.map { it.pubkey!! })
        println("multisig script is ${msigTemplateScript.toAsm()}")

        val locking = msigTemplateScript.constraint()  // TODO rename fn call to locking()
        check(locking.type == SatoshiScript.Type.TEMPLATE)
        println("locking script is ${locking.toAsm()}")

        val addr = locking.address
        check(addr != null)
        // this check will only work if getDestinationAtIndex
        //check(addr.toString() == "nexareg:nqtsq99jmapuhdlh6rvuhy6jtvrt6p23kn3swasq4etq95pe")
        println("multisig address is $addr")

        // ok send some money to this address, using the full node
        val fundingTxIdem = rpc.sendtoaddress(addr.toString(), BigDecimal.fromInt(10000))
        println("funded with tx $fundingTxIdem")

        // Get info about this send
        val fundingTxDetails = rpc.gettransactiondetails(fundingTxIdem)
        println("$fundingTxDetails")
        val rawfundingTxBa = rpc.getrawtransaction(fundingTxIdem)
        val fundingTx = NexaTransaction.fromHex(cs, rawfundingTxBa.toHex())

        // Now lets spend this multisig
        val backAddr = rpc.getnewaddress()

        val spendingTx = NexaTransaction(cs)
        val outputIdx = fundingTx.findOutput(addr)
        val spendingUtxo = fundingTx.outputs[outputIdx]
        val inAmt = spendingUtxo.amount
        val inp = NexaTxInput(Spendable(cs, fundingTx.outpoints[outputIdx], inAmt))

        spendingTx.add(inp)
        spendingTx.add(NexaTxOutput(cs, inAmt-500, PayAddress(backAddr).lockingScript() ))
        val unsignedTx = spendingTx.BCHserialize(SerializationType.NETWORK).toByteArray()

        // sign the transaction
        val sig0 = libnexa.signTxOneInputUsingSchnorr(unsignedTx, NexaSigHashType().all().build() , 0, inAmt, msigTemplateScript.toByteArray(), dests[0].secret!!.getSecret())
        val sig1 = libnexa.signTxOneInputUsingSchnorr(unsignedTx, NexaSigHashType().all().build() , 0, inAmt, msigTemplateScript.toByteArray(), dests[1].secret!!.getSecret())
        // val sig2 = libnexa.signTxOneInputUsingSchnorr(unsignedTx, NexaSigHashType().all().build() , 0, inAmt, fundingTx.outputs[outputIdx].script.toByteArray(), dests[2].secret!!.getSecret())

        // see https://spec.nexa.org/cryptography/multisignature/?h=op_che#public-multisignature-op_checkmultisigverify for multisig formulation
        val satisfier = SatoshiScript(cs).add(OP.C3, OP.push(sig0),OP.push(sig1))
        //val satisfier = SatoshiScript(cs).add(OP.C1, OP.push(sig0))

        spendingTx.inputs[0].script = createUnlockingScript(msigTemplateScript, null, satisfier)

        println("Spend with: $spendingTx")
        // println("SPEND HEX: ${spendingTx.toHex()}")
        // println("$NexaDbg/tx/${spendingTx.toHex()}?idx=0")
        println("$NexaDbg/tx/${spendingTx.toHex()}?idx=0&utxo=${spendingUtxo.BCHserialize(SerializationType.NETWORK).toHex()}")

        val txid = rpc.sendrawtransaction(spendingTx.toHex())
        println("spending tx id is $txid")

        rpc.generate(1)
        parts.forEach { it.finalize()}
        println("done")
    }

    /** This test uses a LibNexaKotlin "destination" to form the multisig.  However this destination cannot "talk" to other entities
     * to form the full multisig.  Instead, the code creates 2 incorrect multisig with just 1 sig each.  Then the test digs into those broken
     * scripts and stitches together the correct multisig spend.
     */
    @Test
    fun testlocalmultisig()
    {
        val cs = ChainSelector.NEXAREGTEST
        // create 3 wallets to act as participants in the multisig operation
        val parts = arrayOf(openOrNewWallet("w0b", cs), openOrNewWallet("w1b", cs), openOrNewWallet("w2b", cs))

        parts.forEachIndexed { idx, it ->
            println("Wallet ${idx} secret: ${it.secretWords} (master private key: ${it.secret.toHex()})")
        }

        // now get a destination from each wallet to form the multisig address
        //val dests = parts.map { it.getNewDestination() }
        val gdests = parts.map { it.getDestinationAtIndex(0) }  // for debug always get the same one
        // sort the destinations so that all participants produce the same multisig script
        val sdests = gdests.sortedWith(LowestDestAddress())
        val pubs = sdests.map { Bytes(it.pubkey!!) }

        val mdests = mutableListOf<MultisigDestination>()

        gdests.forEachIndexed { idx, item ->
            val privs = Array<Secret?>(gdests.size, { if (it == idx) sdests[it].secret else null })
            println("Wallet ${idx} destination 0 secret: ${sdests[idx].secret!!.getSecret().toHex()}")
            val md = MultisigDestination(cs,2,pubs.toTypedArray(), privs)
            mdests.add(md)
            // parts[idx].add(md)  TODO: add a special destination into the wallet
        }


        // The multisig address in every participating wallet should be the same
        val addr = mdests[0].address
        mdests.forEach {
            check(it.address == addr)
        }

        println("Multisig Destination:\n  template is ${mdests[0].templateScript().toHex()} (NexaASM: ${mdests[0].templateScript().toAsm(" ")})" +
             "\n  locking script is: ${mdests[0].lockingScript().toHex()} (NexaASM: ${mdests[0].lockingScript().toAsm(" ")})" +
             "\n  address is $addr")

        // ok send some money to this address, using the full node
        val fundingTxIdem = rpc.sendtoaddress(addr.toString(), BigDecimal.fromInt(10000))
        println("Funded with tx: $fundingTxIdem")

        // Get info about this send
        val fundingTxDetails = rpc.gettransactiondetails(fundingTxIdem)
        println("$fundingTxDetails")
        val rawfundingTxBa = rpc.getrawtransaction(fundingTxIdem)
        println("Funding transaction: ${rawfundingTxBa.toHex()}")
        val fundingTx = NexaTransaction.fromHex(cs, rawfundingTxBa.toHex())


        // Now lets spend this multisig

        // get a normal address to spend it to
        val backAddr = rpc.getnewaddress()

        // create the transaction
        val spendingTx = NexaTransaction(cs)
        val outputIdx = fundingTx.findOutput(addr)  // find the output that paid to the multisig
        val spendingUtxo = fundingTx.outputs[outputIdx]
        val inAmt = spendingUtxo.amount
        val inp = NexaTxInput(Spendable(cs, fundingTx.outpoints[outputIdx], inAmt))  // set the new tx input to spend it

        spendingTx.add(inp)

        spendingTx.add(NexaTxOutput(cs, inAmt-500, PayAddress(backAddr).lockingScript() ))  // Add an output to spend to


        // Ideally the wallet's txCompleter discovers that we can sign an input and furnishes a sig
        /*
        for (w in parts)
        {
            w.txCompleter(spendingTx, 0, TxCompletionFlags.SIGN)
        }
         */

        val unsignedFlatTx = spendingTx.BCHserialize(SerializationType.NETWORK).toByteArray()
        val spend0 = mdests[0].unlockingScript(unsignedFlatTx, 0L,  NexaSigHashType().all().build(), inAmt)
        val spend1 = mdests[1].unlockingScript(unsignedFlatTx, 0L,  NexaSigHashType().all().build(), inAmt)
        val sp0 = spend0.parsed()
        val sp1 = spend1.parsed()
        check(sp0[0] contentEquals sp1[0], { "template script mismatch"})
        check(!(sp0[1] contentEquals sp1[1]), { "multisig bit vector is the same: ${sp0[1].toHex()}"})
        val bv0 = OP.parse(sp0[1]).number!!
        val bv1 = OP.parse(sp1[1]).number!!
        check(bv0 != bv1)

        val cbv = bv0 or bv1

        val combinedScript = SatoshiScript(cs)
        combinedScript.add(sp0[0]) // including the raw data (opcode and bytes)
        combinedScript.add(OP.push(cbv))
        if (bv0 < bv1) // push whichever one has the lowest bitvector first
        {
            combinedScript.add(sp0[2]) // including the raw data (opcode and bytes)
            combinedScript.add(sp1[2])
        }
        else
        {
            combinedScript.add(sp1[2])
            combinedScript.add(sp0[2])
        }

        println(spend0.toAsm(" "))
        println(spend1.toAsm(" "))
        println("combined")
        println(combinedScript.toAsm(" "))
        spendingTx.inputs[0].script = combinedScript
        println("$NexaDbg/tx/${spendingTx.toHex()}?idx=0&utxo=${spendingUtxo.BCHserialize(SerializationType.NETWORK).toHex()}")

        val txid = rpc.sendrawtransaction(spendingTx.toHex())
        println("spending tx id is $txid")
        val txdetails = rpc.gettransactiondetails(txid)
        println(txdetails)
        check(txdetails.in_txpool)
        rpc.generate(1)
        println("done")
        val txpool = rpc.gettxpoolinfo()
        check(txpool.size == 0L)
        parts.forEach { it.finalize() }
    }

    /**  This tests a full multi-party multisig, using destinations that are capable of communicating with each other.
     */
    @Test
    fun testmultisig()
    {
        val cs = ChainSelector.NEXAREGTEST
        // create 3 wallets to act as participants in the multisig operation
        val wallets = arrayOf(openOrNewWallet("w0c", cs), openOrNewWallet("w1c", cs), openOrNewWallet("w2c", cs))
        //val wallets = arrayOf(forceNewWallet("w0", cs), forceNewWallet("w1", cs), forceNewWallet("w2", cs))
        wallets[0].blockchain.req.net.exclusiveNodes(setOf(FULL_NODE_IP))

        wallets.forEachIndexed { idx, it ->
            println("Wallet ${idx} secret: ${it.secretWords} (master private key: ${it.secret.toHex()})")
        }

        // Get a protocol conversation ID
        val mfi = MultisigFormationInvitation("test multisig", libnexa.secureRandomBytes(16), 2, 3)

        // Now you'd share this ID with all the participants

        // Every participant creates a formation object and builds the destination
        val formation = wallets.map { MultisigFormationProtocol(it, mfi) }

        // Simulate all wallets doing formation at the same time
        val destChannel = Channel<InteractiveMultisigDestination>(3)
        formation.forEach { launch { destChannel.send(it.form()) } }

        val mdests = mutableListOf<InteractiveMultisigDestination>()

        // Wait for formation
        runBlocking {
            mdests.add(destChannel.receive())
            println("constructed multisig 1")
            mdests.add(destChannel.receive())
            println("constructed multisig 2")
            mdests.add(destChannel.receive())
            println("constructed multisig 3")
        }

        // The multisig address in every participating wallet should be the same
        val addr = mdests[0].address
        mdests.zip(formation).forEach {
            check(it.first.address == addr)
            it.first.beginMonitoring(it.second.wallet)   // TODO, we'll need to do this for generic destinations in the wallet init code
        }

        println("Multisig Destination:\n  template is ${mdests[0].templateScript().toHex()} (NexaASM: ${mdests[0].templateScript().toAsm(" ")})" +
            "\n  locking script is: ${mdests[0].lockingScript().toHex()} (NexaASM: ${mdests[0].lockingScript().toAsm(" ")})" +
            "\n  address is $addr")

        // ok send some money to this address, using the full node
        val fundingTxIdem = rpc.sendtoaddress(addr.toString(), BigDecimal.fromInt(10000))
        println("Funded with tx: $fundingTxIdem")

        // Get info about this send
        val fundingTxDetails = rpc.gettransactiondetails(fundingTxIdem)
        println("$fundingTxDetails")
        val rawfundingTxBa = rpc.getrawtransaction(fundingTxIdem)
        println("Funding transaction: ${rawfundingTxBa.toHex()}")
        val fundingTx = NexaTransaction.fromHex(cs, rawfundingTxBa.toHex())

        // Now lets spend this multisig.  Let's pretend wallet 0 produces the proposal.

        val initiatingWal = wallets[0]
        // get a normal address to spend it to
        val backAddr = rpc.getnewaddress()

        // create the transaction
        val spendingTx = NexaTransaction(cs)
        val outputIdx = fundingTx.findOutput(addr)  // find the output that paid to the multisig
        val spendingUtxo = fundingTx.outputs[outputIdx]
        val inAmt = spendingUtxo.amount

        val sp = Spendable(cs, fundingTx.outpoints[outputIdx], inAmt)
        sp.backingPayDestination = mdests[0]
        val inp = NexaTxInput(sp)  // set the new tx input to spend it

        spendingTx.add(inp)

        spendingTx.add(NexaTxOutput(cs, inAmt-500, PayAddress(backAddr).lockingScript() ))  // Add an output to spend to

        val waiter = Gate()
        val tracker = initiatingWal.proposeSignTransaction(spendingTx)

        // todo completed callback
        //while (!tracker.completed) { millisleep(500U) }
        /*
        {
            println("Multisig signing status: ${it.statusString}")
            waiter.wake()
        }
         */

        var done:TxSpendingProposal? = null

        // This loop is a little contrived.  It monitors all 3 wallets and moves their proposals from proposed to accepted and then kicks out when
        // a proposal is complete.  Normally each participant would be independently doing this for their own proposals.
        while(done == null)  // todo completed callback
        {
            //waiter.timedwaitfor(1000, { (tracker.status == AsyncOpTracker.Status.ABORTED) || (tracker.status == AsyncOpTracker.Status.COMPLETED) }) { done = true }
            millisleep(5000U)
            // for this test, just accept every proposal coming into any wallet.
            wallets.forEach {
                it.foreachProposal {
                    if (it.valid && !it.accepted)
                    {
                        LogIt.info("Accepting Proposal")
                        it.accept()
                    }
                    if (it.completed) done=(it as? TxSpendingProposal)
                    it.completed  // once one is completed, we can stop this loop
                }
            }
        }

        val finishedTx = done!!.tx

        println("multisig unlocking script:\n" + finishedTx.inputs[0].script.toAsm("\n  "))

        println("$NexaDbg/tx/${finishedTx.toHex()}?idx=0&utxo=${spendingUtxo.BCHserialize(SerializationType.NETWORK).toHex()}")

        val txid = rpc.sendrawtransaction(finishedTx.toHex())
        println("spending tx id is $txid")
        val txdetails = rpc.gettransactiondetails(txid)
        println(txdetails)
        check(txdetails.in_txpool)
        rpc.generate(1)
        println("done")
        val txpool = rpc.gettxpoolinfo()
        check(txpool.size == 0L)

        wallets.forEach { it.finalize() }
    }
}
