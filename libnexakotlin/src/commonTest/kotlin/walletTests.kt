package org.nexa.libnexakotlin
import com.ionspin.kotlin.bignum.decimal.BigDecimal
import kotlinx.coroutines.delay
import org.nexa.nexarpc.HashId
import kotlin.test.*

import org.nexa.nexarpc.NexaRpc
import org.nexa.nexarpc.NexaRpcException
import org.nexa.nexarpc.NexaRpcFactory
import org.nexa.threads.millisleep
import kotlin.random.Random

// If your test environment's regtest full node is NOT located on localhost, you must define
// this variable to be the IP address of your full node (otherwise use null)
var FULL_NODE_IP:String = "localhost"
// Configure these values to connect to your NEXA regtest network instance
val RPC_USER = "regtest"
val RPC_PASSWORD = "regtest"

private val LogIt = GetLog("walletTests.kt")

/** Database name prefix, empty string for mainnet, set for testing */
private var dbPrefix = "nexa_unittest_"

fun forceNewWallet(name: String, cs:ChainSelector): Bip44Wallet
{
    val nm = dbPrefix + name
    deleteDatabase(nm)
    return newWallet(nm, cs)
}


class TestTimeoutException(what: String): Exception(what)
fun<T> waitFor(timeout: Int = 10000, lazyErrorMsg: (()->String)? = null, checkIt: ()->T?):T
{
    var count = timeout
    var ret:T? = checkIt()
    while(ret == null || ret == false)
    {
        millisleep(100U)
        count-=100
        if (count < 0 )
        {
            val msg = lazyErrorMsg?.invoke()
            if (msg != null) LogIt.error(msg)
            throw TestTimeoutException("Timeout waiting for predicate: $msg")
        }
        ret = checkIt()
    }
    return ret
}

fun mycheck(b:Boolean, onFail: (()->String?)? = null)
{
    if  (!b)
    {
        val v = onFail?.invoke()
        if (v != null) LogIt.severe("TEST FAILURE: " + v)
        throw IllegalStateException(v)
    }
}

fun openRpc(): NexaRpc
{
    val rpcConnection = "http://$RPC_USER:$RPC_PASSWORD@${FULL_NODE_IP}:" + NexaRegtestRpcPort
    LogIt.info("Connecting to: " + rpcConnection)
    val rpc = NexaRpcFactory.create(rpcConnection)
    val tipIdx:Long = rpc.getblockcount()
    if (tipIdx < 102)
        rpc.generate((101 - tipIdx).toInt())
    else
    {
        val tip = rpc.getblock(tipIdx)
        // The tip is so old that this node won't think its synced so we need to produce a block
        if (epochSeconds() - tip.time > 1000) rpc.generate(1)
    }
    return rpc
}

fun debugDumpState(w: Wallet, rpc: NexaRpc): String
{
    val sb = StringBuilder()
    val txpool = rpc.getrawtxpool()
    val txpools = txpool.map { it.toHex() }.joinToString("\n")

    sb.append("wallet: ${(w as CommonWallet).debugDump()}\ntxpool: ${rpc.gettxpoolinfo()} \n${txpools}")
    return sb.toString()
}


class WalletTests
{
    companion object
    {
        init
        {
            initializeLibNexa()
        }
    }

    @BeforeTest
    fun setup()
    {
        REG_TEST_ONLY = true
    }
    @Test
    fun create()  // expects that the REGTEST blockchain has produced a block within the last hour (or it will hang)
    {
        // Since we are testing creation from scratch, we need to delete the underlying blockchain if its already been started
        // in some other test.  To do that we need to close it first.  Doing this is not really part of the "normal" API...
        val tmp = blockchains[ChainSelector.NEXAREGTEST]
        if (tmp != null)
        {
            tmp.stop()
            blockchains.remove(ChainSelector.NEXAREGTEST)
            tmp.finalize()
        }
        if (deleteDatabase("unittest_RNEX") == false)
        {
            println("blockchain DB didn't need to be deleted")
        }


        var syncHash = Hash256()
        if (true)
        {
        org.nexa.threads.DefaultThreadExceptionHandler = {
            println(it)
            handleThreadException(it)
        }
        try
        {
            deleteDatabase(dbPrefix + "wal")
        }
        catch (e: org.nexa.libnexakotlin.LibNexaException)
        {
            if (e.message?.contains("with the Android application context") ?: false) return
            else throw e
        }
        val wal = newWallet(dbPrefix + "wal", ChainSelector.NEXAREGTEST)
        // point to your hard-coded regtest full node
        if (FULL_NODE_IP != null)
        {
            LogIt.info("running in exclusive node mode to $FULL_NODE_IP")
            wal.blockchain.req.net.exclusiveNodes(setOf(FULL_NODE_IP!!))
        }

        var matched = 0
        while (!wal.synced())
        {
            println("wal not synced! at: ${wal.chainstate?.syncedHeight}  chain at: ${wal.blockchain.curHeight}")
            if (wal.chainstate?.syncedHeight == wal.blockchain.curHeight)
            {
                matched++
                if (matched > 5) println("hang?  you probably haven't produced a regtest block in an hour...")
            }
            millisleep(1000U)
        }
            syncHash = wal.chainstate!!.syncedHash

            wal.save(true)
            wal.finalize()
        }


        // Test reopen
       if (true)
       {
           val wal = openWallet(dbPrefix + "wal")
           check(wal.chainSelector == ChainSelector.NEXAREGTEST)
           check(wal.chainstate!!.syncedHash == syncHash)
           wal.finalize()
       }

    }

    fun commitTxPool(rpc: NexaRpc)
    {
        while(rpc.gettxpoolinfo().size>0)
        {
            rpc.generate(1)
        }
    }

    @Test
    fun WalletBalance()
    {
        var rpc = openRpc()
        commitTxPool(rpc) // clean up

        //val feeder = openWallet("feederWallet")
        //val w = openWallet("largeWallet")
        //val w = openOrNewWallet("rewind", ChainSelector.NEXAREGTEST)
        val w = forceNewWallet("balance", ChainSelector.NEXAREGTEST)

        waitFor(500000, { "Wallet did not initially sync (at " + w.syncedHeight + ". Chain is at height: " + rpc.getblockcount() }, { w.synced() })
        println("wait to settle")
        millisleep(5000U)
        println("send to this wallet from full node")
        val myAddr = w.getNewDestination()
        println("expecting an unconfirmed tx to be received")
        val sendAmt = 2000L
        var txrpc = rpc.sendtoaddress(myAddr.address.toString(), BigDecimal.fromLong(sendAmt/100L))
        val unconfTx:TransactionHistory? = try
        {
            waitFor(10000, { "wallet did not receive unconfirmed tx: " + txrpc.toHex() }, { w.getTx(Hash256(txrpc.hash)) })
        }
        catch(e:Exception)
        {
            throw e
        }
        mycheck(unconfTx!!.isUnconfirmed(), {"tx is not unconfirmed"})
        mycheck(w.balanceUnconfirmed == sendAmt)

        rpc.generate(1)
        println("block generated")
        val txhash = Hash256(txrpc.hash)
        val tx = waitFor(100000, {
            "wallet did not confirm tx: ${txrpc.toHex()}\n${debugDumpState(w, rpc)}" }, {
            val txh = w.getTx(txhash)
            if (txh==null) null
            else if (txh.isUnconfirmed() == true) null
            else txh
        })
        println(tx)
        mycheck(w.balanceUnconfirmed == 0L)
        mycheck(w.balance == sendAmt)

        val myAddr2 = w.getNewDestination()
        rpc.sendtoaddress(myAddr2.address.toString(), SatToNexa(sendAmt))
        rpc.sendtoaddress(myAddr.address.toString(), SatToNexa(sendAmt))

        waitFor(5000, {"balances incorrect: ${w.balance} (${w.balanceUnconfirmed} unconf)"},
            { if (w.balanceUnconfirmed == 2*sendAmt && w.balance == 3*sendAmt) true else null })

        // Check operation of forEachUtxoWithAddress
        var addrUtxos = 0
        w.forEachUtxoWithAddress(myAddr.address!!) {
            addrUtxos++
            false
        }
        mycheck(addrUtxos == 2)

        addrUtxos = 0
        w.forEachUtxoWithAddress(myAddr2.address!!) {
            addrUtxos++
            false
        }
        mycheck(addrUtxos == 1)

        val myAddr3 = w.getNewDestination()
        addrUtxos = 0
        w.forEachUtxoWithAddress(myAddr3.address!!) {
            addrUtxos++
            false
        }
        mycheck(addrUtxos == 0)


        val fullNodeAddr = rpc.getnewaddress()

        w.send(sendAmt,fullNodeAddr, true)
        // These coins could be taken from either unconfirmed or confirmed tx so the balanceUnconfirmed is 1 of 2 possibilities
        waitFor(5000, {"balances incorrect: ${w.balance} (${w.balanceUnconfirmed} unconf)"},
            { if ((w.balanceUnconfirmed == 1*sendAmt || w.balanceUnconfirmed == 2*sendAmt) && w.balance == 2*sendAmt) true else null })

        rpc.generate(1)

        waitFor(30000, {"balances incorrect: ${w.balance} (${w.balanceUnconfirmed} unconf)"},
            { if (w.balanceUnconfirmed == 0L && w.balance == 2*sendAmt) true else null })
        w.finalize()
    }

    @Test
    fun WalletRewind()
    {
        var rpc = openRpc()
        commitTxPool(rpc) // clean up

        //val feeder = openWallet("feederWallet")
        //val w = openWallet("largeWallet")
        //val w = openOrNewWallet("rewind", ChainSelector.NEXAREGTEST)
        val w = forceNewWallet("rwnd", ChainSelector.NEXAREGTEST)
        //val w = openWallet("rewind")

        _WalletRewind(w, rpc)

        LogIt.info("TestWallet tests complete, stopping temporary blockchain")
        w.finalize()
    }


    //@Test  // Goes for too long to be a CI test
    fun WalletRewindRepeat()
    {
        var rpc = openRpc()
        commitTxPool(rpc) // clean up

        for(i in 0 .. 100)
        {
            val w = forceNewWallet("rwnd", ChainSelector.NEXAREGTEST)
            _WalletRewind(w, rpc)
            LogIt.info("TestWallet tests complete, stopping temporary blockchain")
            w.finalize()
            w.delete()
        }
    }


    fun _WalletRewind(wallet: Wallet, rpc: NexaRpc)
    {
        rpc.generate(1) // If a prior test left the blockchain in a "tie" state, break the tie so that this test starts cleanly

        // Test sending to this wallet
        val myAddr = wallet.getNewDestination()
        var height = rpc.getblockcount().toLong()
        waitFor(500000, { "Wallet did not initially sync (at " + wallet.syncedHeight + ". Chain is at height: " + rpc.getblockcount() }, { wallet.synced(height) })

        // Send 1000
        var txrpc = rpc.sendtoaddress(myAddr.address.toString(), SatToNexa(1000))
        LogIt.info("Sent 1000 to: ${myAddr.address}")
        LogIt.info("Wallet initial height: " + wallet.syncedHeight)
        LogIt.info("Initial funding tx:" + txrpc.toString())

        waitFor(10000, { "Unconfirmed balance incorrect. Was: " + wallet.balanceUnconfirmed + "  Expecting: " + 1000 }, { wallet.balanceUnconfirmed == 1000L })
        mycheck(wallet.balanceConfirmed == 0.toLong(), { "Confirmed balance incorrect" })

        val block1Hash = rpc.generate(1)[0]
        LogIt.info("Moving forward to " + block1Hash)
        height = rpc.getblockcount()
        waitFor(50000, { "wallet did not sync to " + height + ".  Its at height: " + rpc.getblockcount() }, { wallet.synced(height) })

        waitFor(10000, { "Unconfirmed balance should be 0, is: " + wallet.balanceUnconfirmed.toString() }, { wallet.balanceUnconfirmed == 0L })
        waitFor(4000, { "Confirmed balance should be 1000, is: " + wallet.balanceConfirmed.toString() }, { wallet.balanceConfirmed == 1000L })

        rpc.invalidateblock(block1Hash)
        LogIt.info("Invalidated " + block1Hash + " abandoning tx " + txrpc.toString())
        // race condition abandoning the transaction so loop until it works.  (It may not have been re-processed yet after the block invalidate)
        waitFor(5000, { "unwound tx ${txrpc.toHex()} didn't appear in txpool" + debugDumpState(wallet, rpc) }, { rpc.getrawtxpool().contains(txrpc) })
        rpc.abandontransaction(txrpc)  // This RPC is not provided by the library so fall back to the query interface
        mycheck(rpc.getrawtxpool().size == 0, { "Expecting empty txpool, found: " + rpc.getrawtxpool().toString() + "\n  tx that should be abandoned: " + rpc.getrawtransaction(txrpc).toString() })

        val forkHashes = rpc.generate(2)
        LogIt.info("----------- Forked with " + forkHashes[0] + " and " + forkHashes[1] + " --------------")
        // Wait for the wallet to catch up to this new fork
        val forkHeight = rpc.getblockcount()
        wallet.sync(100000, forkHeight)
        // Whether the wallet completely "forgets" the tx or holds it unconfirmed depends on whether the tx was noticed by the wallet to be confirmed
        // or not, so we'll
        //val cw = (wallet as CommonWallet)
        //cw.cleanUnconfirmed(cw.unconfirmedTx())

        // Wait for this wallet to sync against the new header chain
        waitFor(50000, { "Post reorg, unconfirmed balance incorrect. Was: " + wallet.balanceUnconfirmed + "  Expecting: " + 0 }, { wallet.balanceUnconfirmed == 0L })
        waitFor(1000000, { "Post reorg confirmed balance incorrect, is " + wallet.balanceConfirmed }, { wallet.balanceConfirmed == 0L })

        LogIt.info("TestWalletRewind simple rewind completed")

        // ----------------------------
        // Send 2 tx to this wallet and one back, invalidating their committed block but replay 1 tx
        LogIt.info("Txpool size is " + rpc.getrawtxpool().size)

        val myAddr2 = wallet.getNewDestination()

        // Send 3000 in 2 transactions
        txrpc = rpc.sendtoaddress(myAddr.address.toString(), SatToNexa(1000))
        val txrpc2 = rpc.sendtoaddress(myAddr2.address.toString(), SatToNexa(2000))

        LogIt.info("1000 in tx ${txrpc.toHex()}, 2000 in tx ${txrpc2.toHex()}")

        waitFor(5000, { "rpc sendtoaddress did not work"}, {rpc.getrawtxpool().size == 2})
        mycheck(wallet.synced(rpc.getblockcount()), { "Wallet did not initially sync (at " + wallet.syncedHeight + ". Chain is at height: " + rpc.getblockcount() })

        waitFor(5000, { "Unconfirmed balance incorrect. Was: " + wallet.balanceUnconfirmed + "  Expecting: " + 3000 }, { wallet.balanceUnconfirmed == 3000L })
        mycheck(wallet.balanceConfirmed == 0.toLong(), { "Confirmed balance incorrect" })

        millisleep(3000U)
        val block2Hash = rpc.generate(1)[0]
        val block2Height = rpc.getblockcount()
        waitFor(5000, { "TXes were not confirmed, TXPOOL is still: ${rpc.getrawtxpool()}" }, { rpc.getrawtxpool().isEmpty() })

        val btcdAddr = rpc.getnewaddress()

        LogIt.info(debugDumpState(wallet, rpc))

        // Send the tx we will unwind and get it committed in a block
        val txSendBack = wallet.send(2000, btcdAddr, deductFeeFromAmount = true)
        println("\nTX we will unwind sends back to full node: " + txSendBack.idem.toHex())
        waitFor(10000,{""}, { rpc.getrawtxpool().contains(HashId(txSendBack.idem.hash)) })
        waitFor(5000, { "tx did not enter txpool" }, { rpc.getrawtxpool().size == 1 })
        println("** Generating block")
        val block3Hash = rpc.generate(1)[0]
        val block3HashHeight = rpc.getblockcount()
        val block3Contents = rpc.getblock(block3Hash)
        LogIt.info("Txpool size is " + rpc.getrawtxpool().size)
        wallet.sync(100000, block3HashHeight)
        println("Generated block: ${block3HashHeight}:${block3Hash} (we will unwind; should contain 1 tx sending 2000-fee back to full node)")
        println("block contents: $block3Contents")
        check(block3Contents.txidem.contains(HashId(txSendBack.idem.hash)))

        println("wallet balance: ${wallet.balance} (${wallet.balanceConfirmed}:${wallet.balanceUnconfirmed}) at ${wallet.syncedHeight}")
        waitFor(10000, { "Unconfirmed balance should be 0, is: " + wallet.balanceUnconfirmed.toString() }, { wallet.balanceUnconfirmed == 0L })
        // Bracket the expected amount to account for the tx fee
        waitFor(10000, { "Confirmed balance incorrect, is: " + wallet.balanceConfirmed.toString() }, { wallet.balanceConfirmed == 1000L })
        //LogIt.info("Wallet TXOs:\n" + txosToString(wallet.txos))

        wallet.forEachTxo {// everything should have been confirmed by the recent block
            check(it.spentUnconfirmed == false)
            check(it.commitUnconfirmed == 0L)
            false
        }

        println("syncing wallet")
        wallet.sync(100000, block3HashHeight)

        //println("EVERY TXO:")
        //wallet.forEachTxo {
        //    println(it.toString())
        //    false
        //}
        LogIt.info(debugDumpState(wallet, rpc))

        // Invalidate this branch
        rpc.invalidateblock(block3Hash)
        LogIt.info("Invalidated block: $block3HashHeight:$block3Hash")
        waitFor(5000, { "invalidated block's tx did not enter txpool" }, { rpc.getrawtxpool().size > 0 })

        LogIt.info("evicting txid from full node: ${txSendBack.id.toHex()} idem: ${txSendBack.idem.toHex()}")
        rpc.evicttransaction(txSendBack.id.toHex())  // Clear my tx out of the node's mempool
        waitFor(5000, { "can't remove tx from txpool" }, { rpc.getrawtxpool().size == 0 })

        val newfork = rpc.generate(3)
        val tiphash = newfork[2]
        height = rpc.getblockcount()
        LogIt.info("----------- Forked with " + newfork[0] + ", " + newfork[1] + ", " + newfork[2] + " --------------")

        waitFor(20000,{ "wallet will not sync: ${debugDumpState(wallet, rpc)}" }, {
            LogIt.info("RPC: $height:$tiphash  chain: ${wallet.blockchain.curHeight}:${wallet.blockchain.getTip()?.hash} wallet: ${wallet.syncedHeight}:${(wallet as CommonWallet).chainstate?.syncedHash} balance: ${wallet.balance} (${wallet.balanceConfirmed}:${wallet.balanceUnconfirmed})")
            wallet.synced(height) } )
        (wallet as CommonWallet).cleanUnconfirmed()

        LogIt.info(debugDumpState(wallet, rpc))
        wallet.forEachTxo {// everything should have been confirmed by the recent block
            check(it.spentUnconfirmed == false)
            check(it.commitUnconfirmed == 0L)
            false
        }

        // Now my send of 1000 should be unconfirmed, because we rewound and switched to a different fork, so should be back to 3000 balance
        mycheck(wallet.balanceUnconfirmed == 0L, { "Unconfirmed balance should be 0, is: ${wallet.balanceUnconfirmed}\n${debugDumpState(wallet, rpc)}" })
        mycheck(wallet.balanceConfirmed == 3000L, {"Confirmed balance should be 3000, is: ${wallet.balanceConfirmed}\n${debugDumpState(wallet, rpc)}" })

        rpc.invalidateblock(block2Hash)
        LogIt.info("invalidation txpool ${rpc.getrawtxpool()}")
        // does not work as a sync point: waitFor(10000, { "full node rewind did not finish" }, { rpc.getblockcount() < block2Height })
        // Nexa does not guarantee tx removed from txpool (or tx reinserted) so explicitly remove them.
        rpc.abandontransaction(txrpc)
        rpc.abandontransaction(txrpc2)

        // Rewind all the blocks in this test
        val finalfork = rpc.generate(5)  // I need enough blocks to exceed the other fork before this wallet will move over
        height = rpc.getblockcount()
        LogIt.info("----------- Final Reset with " + finalfork[0] + " --------------")

        waitFor(20000,{""}, {
            LogIt.info("RPC: $height  chain: ${wallet.blockchain.curHeight}  wallet: ${wallet.syncedHeight} balance: ${wallet.balance} (${wallet.balanceConfirmed}:${wallet.balanceUnconfirmed})")
            wallet.synced(height) } )

        LogIt.info(debugDumpState(wallet, rpc))

        waitFor(20000, { "Final post reorg, unconfirmed balance incorrect. Was: " + wallet.balanceUnconfirmed + "  Expecting: " + 0 }, { wallet.balanceUnconfirmed == 0L })
        waitFor(20000, { "Final post reorg confirmed balance incorrect, is " + wallet.balanceConfirmed + "  Expecting: " + 0 + "because the full node should have replayed the transaction we did not explicitly abandon."}, { wallet.balanceConfirmed == 0L })

        check(wallet.numTxos() == 0)

        LogIt.info("TestWalletRewind completed")
    }


    @Test
    fun emptyRediscover()
    {
        var rpc = openRpc()

        val REPEAT = 0
        val cs = ChainSelector.NEXAREGTEST
        LogIt.info("Test wallet syncing from genesis")
        val w = openOrNewWallet("emptyWallet", cs)
        w.chainstate?.prehistoryDate = 0
        w.chainstate?.prehistoryHeight = 0

        for(i in 0..REPEAT)
        {
            rpc.generate(1)  // In case the blockchain hasn't generated a block in a long time
            w.rediscover()
            millisleep(3000U)
            if (!w.sync(500000))
            {
                throw Exception("Wallet sync timeout")
            }

            LogIt.info("POST REDISCOVER")
            val wi = w.statistics()
            LogIt.info("Wallet: ${w.balance} ${w.balanceConfirmed}:${w.balanceUnconfirmed} at ${w.chainstate?.syncedHeight}  Transactions: ${w.numTx()}  Txos: ${w.numTxos()} Utxos: ${w.numUtxos()} Addrs: ${wi.numUsedAddrs} of ${wi.numUsedAddrs + wi.numUnusedAddrs}")
            w.save(true)
        }
        w.finalize()
    }


    // this needs to be run after you have a wallet:
    // @Test
    fun largeWalletInfo()
    {
        LogIt.info("Test wallet syncing from genesis")
        //val cs = ChainSelector.NEXAREGTEST
        //val w = ForceNewWallet("largeWallet", cs)

        val feeder = openWallet("feederWallet")
        val w = openWallet("largeWallet")

        if (!w.sync(500000))
        {
            throw Exception("Wallet sync timeout")
        }
        if (!feeder.sync(500000))
        {
            throw Exception("Wallet sync timeout")
        }

        var wi = w.statistics()
        LogIt.info("Wallet: ${w.balance} ${w.balanceConfirmed}:${w.balanceUnconfirmed} at ${w.chainstate?.syncedHeight}  Transactions: ${w.numTx()}  Txos: ${w.numTxos()} Utxos: ${w.numUtxos()} Addrs: ${wi.numUsedAddrs} of ${wi.numUsedAddrs + wi.numUnusedAddrs}")
        var fi = feeder.statistics()
        LogIt.info("Feeder Wallet: ${feeder.balance} ${feeder.balanceConfirmed}:${feeder.balanceUnconfirmed} at ${feeder.chainstate?.syncedHeight}  Transactions: ${feeder.numTx()}  Txos: ${feeder.numTxos()} Utxos: ${feeder.numUtxos()} Addrs: ${fi.numUsedAddrs} of ${fi.numUsedAddrs + fi.numUnusedAddrs}")

        w.finalize()
        feeder.finalize()
    }

    // this needs to be run after you have a wallet:
    // @Test
    fun rediscoverLargeWallet()
    {
        val cs = ChainSelector.NEXAREGTEST
        LogIt.info("Test wallet syncing from genesis")

        val w = openOrNewWallet("largeWallet", cs)

        w.rediscover()
        millisleep(3000U)
        if (!w.sync(5000*1000))
        {
            throw Exception("Wallet sync timeout")
        }

        LogIt.info("POST REDISCOVER")
        val wi = w.statistics()
        LogIt.info("Wallet: ${w.balance} ${w.balanceConfirmed}:${w.balanceUnconfirmed} at ${w.chainstate?.syncedHeight}  Transactions: ${w.numTx()}  Txos: ${w.numTxos()} Utxos: ${w.numUtxos()} Addrs: ${wi.numUsedAddrs} of ${wi.numUsedAddrs + wi.numUnusedAddrs}")

        w.finalize()
    }

    @Test
    fun rediscoverBothWallets()
    {
        val cs = ChainSelector.NEXAREGTEST
        LogIt.info("Test wallet syncing from genesis")

        val feeder = openOrNewWallet("feederWallet", cs)
        val w = openOrNewWallet("largeWallet", cs)

        w.rediscover()
        feeder.rediscover()
        millisleep(1000U)
        if (!w.sync(5000*1000))
        {
            throw Exception("Wallet sync timeout")
        }
        if (!feeder.sync(5000*1000))
        {
            throw Exception("Wallet sync timeout")
        }

        LogIt.info("POST REDISCOVER")
        val wi = w.statistics()
        LogIt.info("Wallet: ${w.balance} ${w.balanceConfirmed}:${w.balanceUnconfirmed} at ${w.chainstate?.syncedHeight}  Transactions: ${w.numTx()}  Txos: ${w.numTxos()} Utxos: ${w.numUtxos()} Addrs: ${wi.numUsedAddrs} of ${wi.numUsedAddrs + wi.numUnusedAddrs}")
        val fi = feeder.statistics()
        LogIt.info("Feeder Wallet: ${feeder.balance} ${feeder.balanceConfirmed}:${feeder.balanceUnconfirmed} at ${feeder.chainstate?.syncedHeight}  Transactions: ${feeder.numTx()}  Txos: ${feeder.numTxos()} Utxos: ${feeder.numUtxos()} Addrs: ${fi.numUsedAddrs} of ${fi.numUsedAddrs + fi.numUnusedAddrs}")

        w.finalize()
        feeder.finalize()
    }

    /*
    @Test
    fun verifyFeederWalletUtxo()
    {
        verifyWalletUtxo("feederWallet")
    }
    @Test
    fun verifyLargeWalletUtxo()
    {
        verifyWalletUtxo("largeWallet")
    }


    @Test
    fun checkWalletStuff()
    {
        val wal = openWallet("largeWallet")
        val txo = wal.getTxo(NexaTxOutpoint(Hash256("16d920c02ff29593a5634c0cebd99a78612086a9957bfb00550c90ddc7f39a96")))
        LogIt.info(txo!!.dump())
    }

     */

    fun verifyWalletUtxo(walletName: String)
    {
        var rpc = openRpc()

        var curHeight = rpc.getblockcount()
        var bestblock = rpc.getblock(curHeight)
        if (bestblock.time < epochSeconds() - 60*60)
        {
            LogIt.info(sourceLoc() + ": Regtest blockchain is stale; need to generate a block or it will never sync")
            rpc.generate(1)  // could be that the blockchain hasn't produced a block in an hour
        }

        //val cs = ChainSelector.NEXAREGTEST
        LogIt.info(sourceLoc() + ": Waiting for sync")
        val wal = openWallet(walletName)
        millisleep(3000U)
        if (!wal.sync(5000 * 1000))
        {
            throw Exception("Wallet sync timeout")
        }
        LogIt.info(sourceLoc() + ": Sync completed")

        var total = 0
        var totalunspent = 0
        var spent = 0
        curHeight = rpc.getblockcount()
        bestblock = rpc.getblock(curHeight)
        var bestblockhash = bestblock.hash

        // Grab all the Txos in chunks into a list so we don't hang onto the database lock for a long time.
        // This isn't a good plan in general because the txo DB could change under you but ok for a test
        var start = 0
        while(true)
        {
            val spList = mutableListOf<Spendable>()

            var chunkCnt = 0

            // Grab in chunks of 200
            wal.forEachTxo {
                    if (chunkCnt >= start)
                        spList.add(it)
                    chunkCnt++
                (spList.size > 200)
                }
            start += spList.size
            if (spList.size == 0) break


            for (it in spList)
            {
                total++
                if (it.isUnspent)
                {
                    totalunspent++
                    try
                    {
                        val utxoInfo = rpc.getutxo(it.outpoint!!.toHex())
                        // bestblock is the tip of the chain that this UTXO is alive on, not the commit block in the main chain.
                        // so we can't do any comparison with it

                        val confBlk = rpc.getblock(curHeight - utxoInfo.confirmations + 1)
                        //LogIt.info("bestblock: ${utxoInfo.bestblock}=${bestblockhash}\n commitblock: ${confBlk.hash.toHex()}== ${it.commitBlockHash.toHex()}\nUnspent: ${it.dump()}  confirmations: ${utxoInfo.confirmations}")
                        // If this is triggered, its a test malfunction: the blockchain has moved forward, so the curHeight variable will be stale compared to utxoInfo.confirmations
                        assert(utxoInfo.bestblock == bestblockhash)

                        if (confBlk.hash.toHex() != it.commitBlockHash.toHex())
                        {

                            if (it.commitUnconfirmed > 0)
                            {
                                LogIt.info("Confirmed outpoint ${it.outpoint!!.toHex()} is still marked unconfirmed.  TX idem is ${it.commitTxIdem.toHex()}, block is ${confBlk.hash.toHex()} ")
                            }
                            else
                            {
                                LogIt.info("Confirmation block is incorrect.")
                            }
                        }
                        // This is a DB inconsistency, commitBlockHash in DB is not what the RPC suggests it should be
                        //assert(confBlk.hash.toHex() == it.commitBlockHash.toHex())

                    }
                    catch (e: Exception)
                    {
                        spent++
                        //LogIt.info(e.toString())
                        LogIt.info("UNKNOWN UTXO: ${it.dump()} in tx ${it.commitTxIdem.toHex()}")
                        val txh = wal.getTx(it.commitTxIdem)
                        if (txh != null)
                        {
                            LogIt.info("TX: ${txh.tx.toHex()}")
                            LogIt.info("TX History: ${txh}")
                            LogIt.info("TX dump: ${txh.tx}")
                            val spends = txh.tx.inputs[0].spendable.outpoint!!

                        wal.forEachTx {
                            if (it.tx.outpoints.contains(spends))
                            {
                                LogIt.info("Found input creation: ${it}")
                            }
                            for (inp in it.tx.inputs)
                            {
                                if (inp.spendable.outpoint == spends)
                                {
                                    LogIt.info("Already spent in: ${it}")
                                    LogIt.info("Already spent TX dump: ${it.tx}" )
                                }
                            }
                            false
                        }

                            LogIt.info("TX dump done")
                        }
                        else
                        {
                            LogIt.info("DONT EVEN KNOW ABOUT THE TX!!!")
                        }
                    }
                }
                else
                {
                    try
                    {
                        val utxoInfo = rpc.getutxo(it.outpoint!!.toHex())
                        LogIt.info("Txo marked as spent is actually unspent: ${it.dump()}  confirmations: ${utxoInfo.confirmations}")
                    }
                    catch (e: Exception)
                    {
                    }
                }
            }
            LogIt.info("total: $total totalunspent: $totalunspent incorrect: $spent")
        }

        wal.finalize()
    }


    @Test
    fun largeWallet()
    {
        val ITERS = 1 // BUMP this number to 1000 for manual testing of very large wallets
        val doublecheck = false
        var rpc = openRpc()

        commitTxPool(rpc) // clean up
        rpc.generate(10) // I need coins for this one
        val cs = ChainSelector.NEXAREGTEST
        LogIt.info("Test wallet syncing from genesis")

        //val feeder = openOrNewWallet("feederWallet", cs)
        val feeder = forceNewWallet("feederWallet", cs)
        feeder.genAddressChunkSize = 1000
        //val w = openOrNewWallet("largeWallet", cs)
        val w = forceNewWallet("largeWallet", cs)
        w.genAddressChunkSize = 1000
        if (!w.sync(500000))
        {
            throw Exception("Wallet sync timeout")
        }
        if (!feeder.sync(500000))
        {
            throw Exception("Wallet sync timeout")
        }

        LogIt.info("initial balances: ${feeder.balance}  ${w.balance}")

        val addr = listOf(w.getnewaddress(), w.getnewaddress(), w.getnewaddress(), w.getnewaddress(), w.getnewaddress())
        val faddr = listOf(feeder.getnewaddress(), feeder.getnewaddress(), feeder.getnewaddress(), feeder.getnewaddress(), feeder.getnewaddress(),
            feeder.getnewaddress(), feeder.getnewaddress(), feeder.getnewaddress(), feeder.getnewaddress(), feeder.getnewaddress(),
            feeder.getnewaddress(), feeder.getnewaddress(), feeder.getnewaddress(), feeder.getnewaddress(), feeder.getnewaddress(),
            feeder.getnewaddress(), feeder.getnewaddress(), feeder.getnewaddress(), feeder.getnewaddress(), feeder.getnewaddress()
            )

        while (feeder.balance < 10000000L)
        {
            LogIt.info("filling up the feeder wallet")
            rpc.generate(1)
            for (a in faddr)
            {
                var amt = 1000000
                while (true)
                {
                    try
                    {
                        rpc.sendtoaddress(a.toString(), BigDecimal.fromInt(amt))
                        break
                    }
                    catch (e: NexaRpcException)
                    {
                        if ("Maximum inputs allowed" in e.toString() ||
                            "requires a transaction fee" in e.toString())
                        {
                            amt = amt / 2
                        }
                        else if ("Invalid amount for send" in e.toString() ||
                            "Transaction amount too small" in e.toString())
                        {
                            // probably got too low
                            rpc.generate(1)
                            break
                        }
                        else
                        {
                            rpc.generate(1)
                            break
                        }
                    }
                }
            }
            val curblock = rpc.getblockcount()
            if (!feeder.sync(500000, curblock))
            {
                throw Exception("Wallet sync timeout")
            }
        }

        val addr2 = listOf(w.getnewaddress(),w.getnewaddress(),w.getnewaddress(),w.getnewaddress(),w.getnewaddress(),
            w.getnewaddress(),w.getnewaddress(),w.getnewaddress(),w.getnewaddress(),w.getnewaddress(),
            w.getnewaddress(),w.getnewaddress(),w.getnewaddress(),w.getnewaddress(),w.getnewaddress(),
            w.getnewaddress(),w.getnewaddress(),w.getnewaddress(),w.getnewaddress(),w.getnewaddress(),
            w.getnewaddress(),w.getnewaddress(),w.getnewaddress(),w.getnewaddress(),w.getnewaddress()
            )

        val veryStart = millinow()
        var totalTx = 0
        LogIt.info("TRANSACTION CREATION LOOP")
        for (i in range(0, ITERS))
        {
            val st = millinow()
            var numTx = 0
            var intervalStart = 0L
            if ((i % 10) == 0) w.consistencyCheck()
            val SENDS = 10
            for (j in range(0, SENDS))
            {
                if ((j % SENDS) == SENDS-1)
                {
                    val end = millinow()
                    launch {
                        val txpoolinfo = rpc.gettxpoolinfo()
                        if (txpoolinfo.size > Random.nextInt(2000) + 200)
                        {
                            rpc.generate(4)
                        }
                    }
                    LogIt.info("TPS: ${numTx.toFloat()/((end-intervalStart).toFloat()/1000f)} TX CREATED: $totalTx  BALANCES: ${w.balance} (${w.balanceUnconfirmed} unconf)  feeder: ${feeder.balance} (${feeder.balanceUnconfirmed} unconf) ")
                    LogIt.info(w.statsDump())
                    LogIt.info(feeder.statsDump())
                    intervalStart = millinow()
                }
                try
                {
                    w.send(Random.nextLong(5000L) + 1000L, addr[0])
                    numTx++
                    totalTx++
                }
                catch(e: WalletNotEnoughBalanceException)
                {
                    // should never happen except starting up; ignore
                }

                for (a in addr2)
                {
                    try
                    {
                        val tx = feeder.send(Random.nextLong(2000L) + 1000L, a, deductFeeFromAmount = false )
                        //millisleep(1000U)  // overkill: wait for the tx and its parents to be evaluated by the full node
                        if (doublecheck)
                        {
                            val p2p = feeder.blockchain.req.net.getNode()
                            try
                            {
                                rpc.getrawtransaction(tx.idem.toHex())
                            }
                            catch (e: Exception)
                            {
                                LogIt.info("could not get tx: " + e.toString())
                                p2p.sendTxVal(tx) {
                                    if (it.contains("isValid\": false"))
                                    {
                                        delay(2000) // maybe the tx was invalid because the parent had not yet been evaluated
                                        p2p.sendTxVal(tx) {
                                            if (it != "transaction already in mempool")
                                            {
                                                LogIt.info("Invalid transaction created: " + it)
                                                LogIt.info("TX: ${tx.toHex()}")
                                                tx.debugDump()
                                                for (inps in tx.inputs)
                                                {
                                                    val outpt = inps.spendable.outpoint
                                                    if (outpt != null)
                                                    {
                                                        val txo = feeder.getTxo(outpt)
                                                        if (txo != null)
                                                        {
                                                            LogIt.info("Prevout $i: ${txo.dump()}")
                                                        }
                                                        else
                                                            LogIt.info("Unknown TXO")
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        LogIt.info("Valid transaction created: " + tx.id.toHex())
                                    }
                                }
                            }
                        }
                        numTx++
                        totalTx++
                    }
                    catch(e: WalletNotEnoughBalanceException)
                    {
                        val numUtxos = feeder.numUtxos()
                        LogIt.info(sourceLoc() +": Feeder Not enough balance: Transactions: ${feeder.balance} ${feeder.balanceConfirmed}:${feeder.balanceUnconfirmed} NumTx: ${feeder.numTx()}  Txos: ${feeder.numTxos()} Utxos: ${numUtxos}")

                        if (numUtxos < 2000)
                        {
                            val splitter = mutableListOf<Pair<PayAddress,Long>>()
                            for (t in range(0,2))
                            {
                                for (fa in faddr)
                                {
                                    splitter.add(Pair(fa, 10000000))
                                    splitter.add(Pair(fa, 10000000))
                                    splitter.add(Pair(fa, 10000000))
                                }
                                try
                                {
                                    feeder.send(splitter)
                                }
                                catch (e: Exception)
                                {
                                    LogIt.info("Cannot split feeder UTXOs: $e state: ${debugDumpState(feeder, rpc)}")
                                }
                            }
                        }
                        LogIt.info("generating block")
                        rpc.generate(1)
                        try
                        {
                            for (ad in faddr)
                                rpc.sendtoaddress(ad.toString(), BigDecimal.fromInt(10000000))
                        }
                        catch(e: NexaRpcException)
                        {
                            rpc.generate(1)
                        }
                    }

                }
            }
            val end = millinow()
            rpc.generate(1)
            val wstats = w.statistics()
            LogIt.info("Loop: $i TPS: ${numTx.toFloat()/((end-st).toFloat()/1000f)} Elapsed: ${(end-veryStart)/1000} Interval: ${(end-st)/1000} NumTx: $totalTx  Wallet: Transactions: ${w.numTx()}  Txos: ${w.numTxos()} Utxos: ${w.numUtxos()} Used Addresses: ${wstats.numUsedAddrs} Unused Addresses: ${wstats.numUnusedAddrs}")
        }


        commitTxPool(rpc)
        w.finalize()
        feeder.finalize()
    }


    @Test
    fun history()  // expects that the REGTEST blockchain has produced a block within the last hour (or it will hang)
    {
        /* only needed if you reset regtest but don't delete the database files here
        if (!deleteDatabase("unittest_RNEX"))
        {
            println("blockchain DB didn't need to be deleted")
        }
         */
        LogIt.info("This test requires a full node running on regtest at ${FULL_NODE_IP} and port ${NexaRegtestRpcPort}")

        // Set up RPC connection
        val nexaRpc: NexaRpc = openRpc()
        commitTxPool(nexaRpc)

        if (deleteDatabase(dbPrefix + "histwal") == false)
        {
            println("could not delete wallet")
        }

        var usedAddress: PayAddress? = null
        if (true)
        {
            val wal = newWallet(dbPrefix + "histwal", ChainSelector.NEXAREGTEST)
            FULL_NODE_IP?.let { wal.blockchain.req.net.exclusiveNodes(setOf(it)) }
            wal.sync(60000)

            // Get some coins and verify recorded in history
            val myaddr1 = wal.getnewaddress()
            val txhash = nexaRpc.sendtoaddress(myaddr1.toString(), BigDecimal.fromInt(50000))
            waitFor(20000, { "txpool never got the transaction" }) {
                nexaRpc.gettxpoolinfo().size > 0
            }
            waitFor(20000, { "txpool never got the transaction" }) {
                txhash in nexaRpc.getrawtxpool()
            }
            nexaRpc.generate(1)
            waitFor(30000, {"wallet never synced"}, { wal.synced()})
            waitFor(20000, { "expected txhistory to be 1, was ${wal.numTx()}. Txhash was $txhash.\nConnections ${wal.blockchain.req.net.p2pCnxns.size} "}) {

                wal.numTx() >= 1

            }
            val tipheight = nexaRpc.getblockcount()
            val txh = wal.getTx(Hash256(txhash.hash))
            check(txh != null)
            check(txh.incomingAmt == 50000 * SATperNEX)
            check(txh.outgoingAmt == 0L)
            if (txh.confirmedHeight != tipheight)
            {
                LogIt.info("Expected confirmed height to be ${tipheight}, got ${txh.confirmedHeight}")
            }

            // Send coins to myself and verify recorded in history
            val myaddr2 = wal.getnewaddress()
            usedAddress = myaddr2
            val tx2 = wal.send(10000, myaddr2)
            waitFor(20000, { "expected txhistory to be 2, was ${wal.numTx()}"}) {
                wal.numTx() == 2
            }
            val tx2h = wal.getTx(tx2.idem)
            check(tx2h != null)
            check(tx2h.outgoingAmt > 10000L)  // fee increases it
            val amtSentTo = tx2h.tx.amountSentTo(myaddr2)
            check(amtSentTo == 10000L, { "amount is incorrect ($amtSentTo):\n${tx2h.tx.toHex()}\n${tx2h.tx}" })

            // Send coins away and verify recorded in history
            val fullNodeAddr = PayAddress(nexaRpc.getnewaddress())
            val tx3 = wal.send(45000, fullNodeAddr)
            val tx3h = waitFor(10000) {
                wal.getTx(tx3.idem)
            }
            nexaRpc.generate(1)
            println("Num Tx: ${wal.numTx()}")
            check(tx3h.tx.amountSentTo(fullNodeAddr) == 45000L)
            check(tx3h.outgoingAmt > 45000L) // fees
            waitFor(10000, { "Num Tx: ${wal.numTx()}" }, {wal.numTx() == 3})
            wal.finalize()
        }

        if (true)
        {
            val wal = openWallet(dbPrefix + "histwal")
            wal.sync(100)

            // Verify that all transactions were saved in the history
            var count=0
            wal.forEachTx { count++; false }
            check(count == 3, {"count is $count"})
            check(wal.numTx() == 3)

            count = 0
            wal.forEachTxByAddress(usedAddress!!, { count++ })
            check(count == 1, {"count is $count"})
            wal.finalize()
        }
    }

}