import java.util.Properties
import java.io.FileInputStream
import java.net.URL
import org.jetbrains.dokka.base.DokkaBase
import org.jetbrains.dokka.base.DokkaBaseConfiguration
import org.jetbrains.dokka.gradle.DokkaTask


plugins {
    //trick: for the same plugin versions in all sub-modules
    id("com.android.library").version("8.4.1") // https://developer.android.com/build/releases/gradle-plugin
    kotlin("multiplatform").version("2.0.0").apply(false)  // https://kotlinlang.org/docs/multiplatform-dsl-reference.html#top-level-blocks
    kotlin("plugin.serialization") version "2.0.0"  // https://kotlinlang.org/docs/serialization.html#add-plugins-and-dependencies
    id("app.cash.sqldelight").version("2.0.2").apply(false)  // https://github.com/cashapp/sqldelight
    id("maven-publish")  // https://docs.gradle.org/current/userguide/publishing_maven.html
    id("org.jetbrains.dokka").version("1.9.20").apply(false)  // https://github.com/Kotlin/dokka
    idea
}

val prop = Properties().apply {
    try {
        load(FileInputStream(File(rootProject.rootDir, "local.properties")))
    } catch(e: java.io.FileNotFoundException)
    {
        File(rootProject.rootDir, "local.properties").writeText("### This file must NOT be checked into version control, since it contains local configuration.")
        load(FileInputStream(File(rootProject.rootDir, "local.properties")))
    }
}

// Define a few local variables
ext {
    var androidNdkDir = ""
    var linuxToolchain = prop["linux.toolchain"]
    var linuxTarget = prop["linux.target"]
}


android {
    namespace = "org.nexa"
    compileSdk = 35
    //onError(InvalidUserDataException containing "NDK is not installed" printAndExit
    //    "Install the Android Native Development Kit (NDK) first") {
    ndkVersion = "27.0.11902837"  // Pin the NDK because this version is installed in the automated CI docker image
    try {
        rootProject.ext["androidNdkDir"] = android.ndkDirectory
    }
    catch (e:org.gradle.api.InvalidUserDataException)
    {
        val msg = e.message
        if (msg != null && ("NDK is not installed" in msg))
        {
            println("Install the Android Native Development kit, in Tools->SDK Manager->plugins")
        }
        throw e
    }

    buildTypes {
        release {
            ndk {
                debugSymbolLevel = "FULL"
            }
        }
        debug {
            ndk {
                debugSymbolLevel = "FULL"
            }
        }
    }

}

buildscript {
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:8.4.1")  // https://developer.android.com/build/releases/gradle-plugin
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:2.0.0")
        // classpath("com.squareup.sqldelight:gradle-plugin:1.5.5")
        classpath("org.jetbrains.dokka:dokka-base:1.9.20")
    }
}


// Stop android studio from indexing the contrib folder
idea {
    module {
        excludeDirs.add(File(projectDir,"libnexa/nexa"))
        excludeDirs.add(File("/fast/nexa/nexa"))  // Painful but the IDE can't deal with symlinks in this exclude
    }
}

/*
tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}
*/
